from heizmanager.models import sig_get_outputs_from_regelung
from django.dispatch import receiver
import pumpenlogik.views as pl


@receiver(sig_get_outputs_from_regelung)
def pl_get_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raum = kwargs['raum']
    regelung = kwargs['regelung']
    if regelung.regelung == 'pumpenlogik':
        return pl.get_outputs(haus, raum, regelung)
    else:
        return {}