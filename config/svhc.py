from fabric.api import local, settings
from datetime import datetime
import os


def check_processes():
    result = local("sudo supervisorctl -c /etc/supervisor/supervisord.conf status", capture=True)
    for line in result.split('\n'):
        if 'STOPPED' in line or 'EXITED' in line or 'FATAL' in line:
            if 'celery' in line:
                with settings(warn_only=True):
                    local("rm /home/pi/rpi/celerybeat.pid")
            local("sudo supervisorctl -c /etc/supervisor/supervisord.conf start %s" % line.split(' ')[0])

    f = os.path.getmtime('/home/pi/rpi/last_update')
    if (datetime.now() - datetime.fromtimestamp(f)).total_seconds() > 700:
        from fab_update import update
        update()


if __name__ == '__main__':
    check_processes()
