from heizmanager.models import sig_get_outputs_from_regelung
from django.dispatch import receiver
import solarpuffer.views as sp


@receiver(sig_get_outputs_from_regelung)
def sp_get_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raum = kwargs['raum']
    regelung = kwargs['regelung']
    if regelung.regelung == 'solarpuffer':
        return sp.get_outputs(haus, raum, regelung)
    else:
        return {}