from django.core.exceptions import PermissionDenied
from models import UserProfile
from heizmanager.models import Haus, Gateway, Sensor
from django.contrib.auth.models import User
from django.utils.functional import wraps
from heizmanager.render import render_redirect
import logging


def is_owner_or_superuser(func):
    # prueft, ob der aktuelle Benutzer den uebergebenen Benutzer / Haus / Sensor
    # aendern darf oder ein superuser ist

    @wraps(func)
    def check(request, *args, **kwargs):
        if 'hausid' in kwargs:
            owner = Haus.objects.get(pk=long(kwargs['hausid'])).eigentuemer
        elif 'sensorid' in kwargs:
            owner = Sensor.objects.get(pk=long(kwargs['sensorid'])).raum.etage.haus.eigentuemer
        elif 'gatewayid' in kwargs:
            owner = Gateway.objects.get(pk=long(kwargs['gatewayid'])).haus.eigentuemer
        elif 'userid' in kwargs:
            owner = [u.id for u in User.objects.filter(pk=long(kwargs['userid']))]
        elif 'hausid' in request.POST:  # fuer Formulare
            owner = Haus.objects.get(pk=long(request.POST['hausid'])).eigentuemer
        else:
            logging.warning('else')
            pass  # nix ... error?
        user = request.user
        if not 'user_profile' in request.session:
            try:
                profile = UserProfile.objects.get(user_id=user.id)
            except TypeError:  # kein user in request
                return render_redirect(request, '/accounts/login/')
            request.session['user_profile'] = profile
        else:
            pass
        if user.is_superuser or user == owner:  # and profile.role != 'K':
            return func(request, *args, **kwargs)
        raise PermissionDenied
    return check


def is_superuser(func):
    @wraps(func)
    def check(request, *args, **kwargs):
        if request.user.is_authenticated():
            if not 'user_profile' in request.session:
                profile = UserProfile.objects.get(user_id=request.user.id)
            if not request.user.is_superuser:
                raise PermissionDenied
        return func(request, *args, **kwargs)
    return check


def has_admin_access(func):
    @wraps(func)
    def check(request, *args, **kwargs):
        if request.user.is_authenticated():
            if not 'user_profile' in request.session:
                profile = UserProfile.objects.get(user_id=request.user.id)
                request.session['user_profile'] = profile
            
            if request.user.is_superuser or profile.role == 'A':  # sind die schon das gleiche?
                return func(request, *args, **kwargs)
        raise PermissionDenied
    return check


def access_wrapper(superuser, admin, mitarbeiter, partner, kunde):
    def has_access_decorator(func):
        @wraps(func)
        def check(request, *args, **kwargs):
            if request.user.is_authenticated():
                if 'user_profile' not in request.session:
                    profile = UserProfile.objects.get(user_id=request.user.id)
                else:
                    profile = request.session['user_profile']
                
                if superuser and request.user.is_superuser:
                    return func(request, *args, **kwargs)
                if admin and profile.role == 'A':
                    return func(request, *args, **kwargs)
                if mitarbeiter and profile.role == 'M':
                    return func(request, *args, **kwargs)
                if partner and profile.role == 'P':
                    return func(request, *args, **kwargs)
                if kunde and profile.role == 'K':
                    return func(request, *args, **kwargs)
                logging.warning('innere PD')
                raise PermissionDenied
            logging.warning('aeussere PD')
            raise PermissionDenied
        return check
    return has_access_decorator


def profile_helper(func):
    @wraps(func)
    def check(request, *args, **kwargs):
        if request.user.is_authenticated() and not 'user_profile' in request.session:
            profile = UserProfile.objects.get(user_id=request.user.id)
        return func(request, *args, **kwargs)
    return check            
