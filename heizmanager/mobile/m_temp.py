# -*- coding: utf-8 -*-

from heizmanager.models import Haus, Raum, HausProfil, RPi, CryptKeys, Regelung, GatewayAusgang
from knx.models import KNXAusgang, KNXAktor
from users.models import UserProfile
from heizmanager.render import render_response, render_redirect
from django.http import HttpResponse
from collections import namedtuple
import heizmanager.cache_helper as ch
import logging
import json
from django.contrib.contenttypes.models import ContentType
from benutzerverwaltung.models import ObjectPermission
from django.views.decorators.csrf import csrf_exempt
import pytz
from datetime import datetime, timedelta
from benutzerverwaltung.decorators import is_admin, multizone_pro_access, has_access_pro, has_access_quick
import importlib
import heizmanager.network_helper as nh
from django.core.cache import cache as memcache
import copy
import operator
from heizmanager.abstracts.base_mode import BaseMode
from heizmanager.modules.module import get_mods_list
import math

LocalPage = namedtuple('LocalPage', ['lines', 'mods', 'offset'])
logger = logging.getLogger("logmonitor")


def delete_room_offset_in_cache(raum, modulename):
    cached = ch.get("%s_roffsets_dict" % raum.id)
    if cached is not None and modulename in cached:
        ch.delete("%s_roffsets_dict" % raum.id)
        ch.delete_module_offset_raum(modulename, raum.etage.haus.id, raum.id)


def get_module_settings(request, raumid, modulename):
    # fuer Module, die zu einem Raum etwas dazufuegen
    raum = Raum.objects.get_for_user(request.user, pk=long(raumid))

    if request.method == 'POST' or 'delete' in request.GET or 'zsdel' in request.GET or 'asdel' in request.GET:
        delete_room_offset_in_cache(raum, modulename)

    try:
        module = importlib.import_module(modulename.strip() + '.views')
    except ImportError:
        module = importlib.import_module('heizmanager.modules.' + modulename.strip())
    ch.set_usage_val('glspr_%s_%s' % (raumid, "post" if request.method == 'POST' else "get"), modulename)
    return module.get_local_settings_page(request, raum)


def _get_haus_for_user(usr):
    for haus in Haus.objects.order_by('id'):
        if usr.id in haus.besitzer:
            try:
                profil = haus.profil.get()
            except:
                profile = HausProfil(haus=haus)
                profile.set_address('', '', '', '')
                profile.modules = 'fernzugriff'
                params = dict()
                params['wetter'] = dict()
                params['wetter']['lng'] = 0.0
                params['wetter']['lat'] = 0.0
                params['wetter']['wetter_params'] = '5,5'
                params['gcal'] = dict()
                params['fernzugriff'] = dict()
                params['fernzugriff']['active'] = True
                params['temperaturmodi'] = {1: {'name': '-*'}}
                profile.module_parameters = str(params)
                profile.save()

            if not len(RPi.objects.all()):
                macaddr = nh.get_mac()
                from rpi.aeskey import key
                from rpi.verification_code import verification_code
                ck = CryptKeys()
                ck.aeskey = key
                ck.save()
                rpi = RPi(crypt_keys=ck, verification_code=verification_code, name=macaddr, upgrade_version="",
                          local_address="", global_address="", haus=haus)
                rpi.save()

            return haus
    else:
        return None


@multizone_pro_access
def index(request):

    from django.contrib.auth.models import AnonymousUser
    if isinstance(request.user, AnonymousUser):
        return render_redirect(request, "/accounts/logout/")

    haus = _get_haus_for_user(request.user)
    if request.method == "GET":
        ch.set_usage_val("raumliste", 1)

        haus = _get_haus_for_user(request.user)
        tempmode_items = []
        tempmodi = False
        heizprogramm_enabled = False
        ruecklaufregelung_enabled = False
        eundr = []
        if haus:
            for etage in haus.etagen.all():
                e = {etage: []}
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    e[etage].append(raum)  # todo: das hat unten nur die .values_list verwendet
                eundr.append(e)

            hparams = haus.get_module_parameters()
            hausmods = haus.get_modules()

            tempmode_params = hparams.get('temperaturmodi', {})

            if 'temperaturszenen' in hausmods:
                tempmodi = True
                sorted_lst = sorted(tempmode_params, key=lambda x: (tempmode_params[x].get('order', 99)))
                if 'temperaturszenen' in hausmods:
                    tempmodi = True
                    tempmode_items = ((item, tempmode_params[item]['name']) for item in sorted_lst)

            heizprogramm_enabled = hparams.get('is_switchingmode_enabled', None)

            # if module is activated and user has access to configuration
            if 'ruecklaufregelung' in hausmods and request.user.userprofile.get().role != 'K':
                ruecklaufregelung_enabled = True  # variable for showing warning message

        return render_response(request, "m_index.html", {"haus": haus, 'eundr': eundr,
                                                         'tempmodes': tempmode_items, 'tempmode': tempmodi,
                                                         'en_hiezprogramm': heizprogramm_enabled,
                                                         'en_ruecklaufregelung': ruecklaufregelung_enabled})

    if request.method == "POST":
        eigentuemer = request.user

        if 'vorname' in request.POST:
            profile = UserProfile.objects.get(user=request.user)
            profile.vorname = request.POST['vorname']
            profile.nachname = request.POST['nachname']
            profile.save()
        if 'str' in request.POST:
            haus = Haus.objects.all()
            name = request.POST.get('str', '') + ' ' + request.POST.get('nr', '') + ', ' + request.POST.get('plz', '') + ' ' + request.POST.get('ort', '')
            if len(haus):
                haus = haus[0]
                haus.name = name
                haus.save()
                haus.profil.get().set_address(request.POST.get('str', ''), request.POST.get('nr', ''), request.POST.get('plz', ''), request.POST.get('ort', ''))
                params = haus.get_module_parameters()
                params.setdefault('wetter', dict())
                params['wetter']['lng'] = request.POST['lng']
                params['wetter']['lat'] = request.POST['lat']
                params.setdefault('backupcontrol', dict())
                params['backupcontrol']['active'] = True
                haus.set_module_parameters(params)
            else:
                haus = Haus(name=name, eigentuemer=eigentuemer)
                haus.save()
                profile = HausProfil(haus=haus)
                profile.set_address(request.POST.get('str', ''), request.POST.get('nr', ''), request.POST.get('plz', ''), request.POST.get('ort', ''))
                profile.modules = 'fernzugriff'
                params = dict()
                params['wetter'] = dict()
                params['wetter']['lng'] = request.POST['lng']
                params['wetter']['lat'] = request.POST['lat']
                params['wetter']['wetter_params'] = '5,5'
                params['gcal'] = dict()
                params['fernzugriff'] = dict()
                params['fernzugriff']['active'] = True
                params['backupcontrol'] = dict()
                params['backupcontrol']['active'] = True
                profile.module_parameters = str(params)
                profile.save()
            hausmods = haus.get_modules()
            hausmods.append('backupcontrol')  # activating module 'backup-control' as default
            haus.set_modules(hausmods)  # save modules list

        if not len(RPi.objects.all()):
            macaddr = nh.get_mac()
            from rpi.aeskey import key
            from rpi.verification_code import verification_code
            ck = CryptKeys()
            ck.aeskey = key
            ck.save()
            rpi = RPi(crypt_keys=ck, verification_code=verification_code, name=macaddr, upgrade_version="",
                      local_address="", global_address="", haus=haus)
            rpi.save()
        else:
            for rpi in RPi.objects.all():
                if not rpi.haus:
                    rpi.haus = haus
                    rpi.save()

        if request.POST.get('redirect', '') != '':
            return render_redirect(request, request.POST['redirect'])
        else:
            return render_redirect(request, "/config/%s/" % haus.id)


def no_access(request):
    user_type = request.user.userprofile.get().role
    if user_type == 'A':
        config_access = True
    else:
        config_access = False
    return render_response(request, "m_no_access_page.html", {'config_access': config_access})


def sensorenuebersicht(request):

    ch.set_usage_val("sensorenuebersicht", 1)

    haus = _get_haus_for_user(request.user)
    html = ''
    if haus:
        html += '<div class="content">'
        for e in haus.etagen.all():
            html += u'''<div  class="ui-nodisc-icon ui-alt-icon ui-collapsible  ui-collapsible-inset ui-corner-all" ><h3 class="ui-collapsible-heading">
                        <a href="#" class="ui-collapsible-heading-toggle ui-btn ui-fullsize ui-btn-up-a">
                        <span class="ui-btn-inner"><span class="ui-btn-text floor_name"> %s </span>
                        </span></a></h3><div class="ui-collapsible-content">''' % e.name
            for raum in Raum.objects.filter_for_user(request.user, etage_id=e.id):

                reg = raum.get_regelung()
                localpage = reg.get_local_page(request, raum)

                html += u'''
                <ul data-role="none" data-id="{id}" class="room_list"><li class="customli ui-li ui-li-static ">
                    <a data-role="none" href="/m_raum/{id}/" class="ui-link-inherit"> <span class="room_icon"><img src="/static/icons/{room_icon}.png" alt="icon" /></span>
                    <span class="room_name">{room_name}</span></a>'''.format(id=raum.id, room_icon=raum.icon, room_name=raum.name)
                html += '<ul data-role="none" class="sensor-list">'
                for left, right, r_width, link in localpage.lines:
                    html += '<li>'
                    if link:
                        html += '<a href="%s" data-role="none" style="max-height: 20px">' % link

                    html += left

                    if right:
                        html += '<span>%s</span>' % right
                    if link:
                        html += '</a>'
                    html += '</li>'

                html += '</ul></ul>'
            html += '</div></div>'
        html += '</div>'

    return render_response(request, "m_sensorenuebersicht.html", {"html": html})


def get_right_menu_module_link(haus, mod):
    try:
        module = importlib.import_module(mod.strip() + '.views')
        return module.get_right_menu_module_link(haus)
    except (ImportError, AttributeError):
        return False


def sensorenuebersicht_hrgw(request):
    haus = _get_haus_for_user(request.user)
    return render_response(request, "m_sensorenuebersicht_hrgw.html", {'haus': haus})


def get_main_menus_ajax(request):

    ch.set_usage_val('screensize', request.META.get('QUERY_STRING', '0x0'))

    haus = _get_haus_for_user(request.user)

    room_html = ''
    module_html = ''
    system_html = ''
    if haus:

        # Left menu
        if has_access_quick(request):
            room_html += u'<li class="quick_link"><h4 class="quick-menu">' \
                         u'<a href="/quick-change" class ="underline multizone">temporäre Änderungen</a></h4></li>'

        hparams = haus.get_module_parameters()
        if has_access_pro(request):
            room_html += '<li><h4 class="pro-menu"><a href = "/" class ="underline multizone">Raumregelung</a></h4></li>'
            for e in haus.etagen.all():
                room_html += u'<li class="room-list" data-role="collapsible"  data-collapsed="false" data-iconpos="right" data-inset="false"><h2 class="etage-left">%s</h2><ul data-role="listview"  >' % e.name
                for raum in Raum.objects.filter_for_user(request.user, etage_id=e.id).values_list('id', 'name'):
                    room_html += u'''<li><a href="/m_raum/{id}/" class="room-left">{room_name}</a></li>'''.format(id=raum[0], room_name=raum[1])
                room_html += '</ul></li>'

            if hparams.get('differenzregelung') or hparams.get('vorlauftemperaturregelung') or "fps" in haus.get_modules() or 'pumpenlogik' in haus.get_modules():
                room_html += u"<li class='diff-vor-left zentrale'><h2 class='underline diff-vor-left'>zentrale Heizungssteuerung PRO</h2>"
                room_html += u"<ul data-role='listview'>"
                diff_hparams = hparams.get('differenzregelung', {})
                vor_hparams = hparams.get('vorlauftemperaturregelung', {})
                regs = Regelung.objects.filter_for_user(request.user).filter(regelung='differenzregelung')
                diff_params = {}
                vor_params = {}
                for reg in regs:
                    dreg_id = reg.get_parameters()['dreg']
                    try:
                        diff_params[dreg_id] = diff_hparams[dreg_id]
                    except KeyError:
                        ct_regelung = ContentType.objects.get_for_model(Regelung)
                        ObjectPermission.objects.filter(content_type=ct_regelung, object_id=reg.id).delete()
                        reg.delete()

                for dreg_id, config in sorted(diff_params.iteritems()):
                    if config.get('show_in_menu', True):
                        name = config.get('name') or "Differenzregelung %s" % dreg_id

                        room_html += u"<li><a href='/m_differenzregelung/%s/show/%s/' class='vor-diff-left'>%s</a></li>" % (haus.id, dreg_id, name)

                regs = Regelung.objects.filter_for_user(request.user).filter(regelung='vorlauftemperaturregelung')
                for reg in regs:
                    mid_id = reg.get_parameters()['mid']
                    try:
                        vor_params[mid_id] = vor_hparams[mid_id]
                    except KeyError:
                        ct_regelung = ContentType.objects.get_for_model(Regelung)
                        ObjectPermission.objects.filter(content_type=ct_regelung, object_id=reg.id).delete()
                        reg.delete()

                for mid_id, config in sorted(vor_params.iteritems()):
                    if config.get('show_in_menu', True):
                        name = config.get('name') or "Vorlauftemperaturregelung %s" % mid_id

                        room_html += u"<li><a href='/m_vorlauftemperaturregelung/%s/show/%s/' class='vor-arrow-right'>%s</a></li>" % (haus.id, mid_id, name)

                regs = Regelung.objects.filter_for_user(request.user).filter(regelung='fps')
                for reg in regs:
                    params = reg.get_parameters()
                    if params.get('show_in_menu', True) and params.get('name'):
                        room_html += u"<li><a href='/m_fps/%s/show/%s/' class='fps-arrow-right'>%s</a></li>" % (haus.id, reg.id, params.get('name', ''))

                regs = Regelung.objects.filter_for_user(request.user).filter(regelung='pumpenlogik')
                for reg in regs:
                    params = reg.get_parameters()
                    if params.get('show_in_menu', True) and params.get('name'):
                        room_html += u"<li><a href='/m_pumpenlogik/%s/show/%s/' class='ra-arrow-right'>%s</a></li>" % (haus.id, reg.id, params.get('name', ''))

                room_html += u"</ul></li>"

        # Right menu
        if not has_access_pro(request):
            system_html += '<li><a class="ui-btn ui-btn-icon-right ui-icon-carat-r" href="/accounts/logout/">Logout</a></li>'
            return HttpResponse(json.dumps({'room_html': room_html,
                                'module_html': module_html, 'system_html': system_html}),
                                content_type="application/json")

        try:
            if request.user.userprofile.get().role != 'K':
                pass
        except:
            from fabric.operations import local as lrun
            result = lrun("sqlite3 /home/pi/rpi/db.sqlite3 '.schema users_userprofile'", capture=True)
            if 'parameters' not in result:
                lrun("python /home/pi/rpi/manage.py makemigrations users")
                lrun("python /home/pi/rpi/manage.py migrate users")

        swlinks, category_dict = get_mods_list(haus)
        mod_d = dict((mod, modename) for mod, modename , desc, desc_link, link in swlinks)
        cat_list = sorted(category_dict.items(), key=lambda x: x[1]['order'])

        arrow_svg = '''
            <svg class="arrow-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" 
            xmlns:xlink="http://www.w3.org/1999/xlink" 
            preserveAspectRatio="xMidYMid meet" viewBox="0 0 640 640" width="13" height="13"><defs>
            <path d="M-24.92 9.05L704.92 9.05L704.92 738.89L-24.92 738.89L-24.92 9.05Z" id="d1JoilotkG"></path>
            <path d="M144.71 140L140.4 130L140 120L141.11 110L146.38 100L153.78 94.29L164.71 90.24L174.71 87.37L185.88
             87.88L199.71 91.82L214.12 100L400 277.29L410.74 289L418.67 300L423.62 315L424.71 331.41L422.01 345L416.47 
             356.12L290 479.25L208.12 557.59L200.91 563.28L192.37 567.83L180.67 567.33L169.71 562.33L156.96 554L148.93 
             542.33L144.71 527.31L146.92 515L154.71 503.67L294.12 365.53L310 350L320 338.54L326.12 322.55L325 313.3L322.1
            306.33L312.5 295L300 283.18L155.29 150.24L144.71 140Z" id="a3N3i6hXX8"></path>
            </defs><g><g><g></g><g><use xlink:href="#a3N3i6hXX8" opacity="1" fill="{color}" fill-opacity="1"></use></g>
            </g></g></svg>
        '''

        for cat, params in cat_list:
            module_html += '<li>'
            # if modname_righ_menu in haus parameters is true link will be shown up

            mod_list = list()
            for mod in params['mod_list']:
                if not mod.get('r_menu') or not mod.get('activated'):
                    continue

                # for checking more base on a specific condition 'check show link' is there
                if mod.get('check_show_link'):
                    if get_right_menu_module_link(haus, mod['modname']):
                        mod_list.append(mod)
                else:
                    if hparams.get('%s_right_menu' % mod['modname'], True):
                        mod_list.append(mod)

            if len(mod_list):
                module_html += "<h4 style='color: %s;'>%s </h4>" % (params['color'], params['name'])
                module_html += '<ul data-role="listview" data-arrow-color="%s">' % (params['color'])
            for mod in mod_list:
                # check permission
                if 'perm' in mod and 'A' in mod['perm']:
                    if request.user.userprofile.get().role == 'K':
                        continue

                module_html += "<li><a class='ui-btn ui-btn-icon-right ui-icon-carat-r' " \
                               "href='/m_%s/%s/'>%s %s</a></li>" % (mod['modname'], haus.id,
                                                                    mod_d[mod['modname']],
                                                                    arrow_svg.format(color=params['color']))
            if len(mod_list):
                module_html += '</ul>'
            module_html += '</li>'

        system_html = u"<li><a class='ui-btn ui-btn-icon-right ui-icon-carat-r' " \
                      u"href='/sensorenuebersicht'>Sensorenübersicht</a>"
        system_html += u"<li><a class='ui-btn ui-btn-icon-right ui-icon-carat-r' " \
                       u"href='/m_benutzerverwaltung/%s/'>Benutzerverwaltung</a></li>" % haus.id

        if haus.show_config() == 1 and request.user.userprofile.get().role != 'K':
            system_html += '<li><a class="ui-btn ui-btn-icon-right ui-icon-carat-r" target="_blank" href="/config">Konfiguration</a></li>'

        system_html += '<li><a class="ui-btn ui-btn-icon-right ui-icon-carat-r" href="/accounts/logout/">Logout</a></li>'

    return HttpResponse(
        json.dumps({'room_html': room_html, 'module_html': module_html, 'system_html': system_html}),
        content_type="application/json"
    )


def get_raum_state_from_reg(request, raumid):
    try:
        raum = Raum.objects.get_for_user(request.user, pk=long(raumid))
        if '/m_raum/' in request.META.get('HTTP_REFERER', ''):
            ch.delete("lastget_%s_%s" % (raum.regelung_id, 0))
        reg = raum.get_regelung()
        html = reg.get_local_html(request, raum)

    except Exception, e:
        logging.exception("error in room page type :%s message: %s" % (str(type(e)), str(e)))

        mac = nh.get_mac()
        rpi = RPi.objects.get(name=mac)
        haus = Haus.objects.all()[0]
        html = u'''<p class='red-str' style='padding: 10px;'>Ein Anzeigefehler ist aufgetreten. 
        Sollte der Fehler bestehen, aktualisieren Sie bitte Ihren Miniserver auf die neueste 
        Version und wechseln Sie ggf. auf die Lab-Version.
        <a href='/m_setup/{hausid}/hardware/setup_rpi?create={rpi_id}'>
        Weiter zur Miniservereinrichtung.</a> </p>'''.format(hausid=haus.id, rpi_id=rpi.id)

    return HttpResponse(html)


@is_admin
def houseprofile_edit(request):
    haus = _get_haus_for_user(request.user)
    address = haus.profil.get().get_address()
    return render_response(request, "m_houseprofile_edit.html", {"haus": haus, "address": address})


@csrf_exempt
@multizone_pro_access
@csrf_exempt
def show_raum(request, raumid):

    try:
        raum = Raum.objects.get_for_user(request.user, pk=long(raumid))
    except Raum.DoesNotExist:
        return render_redirect(request, "/")

    if not raum:
        return render_redirect(request, "/")

    if request.method == "GET":
        ch.set_usage_val("raum_get", raumid)

        haus = raum.etage.haus
        reg = raum.get_regelung()

        localpage = reg.get_local_page(request, raum)

        rlonly = False
        params = reg.get_parameters()
        ms = raum.get_mainsensor()
        if (ms is None and params.get('rlsensorssn', False)) or params.get('ignorems', False):
            rlonly = True
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        yd = (now - timedelta(days=1)).strftime("%Y-%m-%dT00:00")
        now = now.strftime("%Y-%m-%dT%H:%M")

        sensoren = raum.get_sensoren()

        out = raum.regelung.get_ausgang()
        gw_inverted = False
        outs = []
        if out is not None and isinstance(out, GatewayAusgang):
            outs = [(out.gateway.name, o) for o in sorted([int(_o) for _o in out.ausgang.split(',')])]
            gw_inverted = haus.profil.get().get_gateway_invertouts(out.gateway.name.lower())
        else:
            out = KNXAusgang.objects.filter(regelung=raum.regelung)
            if len(out):
                out = out[0]
                try:
                    outs = [(out.gateway.address, KNXAktor.objects.get(pk=o).group_address) for o in out.aktor]
                except Exception:
                    logging.exception("exc")
        if isinstance(localpage, HttpResponse):  # 404, vermutlich
            return render_response(request, "m_raum.html",
                                   {'raum': raum, 'lines': [], 'mods': [], 'offset': 0.0, "outs": outs,
                                    'rlonly': rlonly, 'raumid': raumid, 'ms': ms, 'sensoren': sensoren,
                                    'logging': 'logger' in haus.get_modules(), 'haus': haus, 'bis': now, 'von': yd})

        if localpage.offset != 0 and localpage.offset != '-':
            offset = '%.2f' % localpage.offset
        else:
            offset = "0.00"

        # TODO!
        _mods = localpage.mods
        mods = []
        if _mods:
            for mod in _mods:
                if len(mod) == 4:
                    mods.append((mod[0],) + mod)
                else:
                    mods.append(mod)

        humidity = raum.get_humidity()
        lf = "%s" % int(humidity[0]) if humidity and humidity[0] and humidity[0] > -1 else None

        hausmods = haus.get_modules()
        hparams = haus.get_module_parameters()
        tempmodi = False
        tempmode_params = hparams.get('temperaturmodi', {})
        tempmode_items = []

        sorted_lst = sorted(tempmode_params, key=lambda x: (tempmode_params[x].get('order', 99)))
        if 'temperaturszenen' in hausmods:
            tempmodi = True
            tempmode_items = ((item, tempmode_params[item]['name']) for item in sorted_lst)

        gmr = BaseMode.get_active_mode_raum(raum)
        activated_mode = None
        if gmr:
            activated_mode = gmr['activated_mode_text']

        heizprogramm = False
        if 'heizprogramm' in hausmods:
            heizprogramm = True

        raummods = raum.get_modules()
        log = None
        hfo_module = None
        strategy_mode = None
        if 'heizflaechenoptimierung' in raummods and 'heizflaechenoptimierung' in hausmods:
            hfo_module = True
            r_params = raum.get_module_parameters()
            if 'strategy' in r_params.get('heizflaechenoptimierung', {}):
                strategy_mode = r_params.get('heizflaechenoptimierung', {}).get('strategy')
            elif 'strategy' in r_params:
                strategy_mode = r_params['strategy']
                r_params.setdefault('heizflaechenoptimierung', {})
                r_params['heizflaechenoptimierung']['strategy'] = strategy_mode
                del r_params['strategy']
                raum.set_module_parameters(r_params)
            else:
                strategy_mode = 'custom'
            strategy_mode = strategy_mode.replace('_', ' ')
            # get hfo log from logmonitor
            from logmonitor.views import get_logs
            log = get_logs(request.user, haus, 'hfo', raumid)
            if len(log) > 0:
                log = log[0]
                log = log[1]
                log = log.split(':', 1)[1] if len(log.split(':', 1)) > 1 else None
                if log:
                    log = log.split(".", 1)

        # kaminofen
        kaminofen_lst = list()
        if 'kaminofen' in haus.get_modules():
            for kid, param in haus.get_spec_module_params('kaminofen').items():
                if str(raum.id) in param['rooms']:
                    kaminofen_lst.append((kid, param))

        ki_mode = raum.get_module_parameters().get('heizflaechenoptimierung', {}).get('ki_mode', 'off')
        ki_active = 'ki' in haus.get_modules() and ki_mode != 'off'
        wetter_pro_active = hparams.get('wetter_pro', {}).get('is_active', False)
        score = -1
        if ki_active and wetter_pro_active:
            if ki_mode == 'proki':
                score = raum.get_hfo_params().get('score', 0) * 100
            elif ki_mode == 'on':
                from ki.views import valuation_ki
                blue, green = valuation_ki(haus.id, raumid)
                blue = blue * 100
                green = green * 100
                score = int(round(blue + green))

        betrieb_ret = raum.get_betriebsart()
        # when betriebsarten is not activated or is set to heizen, background is white and red
        chart_background = None
        if int(betrieb_ret) == 2 and not gw_inverted:
            chart_background = 'heating'
        elif int(betrieb_ret) == 2 and gw_inverted:
            chart_background = 'heating_inverted'
        # when betriebsarten is activated and is set to kuehlen, background always blue
        elif int(betrieb_ret) == 1 and not gw_inverted:
            chart_background = 'cooling'
        return render_response(request, "m_raum.html",
                               {'raum': raum, 'lines': localpage.lines, 'mods': mods, 'offset': offset, "outs": outs,
                                'rlonly': rlonly, 'raumid': raumid, 'ms': ms, 'sensoren': sensoren, "lf": lf,
                                'heizprogramm': heizprogramm, 'logging': 'logger' in haus.get_modules(),
                                'haus': haus, 'bis': now, 'von': yd,
                                'tempmodes': tempmode_items, 'tempmode': tempmodi,
                                'activated_mode': activated_mode, 'hfo_log': log, 'hfo_module': hfo_module,
                                'strategy_mode': strategy_mode, 'kaminofen_lst': kaminofen_lst,
                                'score': score, 'ki_active': ki_active, 'wetter_pro_active': wetter_pro_active,
                                'chart_background': chart_background})

    elif request.method == "POST":
        ch.set_usage_val("raum_post", raumid)
        soll = float(request.POST['slidernumber'].replace(',', '.'))
        if math.isnan(soll):
            return HttpResponse()
        mode = BaseMode.get_active_mode_raum(raum)
        if mode:
            mode['activated_mode'].set_mode_temperature_raum(raum, soll)
        else:
            reg = raum.get_regelung()
            if reg:
                return reg.get_local_page(request, raum)
        return render_redirect(request, "/m_raum/%s/" % raum.id)


def get_offsets_icon(raum):
    clear_offsets = get_module_offsets(raum, only_clear_offsets=True, as_dict=True)
    all_offsets = get_module_offsets(raum, as_dict=True)
    offsets = sum(clear_offsets.values())
    offsets_items = dict((mod, abs(offset)) for mod, offset in all_offsets.items())
    icon = ""
    if len(offsets_items):
        max_offset_module = max(offsets_items.iteritems(), key=operator.itemgetter(1))[0]
        if offsets_items[max_offset_module] != 0:
            module_trans = {'luftfeuchte': 'humidity', 'gcal': 'wochenkalender', 'zeitschalter': 'timer', 'api': 'offsets'}
            if raum.regelung.regelung == 'ruecklaufregelung':
                module_trans.update(dict(heizflaechenoptimierung='heizflaechenoptimierung_blue'
                                                                 if all_offsets.get('heizflaechenoptimierung', 0.0) < 0
                                                                 else 'heizflaechenoptimierung_red'
                                                                 if all_offsets.get('heizflaechenoptimierung', 0.0) > 0
                                                                 else 'heizflaechenoptimierung_grey'))
            icon = "icon_%s" % (
                module_trans[max_offset_module] if max_offset_module in module_trans else max_offset_module)
    return offsets, icon


def get_mode_regelung(haus, reg):

    # todo: same structure as get_mode_raum()

    pass


def get_module_offsets(raum, only_clear_offsets=False, exclude=None, as_dict=False):
    memcache.close()

    cached = ch.get("%s_roffsets_dict" % raum.id)
    if only_clear_offsets:
        if cached and cached.get('only_clear_offsets') == 0.0:
            del cached['only_clear_offsets']
            if as_dict:
                return cached
            else:
                return sum(cached.values())
        elif cached:
            haus = raum.etage.haus
            for mod in set(haus.get_modules()) & set(raum.get_modules()):
                if mod == exclude:
                    continue
                try:
                    module = importlib.import_module(mod.strip() + '.views')
                except ImportError:
                    continue
                try:
                    if mod in cached and module.is_hidden_offset():
                        del cached[mod]
                except AttributeError:
                    continue

    else:
        if cached and cached.get('only_clear_offsets') == 0.0:
            cached = None

    if exclude:
        # wenn jemand mit exclude aufruft, muss er trotzdem immer selber nochmal loeschen!
        try:
            del cached[exclude]
        except:
            pass

    if as_dict:
        if cached is not None:
            return cached
    else:
        if cached is not None:
            return sum(cached.values())

    clear_offset = 0.0
    total_offset = 0.0
    ret = {}
    haus = raum.etage.haus
    for mod in set(haus.get_modules()) & set(raum.get_modules()):
        if mod == exclude:
            continue

        try:
            module = importlib.import_module(mod.strip() + '.views')
        except ImportError:
            continue

        raum_offset = haus_offset = None

        try:
            ignore_cache = module.calculate_always_anew()
        except AttributeError:
            ignore_cache = False

        if not ignore_cache:
            haus_offset = ch.get_module_offset_haus(mod, haus.id)

        if haus_offset is None:
            try:
                haus_offset = module.get_offset(haus)
            except AttributeError:
                haus_offset = 0.0

        if not ignore_cache:
            raum_offset = ch.get_module_offset_raum(mod, haus.id, raum.id)

        if raum_offset is None:
            try:
                raum_offset = module.get_offset(haus, raum=raum)
            except AttributeError:
                raum_offset = 0.0

        try:
            if only_clear_offsets and not module.is_hidden_offset():
                clear_offset += haus_offset + raum_offset
        except AttributeError:
            pass

        if haus_offset or raum_offset:
            logging.warning("mod %s mit %s/%s" % (mod, haus_offset, raum_offset))
        else:
            logging.warning("mod %s ohne offset" % mod)
        total_offset += haus_offset + raum_offset
        try:
            if (only_clear_offsets and not module.is_hidden_offset()) or not only_clear_offsets:
                ret[mod] = haus_offset + raum_offset
        except AttributeError:
            pass

    api_raum = ch.get('api_roomoffset_%s' % raum.id)
    if api_raum is not None:
        total_offset += sum(api_raum.values())
        clear_offset += sum(api_raum.values())
        ret['api'] = sum(api_raum.values())
    api_haus = ch.get('api_houseoffset_%s' % haus.id)
    if api_haus is not None:
        total_offset += sum(api_haus.values())
        clear_offset += sum(api_haus.values())
        ret.setdefault('api', 0.0)
        ret['api'] += sum(api_haus.values())

    if only_clear_offsets is True:
        # das geht davon aus, dass ein modul, das so aufruft, immer so aufruft
        ret['only_clear_offsets'] = 0.0
    ch.set("%s_roffsets_dict" % raum.id, ret, time=300)

    ch.set_usage_val("offsets", ret)

    if as_dict:
        if exclude is not None:
            try:
                del ret[exclude]
            except:
                pass
        return ret
    else:
        if only_clear_offsets:
            return clear_offset
        else:
            return total_offset


def get_module_offsets_regelung(haus, objid, modulename, as_dict=False):

    params = haus.get_module_parameters()
    hmods = haus.get_modules()
    mparams = params.get(modulename, dict())

    try:
        module_list = mparams[objid]['modules_list']
    except:
        module_list = []

    total_offset = 0
    module_offsets = {}
    for mod in module_list:
        if mod not in hmods:
            continue
        offset = ch.get_module_offset_regelung(mod, haus.id, modulename, objid)
        if offset is None:
            ttl = 600
            try:
                module = importlib.import_module(mod.strip() + '.views')
                offset = module.get_offset_regelung(haus, modulename, objid)
                if module.get_cache_ttl() < 600:
                    ttl = module.get_cache_ttl()
            except Exception as e:
                logging.exception("exception getting module offset")
                offset = 0
            ch.set_module_offset_regelung(mod, haus.id, modulename, objid, offset, ttl=ttl)

        total_offset += offset
        module_offsets[mod] = offset

    if as_dict:
        return module_offsets
    else:
        return total_offset


def get_module_line_list_regelung(haus, objid, modulename):

    params = haus.get_module_parameters()
    mparams = params.get(modulename, dict())

    try:
        module_list = mparams[objid]['modules_list']
    except:
        module_list = []

    module_line_list = []
    for mod in module_list:
        offset = 0
        try:
            module = importlib.import_module(mod.strip() + '.views')
            try:
                ret = module.get_local_settings_link_regelung(haus, modulename, objid)
            except:
                ret = (module.get_name(), mod, offset, 75, '#')
            module_line_list.append(ret)
        except:
            pass

    return module_line_list


def get_module_upcoming_events(haus, entity, entity_id, lookahead=12):

    # get model name
    if isinstance(entity, Regelung):
        entity_str = unicode(entity)
    else:
        entity_str = entity.__class__.__name__
    #
    cache_key = "%s_upcoming_lookedahead_%s_%s" % (haus.id, entity_str, entity_id)
    ret = {}
    berlin_timezone = pytz.timezone('Europe/Berlin')
    lookahead_time = datetime.now(berlin_timezone) + timedelta(hours=lookahead)

    # checking in cache
    cached_data = ch.get(cache_key)

    if cached_data is not None:
        if lookahead_time <= cached_data[0]:
            return cached_data[1]

    # calculate a longer lookahead
    lookahead += 6
    from heizmanager.modules.module import planning_modules
    planning_mods = copy.deepcopy(planning_modules)
    planning_mods = [mod for mod in planning_mods if mod in haus.get_modules()]

    for mod in planning_mods:

        try:
            module = importlib.import_module(mod.strip() + '.views')
        except ImportError:
            continue
        try:
            upcoming_events = module.get_module_upcoming_events(haus, entity, entity_id=entity_id, lookahead=lookahead)

            if upcoming_events is not None:
                ret[mod] = upcoming_events

        except AttributeError:
            continue

    # caching
    ch.set(cache_key, (datetime.now(berlin_timezone) + timedelta(hours=lookahead), ret), lookahead * 60 * 60)
    return ret
