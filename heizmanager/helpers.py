from datetime import datetime
from time import mktime
import pytz


def get_active_modules(haus):
    active_modules_list = haus.get_modules()
    active_modules_list.append('quickui')
    return active_modules_list


def get_str_formatted_time(time):
    return time.strftime("%Y-%m-%d %H:%M")


def ret_formatted_time_from_str(str_time):
    return datetime.strptime(str_time, '%Y-%m-%d %H:%M')


def get_ts_from_str_time(str_time):
    berlin = pytz.timezone('Europe/Berlin')
    return mktime(berlin.localize(datetime.strptime(str_time, '%Y-%m-%d %H:%M')).timetuple())


def get_temperatureszene_mode_name(haus, mode_id):
    return haus.get_spec_module_params('temperaturmodi').get(mode_id, {}).get('name')


def get_temperatureszene_temperature(raum, mode_id):
    return raum.get_spec_module_params('temperaturmodi').get(mode_id, {}).get('solltemp')


def get_temperatureszene_activated_mode_id_raum(raum):
    return raum.get_spec_module_params('temperaturmodi').get('activated_mode', 1)


def get_heizprogramm_dict(haus, heizp_id):
    heizprogramm_list = haus.get_module_parameters().get('heizprogramms_list', [])
    try:
        heizprogramm_of_interest = next(item for item in heizprogramm_list if item["heizp_id"] == heizp_id)
    except StopIteration:
        heizprogramm_of_interest = None

    # Standard is default
    if heizprogramm_of_interest is None and heizp_id == 0:
        return {'heizp_id': 0, 'heizp_name': 'Standard', 'is_active': True}
    return heizprogramm_of_interest


def deactivate(hausoderraum, module_name):
    mods = hausoderraum.get_modules()
    try:
        mods.remove(module_name)
        hausoderraum.set_modules(mods)
    except ValueError:  # nicht in modules
        pass


def activate(obj, module_name):
    if module_name not in obj.get_modules():
        obj.set_modules(obj.get_modules() + [module_name])
