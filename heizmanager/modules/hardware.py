# -*- coding: utf-8 -*-

from heizmanager.render import render_redirect, render_response
from heizmanager.models import Haus, Etage, Raum, Gateway, GatewayAusgang, Sensor, Regelung, RPi, AbstractSensor, CryptKeys, Luftfeuchtigkeitssensor
import logging
from django.utils.datastructures import MultiValueDictKeyError
from django.http import HttpResponse
import heizmanager.cache_helper as ch
from datetime import datetime, timedelta
from itertools import chain
import pytz
from heizmanager.models import sig_get_outputs_from_regelung, sig_get_all_outputs, sig_get_possible_outputs_from_regelung, sig_get_outputs_from_output, sig_get_all_outputs_from_output, sig_create_output, sig_create_regelung, sig_get_ctrl_devices
import re
import json
from rf.models import RFAktor, RFDevice, RFAusgang
from benutzerverwaltung.decorators import is_admin
import heizmanager.network_helper as nh
import urllib
from heizmanager.getandset import calc_opening
from knx.models import KNXGateway


@is_admin
def index(request, hausid):
    haus = Haus.objects.get(pk=long(hausid))
    berlin = pytz.timezone("Europe/Berlin")
    now = datetime.now(berlin)

    if request.method == "GET" and "overview" in request.GET:
        ret = []

        cb2outs = sig_get_all_outputs_from_output.send(sender="hardware", haus=haus)
        outs = [o for out in [_o[1] for _o in cb2outs] for o in out]
        # logging.warning(outs)
        # -> [(<Gateway: GW EG>, <Raum: WC>, <Regelung: rlr>, <GatewayAusgang: GatewayAusgang object>, [3]), ...]

        def _get_sensor_name(_sensor, prefix=False):
            if _sensor.get_type() == "Sensor":
                return "%s%s" % ("Sensor " if prefix else "", _sensor.description or _sensor.name)
            elif _sensor.get_type() == "VSensor":
                return "%s%s" % ("Virtueller Sensor " if prefix else "", _sensor.description or _sensor.name)
            elif _sensor.get_type() == "RFSensor":
                return "%s%s" % ("Mehrfachsensor " if prefix else "", _sensor.description or _sensor.name)
            elif _sensor.get_type() == "RFAktor":
                if _sensor.type == 'hktControme':
                    return "%s%s" % ("Raumcontroller PRO " if prefix else "", _sensor.description or _sensor.name)
                return "%s%s" % ("Wandthermostat " if prefix else "", _sensor.description or _sensor.name)
            elif _sensor.get_type() == "KNXSensor":
                return "%s%s" % ("Sensor " if prefix else "", _sensor.description or _sensor.group_address)

        cb2ctrldevs = sig_get_ctrl_devices.send(sender="hardware", haus=haus)
        ctrldevs = [c for ctrldev in [cd[1] for cd in cb2ctrldevs] for c in ctrldev]
        ctrldevpings = {}
        max_open = {}
        for ctrldev in ctrldevs:
            # wenn die sensoren offline sind, wie findet man dann raus, ob das gateway autark ist?

            ping = ch.get_gw_ping(ctrldev.name)
            try:
                marker = ctrldev.get_marker()
            except TypeError:
                marker = ctrldev.get_marker(ctrldev.name, ctrldev.get_shortest_wakeup())
            ctrldevpings[ctrldev.name] = (ping, marker)
            if ping is None or ping[0].replace(tzinfo=berlin) < (now - timedelta(seconds=900)):
                r = {
                    'heading': u"%s" % ctrldev.name,
                    'text': [(1, None, u"letzte Übertragung%s" % (ping[0].strftime(u" am %d.%m. um %H:%M") if ping else u": -"))],
                    'status': '<span style="color: #cb0963;">&#x2717;</span>'
                }
                ret.append(r)
            # hier sollten wir noch schauen, ob das geraet ueberhaupt uebertragen soll -> autark/nicht autark!

            if isinstance(ctrldev, Gateway) and "6.00" > ctrldev.version >= "5.00" and ctrldev.version != "nix":
                max_open[ctrldev.name] = ctrldev.get_parameters().get('max_opening', dict((i, 100) for i in range(1, 16)))

        for etage in haus.etagen.all():
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):

                all_good = True

                r = {'heading': u"%s / %s" % (etage.name, raum.name), 'text': []}

                reg = raum.regelung
                sensoren = raum.get_sensoren()

                ausgaenge = []
                for out in outs:
                    if out[2].id == raum.regelung_id:
                        ausgaenge.append(out)  # (gw, raum, reg, gwausgang, ausgangnos)

                if not len(ausgaenge) and not reg.regelung == 'funksollregelung':
                    # reinschreiben: keine Ausgaenge zugeordnet
                    # logging.warning("raum %s hat keinen ausgang!" % r[0])
                    all_good = False
                    r['text'] = [(1, None, u"keine Ausgänge zugeordnet")]
                    r['status'] = '<span style="color: red;">&#x2717;</span>'

                elif not len(ausgaenge) and reg.regelung == "funksollregelung":
                    outstates = []
                    ms = raum.get_mainsensor()
                    if ms and ms.get_type().startswith("RF"):  # batteriestand checken
                        marker = ms.get_marker(use_last_data=False)
                        # if marker == 'yellow':
                        #     last = sensor.get_wert()
                        #     if last and (now-last[1]).total_seconds() < 900:
                        #         marker = 'green'
                        #     else:
                        #         marker = 'red'
                        vals = ch.get_last_vals(ms.controller.name+'_'+ms.name+'_val')
                        battery = vals[0].get('battery', 100) if vals and vals[0] else 100
                        msg = u""
                        if battery is not None and battery <= 20:
                            all_good = False
                            msg = u"(niedriger Batteriestand)"
                        if marker == 'red':
                            all_good = False
                        outstates = [(1, marker, u"Raumtemperatursensor %s %s" % (ms.description or ms.name, msg))]

                    hkts = RFAktor.objects.filter(raum_id=raum.id, type__startswith='hkt')
                    for hkt in hkts:
                        marker = hkt.get_marker(use_last_data=False)
                        vals = ch.get_last_vals(hkt.controller.name+'_'+hkt.name+'_val')
                        battery = vals[0].get('battery', 100) if vals and vals[0] else 100
                        msg = u""
                        if battery is not None and battery <= 20:
                            all_good = False
                            msg = u"(niedriger Batteriestand)"
                        if marker == 'red':
                            all_good = False
                        outstates.append((2 if ms else 1, marker, u"Heizkörperthermostat %s %s" % (hkt.description or hkt.name, msg)))

                    if not ms and not len(hkts):
                        all_good = False

                    r['text'] += outstates
                    r['status'] = '<span style="color: %s;">%s</span>' % ('green' if all_good else 'red', '&#10003;' if all_good else '&#x2717;')

                else:
                    for ausgang in ausgaenge:

                        # regelungsirrelevante sensoren nicht anzeigen. sollte es ja ohnehin nicht so viele geben.
                        if reg.regelung == "zweipunktregelung":
                            # alle ausgaenge werden vom gleichen sensor bedient

                            ms = raum.get_mainsensor()
                            if ms:
                                msg = u""
                                if isinstance(ms, RFDevice):  # batteriestand checken
                                    vals = ch.get_last_vals(ms.controller.name+'_'+ms.name+'_val')
                                    battery = vals[0].get('battery', 100) if vals and vals[0] else 100
                                    if battery is not None and battery <= 20:
                                        all_good = False
                                        msg = u" (niedriger Batteriestand)"

                                if isinstance(ms, RFDevice):
                                    marker = ms.get_marker(use_last_data=False)
                                else:
                                    marker = ms.get_marker()
                                # if marker == 'yellow':
                                #     last = sensor.get_wert()
                                #     if last and (now-last[1]).total_seconds() < 900:
                                #         marker = 'green'
                                #     else:
                                #         marker = 'red'
                                if marker != 'green':  # eigentlich auch yellow, weil yellow bis zu einem tag geht
                                    all_good = False

                                outstates = []
                                if len(ausgang[4]):
                                    if not len(r['text']):
                                        outstates.append((1, marker, u"%s steuert%s:" % (_get_sensor_name(ms, prefix=True), msg)))
                                    for o in ausgang[4]:
                                        if isinstance(ausgang[3], RFAusgang):
                                            device = RFAktor.objects.filter(controller__name=ausgang[0].name, name=o).first()
                                            gwmarker = "red"
                                            lastget = ""
                                            if device is not None:
                                                if device.protocol == "enocean":
                                                    gwmarker = device.get_marker()
                                                    try:
                                                        lastget = ch.get("enocean_relais_%s" % device.name.replace(':', '').lower())[0]
                                                    except:
                                                        pass
                                                elif device.protocol == "zwave":
                                                    gwmarker = ctrldevpings[ausgang[0].name][1]
                                                    try:
                                                        lastget = ch.get("zwave_rfausgang_%s%s" % (device.controller.name, device.name))[0]
                                                    except:
                                                        pass

                                            outstates.append((2, gwmarker, u"Ausgang %s an %s %s auf %s" % (o, "Funkstick", ausgang[0].description or ausgang[0].name, lastget if len(str(lastget)) else "?")))

                                        else:
                                            gwmarker = ctrldevpings[ausgang[0].name][1]  # ausgang[0].get_marker()
                                            lastget = ch.get('lastout_%s_%s' % (ausgang[2].id, o))
                                            if lastget is None:
                                                if gwmarker == 'red':
                                                    all_good = False
                                                if hasattr(ausgang[0], "version") and "6.00" > ausgang[0].version >= "5.00":
                                                    lastget = ""
                                                else:
                                                    lastget = ausgang[2].do_ausgang(o).content
                                                    try:
                                                        lastget = str(calc_opening(lastget, raum.get_betriebsart(), ausgang[0].version if isinstance(ausgang[0], Gateway) else "4.23", o, raum, haus.profil.get().get_gateway_invertouts(ausgang[0].name.lower()), nolog=True))
                                                    except ValueError:
                                                        lastget = ""
                                            else:
                                                lastget = lastget[0]

                                            outstates.append((2, gwmarker, u"Ausgang %s an %s %s auf %s" % (o, "Gateway", ausgang[0].description or ausgang[0].name, lastget if len(str(lastget)) else "?")))

                                else:
                                    # wir haben einen hauptsensor aber keine ausgaenge
                                    # checken, oder der sensor atsensor
                                    hparams = haus.get_module_parameters()
                                    if hparams.get('ruecklaufregelung', dict()).get('atsensor', -1) != ms.get_identifier():
                                        outstates.append((1, marker, u"&#172; %s steuert keinen Ausgang!" % (ms.description or ms.name)))

                                r['text'] += outstates
                            else:
                                all_good = False

                        elif reg.regelung == "ruecklaufregelung":
                            # bei rlonly, ignorems und ms kann man jeweils das gleiche anzeigen.
                            # man muss aber zusaetzlich den ms anzeigen, falls vorhanden
                            # gut wäre auch, wenn die ausgangnummer vom gwausgang zu rlsensorssn passen, das anzuzeigen

                            params = reg.get_parameters()
                            outstates = []
                            ms = raum.get_mainsensor()
                            if ms:
                                if isinstance(ms, RFDevice):
                                    marker = ms.get_marker(use_last_data=False)
                                else:
                                    marker = ms.get_marker()
                                # if marker == 'yellow':
                                #     last = ms.get_wert()
                                #     if last and (now-last[1]).total_seconds() < 900:
                                #         marker = 'green'
                                #     else:
                                #         marker = 'red'

                                msg = u""
                                if ms and ms.get_type().startswith("RF"):  # batteriestand checken
                                    vals = ch.get_last_vals(ms.controller.name+'_'+ms.name+'_val')
                                    battery = vals[0].get('battery', 100) if vals and vals[0] else 100
                                    if battery is not None and battery <= 20:
                                        all_good = False
                                        msg = u"(niedriger Batteriestand)"

                                # schauen, ob regelungsrelevant. wenn ja und offline, anzeigen
                                ignorems = params.get('ignorems', False)
                                if marker != 'green' and not ignorems:
                                    all_good = False
                                if not len(r['text']):
                                    if ignorems:
                                        outstates = [(1, marker, u"Raumtemperatursensor: %s nicht regelungsrelevant %s" % (_get_sensor_name(ms, prefix=True), msg))]
                                    else:
                                        outstates = [(1, marker, u"Raumtemperatursensor: %s %s" % (_get_sensor_name(ms, prefix=True), msg))]

                            for sensor in sensoren:
                                if sensor.mainsensor:
                                    continue
                                if sensor.get_identifier() in params['rlsensorssn']:
                                    marker = sensor.get_marker()
                                    # if marker == 'yellow':
                                    #     last = sensor.get_wert()
                                    #     if last and (now-last[1]).total_seconds() < 900:
                                    #         marker = 'green'
                                    #     else:
                                    #         marker = 'red'
                                    if len(params['rlsensorssn'][sensor.get_identifier()]):
                                        if marker != 'green':
                                            all_good = False

                                        outstates.append((1, marker, u"Rücklaufsensor %s%s" % (_get_sensor_name(sensor, prefix=False), u" steuert" if len(params['rlsensorssn'][sensor.get_identifier()]) else u"")))

                                        for out in sorted(params['rlsensorssn'][sensor.get_identifier()]):
                                            if isinstance(ausgang[3], RFAusgang):
                                                device = RFAktor.objects.filter(controller__name=ausgang[0].name, name=out).first()
                                                gwmarker = "red"
                                                lastget = []
                                                if device is not None:
                                                    if device.protocol == "enocean":
                                                        gwmarker = device.get_marker()
                                                        try:
                                                            lastget = str(ch.get("enocean_relais_%s" % device.name.replace(':', '').lower())[0])
                                                        except:
                                                            lastget = ""
                                                    elif device.protocol == "zwave":
                                                        gwmarker = ctrldevpings[ausgang[0].name][1]
                                                        try:
                                                            lastget = str(ch.get("zwave_rfausgang_%s%s" % (device.controller.name, device.name))[0])
                                                        except:
                                                            lastget = ""

                                                    outstates.append((2, gwmarker, u"Ausgang %s an %s %s auf %s" % (out, "Funkstick", ausgang[0].description or ausgang[0].name, lastget if len(lastget) else "?")))

                                            else:
                                                if out not in ausgang[4]:
                                                    continue

                                                gwmarker = ctrldevpings[ausgang[0].name][1]

                                                lastget = ch.get('lastout_%s_%s' % (ausgang[2].id, out))
                                                if lastget is None:
                                                    if gwmarker == 'red':
                                                        all_good = False
                                                    if (isinstance(ausgang[0], Gateway) and "6.00" > ausgang[0].version >= "5.00") or (isinstance(ausgang[0], KNXGateway) and ausgang[3].is010v()):
                                                        lastget = ""
                                                    else:
                                                        lastget = ausgang[2].do_ausgang(out).content
                                                        try:
                                                            lastget = str(calc_opening(lastget, raum.get_betriebsart(), ausgang[0].version if isinstance(ausgang[0], Gateway) else "4.23", out, raum, haus.profil.get().get_gateway_invertouts(ausgang[0].name.lower()), nolog=True))
                                                        except ValueError:
                                                            lastget = ""
                                                else:
                                                    lastget = lastget[0]

                                                outstates.append((2, gwmarker, u"Ausgang %s an %s %s auf %s" % (out, "Gateway", ausgang[0].description or ausgang[0].name, lastget if len(lastget) else "?")))

                                    else:

                                        # das ist zwar gut, deckt aber nicht den fall ab, dass ein rücklaufsensor
                                        # in einem vsensor steckt, der stattdessen den ausgang abdeckt

                                        # all_good = False
                                        # marker = 'red'  # das ist keine so gute idee, weil dann rot zwei sachen anzeigen kann
                                        outstates.append((1, marker, u"&#172; Rücklaufsensor %s steuert keinen Ausgang!" % (sensor.description or sensor.name)))

                            r['text'] += outstates

                        elif reg.regelung == "funksollregelung":

                            outstates = []
                            ms = raum.get_mainsensor()
                            if ms and ms.get_type().startswith("RF"):  # batteriestand checken
                                marker = ms.get_marker(use_last_data=False)
                                # if marker == 'yellow':
                                #     last = sensor.get_wert()
                                #     if last and (now-last[1]).total_seconds() < 900:
                                #         marker = 'green'
                                #     else:
                                #         marker = 'red'
                                vals = ch.get_last_vals(ms.controller.name+'_'+ms.name+'_val')
                                battery = vals[0].get('battery', 100) if vals and vals[0] else 100
                                msg = u""
                                if battery is not None and battery <= 20:
                                    all_good = False
                                    msg = u"(niedriger Batteriestand)"
                                if marker == 'red':
                                    all_good = False
                                outstates = [(1, marker, u"Raumtemperatursensor %s %s" % (ms.description or ms.name, msg))]

                            hkts = RFAktor.objects.filter(raum_id=raum.id, type__startswith='hkt')
                            for hkt in hkts:
                                marker = hkt.get_marker(use_last_data=False)
                                vals = ch.get_last_vals(hkt.controller.name+'_'+hkt.name+'_val')
                                battery = vals[0].get('battery', 100) if vals and vals[0] else 100
                                msg = u""
                                if battery is not None and battery <= 20:
                                    all_good = False
                                    msg = u"(niedriger Batteriestand)"
                                if marker == 'red':
                                    all_good = False
                                outstates.append((2 if ms else 1, marker, u"Heizkörperthermostat %s %s" % (hkt.description or hkt.name, msg)))

                            if not ms and not len(hkts):
                                all_good = False

                            r['text'] += outstates

                        else:
                            # das sollte nicht passieren
                            r['text'] = []

                        # werden RFAusgänge korrekt/zureichend behandelt? Testen!

                        r['status'] = '<span style="color: %s;">%s</span>' % ('green' if all_good else 'red', '&#10003;' if all_good else '&#x2717;')

                ret.append(r)

        # pl auch anzeigen?

        logging.warning(ret)
        return HttpResponse(json.dumps(ret), content_type="application/json")

    elif request.method == "GET":
        import importlib

        hwlinks = []
        mods = ['heizmanager.modules.setup_rpi', 'rf.views', 'knx.views', 'wifi.views']
        for modulename in mods:
            module = importlib.import_module(modulename.strip())
            link = module.get_global_settings_link(request, haus)
            if link is not None:
                hwlinks.append(link)

        return render_response(request, "m_hardware.html", {"haus": haus, 'hwlinks': hwlinks})


@is_admin
def raeume_einrichtung(request, hausid):
    haus = Haus.objects.get(pk=long(hausid))
    return render_response(request, "m_install_er.html", {"haus": haus})


@is_admin
def raum_editieren(request, hausid, raumid=None):
    haus = Haus.objects.get(pk=long(hausid))
    if request.method == "GET":

        if 'rdel' in request.GET:
            Raum.objects.get(pk=long(request.GET['rdel'])).delete()
            return render_redirect(request, "/m_setup/%s/hardware/er/" % hausid)

        else:
            raum = Raum()
            if raumid:
                raum = Raum.objects.get(pk=long(raumid))
            etagen = Etage.objects.filter(haus=haus).order_by("-position")
            icons = ['bett', 'dusche', 'haus', 'muetze', 'punkte', 'sofa', 'spiel', 'desk', 'esstisch', 'fernseher1',
                     'fernseher2', 'feuer', 'garage', 'messergabel', 'putzenb', 'putzeng', 'schuhe', 'sessel',
                     'stufen', 'toilette', 'waschmaschine']
            return render_response(request, "m_install_erc.html",
                                   {"haus": haus, "etagen": etagen, "raum": raum, 'icons': icons})

    if request.method == "POST" and request.user.userprofile.get().role != 'K':
        logging.warning(request.POST)
        raumname = request.POST['raumname']
        try:
            etagenname = request.POST['etagenid']
        except MultiValueDictKeyError:
            raumerror = "Bitte Etage angeben."
            etagen = Etage.objects.filter(haus=haus)
            raum = Raum(name=raumname)
            return render_response(request, "m_install_erc.html",
                                   {"haus": haus, "etagen": etagen, "raum": raum, "raumerror": raumerror})
        etage = Etage.objects.get(pk=long(etagenname), haus=haus)

        try:
            icon = request.POST['icon']
        except MultiValueDictKeyError:
            icon = "punkte"

        if not len(raumname.strip()):
            etagen = Etage.objects.filter(haus=haus)
            raumerror = "Bitte Raumnamen eingeben."
            if raumid:
                raum = Raum.objects.get(pk=long(raumid))
            else:
                raum = Raum(etage=etage, icon=icon)
            return render_response(request, "m_install_erc.html",
                                   {"haus": haus, "etagen": etagen, "raum": raum, "raumerror": raumerror})
        raumnamen = [r[0] for r in Raum.objects.filter_for_user(request.user, etage_id=etage.id).values_list('name')]
        if raumname in raumnamen and not raumid:
            etagen = Etage.objects.filter(haus=haus)
            raumerror = "Raumname in dieser Etage bereits verwendet."
            raum = Raum(etage=etage, icon=icon)
            return render_response(request, "m_install_erc.html",
                                   {"haus": haus, "etagen": etagen, "raum": raum, "raumerror": raumerror})

        if raumid:
            raum = Raum.objects.get(pk=long(raumid))
            raum.name = raumname
            raum.etage = etage
        else:
            raum = Raum(name=raumname, etage=etage)
            lastroom = Raum.objects.filter_for_user(request.user, etage_id=etage.id).order_by('-position')
            position = lastroom[0].position+1 if len(lastroom) else 1
            raum.position = position
            raum.solltemp = 21.0
            params = dict()
            params['wetter'] = dict()
            params['wetter']['wetter_params'] = '5,5'
            params['zeitschalter'] = dict()
            params['zeitschalter']['offset'] = 0.0
            raum.module_parameters = str(params)
            raum.modules = 'wetter'
            reg = Regelung(regelung='zweipunktregelung', parameter=str({'delta': 0.0, 'active': True}))
            reg.save()
            raum.regelung = reg
        raum.icon = icon
        raum.save()
        return render_redirect(request, "/m_setup/%s/hardware/er/" % haus.id)


@is_admin
def etage_editieren(request, hausid, etagenid=None):
    haus = Haus.objects.get(pk=long(hausid))
    if request.method == "GET":

        if 'edel' in request.GET:
            Etage.objects.get(pk=long(request.GET['edel'])).delete(request)
            return render_redirect(request, '/m_setup/%s/hardware/er/' % hausid)

        else:
            etage = Etage()
            if etagenid:
                etage = Etage.objects.get(pk=long(etagenid))
            return render_response(request, "m_install_erc.html", {"haus": haus, "etage": etage})

    if request.method == "POST" and request.user.userprofile.get().role != 'K':
        etagenname = request.POST['etagenname']
        if len(Etage.objects.filter(name=etagenname, haus=haus)) and not etagenid:
            etagenerror = "Etage mit diesem Namen existiert bereits."
            etagen = Etage.objects.filter(haus=haus)
            return render_response(request, "m_install_erc.html",
                                   {"haus": haus, "etagen": etagen, "etagenerror": etagenerror,
                                    "etagenname": etagenname})
        if not len(etagenname.strip()):
            etagen = Etage.objects.filter(haus=haus)
            etage = Etage()
            if etagenid:
                etage = Etage.objects.get(pk=long(etagenid))
            return render_response(request, "m_install_erc.html", {"haus": haus, "etage": etage, "etagen": etagen})
        else:
            lastfloor = haus.etagen.order_by('-position')
            position = lastfloor[0].position+1 if len(lastfloor) else 1
            if etagenid:
                e = Etage.objects.get(pk=long(etagenid))
                e.name = etagenname
            else:
                e = Etage(name=etagenname, haus=haus, position=position)
            e.save()
            return render_redirect(request, "/m_setup/%s/hardware/er/" % haus.id)


@is_admin
def raeume_anzeigen(request, hausid):
    import json
    haus = Haus.objects.get(pk=long(hausid))
    etagen = Etage.objects.filter(haus=haus)
    ret = []
    for etage in etagen:
        raeume = Raum.objects.filter_for_user(request.user, etage_id=etage.id).order_by('position').values_list('id', 'name', 'icon', 'position')
        if len(set([r[3] for r in raeume])) < len(raeume):
            for i, raum in enumerate(raeume):
                r = Raum.objects.get_for_user(request.user, pk=raum[0])
                r.position = i+1
                r.save()
            raeume = Raum.objects.filter_for_user(request.user, etage_id=etage.id).order_by('position').values_list('id', 'name', 'icon', 'position')
        ret.append({str(etage.id)+'##'+etage.name: list(raeume)})
    return HttpResponse(json.dumps(ret), content_type="application/json")


@is_admin
def gateway_einrichtung(request, hausid):
    haus = Haus.objects.get(pk=long(hausid))
    if request.method == "GET":
        return render_response(request, "m_install_gw.html", {"haus": haus})


@is_admin
def gateways_anzeigen(request, hausid):
    import json
    from django.core.serializers.json import DjangoJSONEncoder
    haus = Haus.objects.get(pk=long(hausid))
    gateways = Gateway.objects.filter(haus=haus)
    a = list()
    fbhgw = list()
    hrgw = list()
    for gateway in gateways:
        last = ch.get_gw_ping(gateway.name)
        marker = gateway.get_marker()
        if last:
            last = last[0].strftime("am %d.%m. um %H:%M")
        if gateway.is_heizraumgw():
            hrgw.append((gateway.id, gateway.name, gateway.description) + (last,) + (marker,))
        else:
            fbhgw.append((gateway.id, gateway.name, gateway.description) + (last,) + (marker,))
    a.append(sorted(list(fbhgw), reverse=True))
    a.append(sorted(list(hrgw), reverse=True))
    return HttpResponse(json.dumps(a, cls=DjangoJSONEncoder), content_type='application/json')


@is_admin
def gateway_editieren(request, hausid, gwid=None):
    haus = Haus.objects.get(pk=long(hausid))
    if request.method == "GET":

        if 'gwdel' in request.GET:
            if request.user.userprofile.get().role != 'K':
                Gateway.objects.get(pk=long(request.GET['gwdel'])).delete()
            return render_redirect(request, '/m_setup/%s/hardware/gw/' % hausid)

        else:
            gw = Gateway()
            if gwid:
                gw = Gateway.objects.get(pk=long(gwid))
            install_mode_set = haus.profil.get().get_module_parameters().get('install_mode_set')
            if install_mode_set and install_mode_set > (datetime.now() - timedelta(seconds=1800)).isoformat():
                install_mode = True
            else:
                install_mode = False
            invert = haus.profil.get().get_gateway_invertouts(gw.name) if gwid else False
            params = gw.get_parameters()
            max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))
            max_open = [(k, max_open[k]) for k in sorted(max_open.keys())]
            rb_min = params.get('regelbereich_min', 0)
            rb_max = params.get('regelbereich_max', 100)
            ip = None
            if gw.name.startswith(("b8", "dc")):
                data = ch.get("mischer_%s" % gw.name)
                if data is not None and 'gateway_ip' in data:
                    ip = data['gateway_ip']
            return render_response(request, "m_install_gwc.html", {"haus": haus, "gw": gw,
                                                                   'install_mode': install_mode, 'version': gw.version,
                                                                   'invert': invert, "max_open": max_open, "ip": ip,
                                                                   'rb_min': rb_min, 'rb_max': rb_max})

    if request.method == "POST" and request.user.userprofile.get().role != 'K':
        name = request.POST['name'].strip()
        description = request.POST['description']
        install_mode = request.POST['install_mode'] if 'install_mode' in request.POST else '0'
        invert = request.POST['invert'] if 'invert' in request.POST else False

        if gwid:
            gw = Gateway.objects.get(pk=long(gwid))
        else:
            gw = Gateway()
            gw.parameters = "{}"
        gw.name = name.lower()
        gw.description = description
        gw.haus = haus
        if not len(RPi.objects.all()):
            macaddr = nh.get_mac()
            from rpi.aeskey import key
            from rpi.verification_code import verification_code
            ck = CryptKeys()
            ck.aeskey = key
            ck.save()
            rpi = RPi(crypt_keys=ck, verification_code=verification_code, name=macaddr, upgrade_version="",
                      local_address="", global_address="", haus=haus)
            rpi.save()
        gw.rpi = RPi.objects.all()[0]

        if install_mode != '0':
            params = haus.profil.get().get_module_parameters()
            params['install_mode_set'] = datetime.now().isoformat()
            haus.profil.get().set_module_parameters(params)
            haus.profil.get().set_gateway_delay(1)
        else:
            params = haus.profil.get().get_module_parameters()
            params['install_mode_set'] = (datetime.now() - timedelta(seconds=1800)).isoformat()
            haus.profil.get().set_module_parameters(params)
            haus.profil.get().set_gateway_delay(180)

        if not gwid:
            ret = gw.create_cryptkey()
            if ret is not None:
                return render_response(request, "m_install_gwc.html",
                                       {"haus": haus, "gw": gw, "gwerror": ret})

        try:
            gw.save()
        except ValueError as e:
            return render_response(request, "m_install_gwc.html", {"haus": haus, "gw": gw, "gwerror": e})

        if not gw.is_heizraumgw():
            haus.profil.get().set_gateway_invertouts(gw.name, invert)
        ch.set_gateway_update(gw.name)

        max_open_change = False
        if "6.00" > gw.version >= "5.00" and gw.version != "nix":
            max_open = dict()
            for i in range(1, 16):
                mo = request.POST.get("out%s" % i, 100)
                max_open[i] = mo
            params = gw.get_parameters()
            if params.get('max_opening') != max_open:
                max_open_change = True
            params["max_opening"] = max_open
            rb_min = max(0, min(100, int(request.POST.get('regelbereich_min', 0))))
            rb_max = max(0, min(100, int(request.POST.get('regelbereich_max', 100))))
            if rb_min > rb_max: rb_min = rb_max
            params['regelbereich_min'] = rb_min
            params['regelbereich_max'] = rb_max
            gw.set_parameters(params)

        if "6.00" > gw.version >= "5.00" and gw.version != "nix" and max_open_change:
            return render_redirect(request, "/m_setup/%s/hardware/gwedit/%s/" % (haus.id, gw.id))
        else:
            return render_redirect(request, "/m_setup/%s/hardware/gw/" % haus.id)


@is_admin
def sensoren_einrichtung(request, hausid):
    haus = Haus.objects.get(pk=long(hausid))
    if request.method == "GET":
        sort = request.GET.get('sort', 'rooms')
        return render_response(request, "m_install_s.html", {"haus": haus, "sort": sort})


@is_admin
def sensoren_anzeigen(request, hausid):
    import json
    from django.core.serializers.json import DjangoJSONEncoder

    sort = request.GET.get('sort')
    if not sort:
        return HttpResponse(json.dumps([]), content_type='application/json')

    haus = Haus.objects.get(pk=long(hausid))
    gwdelay = haus.profil.get().get_gateway_delay()
    if int(gwdelay) == 1:
        gwdelay = 90

    liniendict = {
        1: "Klemme 32",
        2: "Klemme 33",
        3: "Klemme 29",
        4: "Klemme 30",
        5: "Klemme 31"
    }

    liniendict_hrgw = {
        -1: "-",
        0: "-", 1: "1", 2: "-", 3: "-", 4: "2", 5: "3", 6: "4", 7: "5"
    }

    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    etagendict = {}
    raumdict = {}
    regdict = {}
    eundr = []
    for etage in haus.etagen.all():
        etagendict[etage.id] = etage.name
        for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id).values_list("id", "name", "etage_id", "regelung_id"):
            raumdict[raum[0]] = (raum[1], raum[2], raum[3])
            regdict[raum[3]] = Regelung.objects.get(pk=long(raum[3]))
            eundr.append(u"%s / %s" % (etage.name, raum[1]))
    eundr.append("Heizraumsensoren")
    eundr.append("unzugeordnet")
    eundr.reverse()

    gws = Gateway.objects.all()
    gwdict = dict((gw.id, gw) for gw in gws)
    gwdict[None] = "ohne Gateway"

    liniensort = ["unzugeordnet", "Klemme 31", "Klemme 30", "Klemme 29", "Klemme 33", "Klemme 32"]
    liniensort_hrgw = ["unzugeordnet", "-", "1", "2", "3", "4", "5"]
    if sort == 'rooms':
        sensoren = [{er: []} for er in eundr]
    elif sort == 'lines':
        sensoren = [{"%s - %s" % (gw, i): []} for gw in gws for i in liniensort if not gw.is_heizraumgw()]
        sensoren += [{"%s - %s" % (gw, i): []} for gw in gws for i in liniensort_hrgw if gw.is_heizraumgw()]
        sensoren.append({"ohne Gateway": []})
        l2idx = dict((d.keys()[0], i) for i, d in enumerate(sensoren))
    elif sort == 'last':
        sensoren = [{"%s - %s" % (gw, i): []} for gw in gws for i in liniensort if not gw.is_heizraumgw()]
        sensoren += [{"%s - %s" % (gw, i): []} for gw in gws for i in liniensort_hrgw if gw.is_heizraumgw()]
        sensoren.append({"ohne Gateway": []})
        l2idx = dict((d.keys()[0], i) for i, d in enumerate(sensoren))
    else:
        return HttpResponse(json.dumps([]), content_type='application/json')

    for sensor in list(chain(haus.sensor_related.values_list("id", "name", "haus_id", "linie", "raum_id", "description", "gateway_id", "mainsensor"),
                             haus.luftfeuchtigkeitssensor_related.values_list("id", "name", "haus_id", "linie", "raum_id", "description", "gateway_id", "mainsensor"))):
        if sensor[6] is not None and sensor[6] > 0 and gwdict[sensor[6]].is_heizraumgw():
            last, marker = _get_last_and_marker(sensor[1], liniendict_hrgw[sensor[3]] if sensor[3] else "", now, gwdelay)
        else:
            last, marker = _get_last_and_marker(sensor[1], liniendict[sensor[3]] if sensor[3] and 0 < sensor[3] < 6 else "", now, gwdelay)
        if 'pt1000' in sensor[1]:
            sname = 'PT1000 an %s' % sensor[1].split('#')[0]
        elif 'ui10' in sensor[1]:
            sname = 'Digitaleingang 1 an %s' % sensor[1].split('#')[0]
        elif 'ui20' in sensor[1]:
            sname = 'Digitaleingang 2 an %s' % sensor[1].split('#')[0]
        else:
            sname = sensor[1]

        typ = ""
        if sensor[7]:
            typ = "RT"
        elif sensor[4] and regdict[raumdict[sensor[4]][2]].regelung == "ruecklaufregelung" and "Sensor*%s" % sensor[0] in regdict[raumdict[sensor[4]][2]].get_parameters()['rlsensorssn']:
            typ = "RL"
        elif sensor[1].startswith("26_"):
            typ = "LF"

        if sensor[4]:
            sd = {
                sname: [
                    sensor[0],
                    sensor[4],
                    u"%s / %s%s %s" % (
                        etagendict[raumdict[sensor[4]][1]],
                        raumdict[sensor[4]][0],
                        " (%s)" % typ if typ else "",
                        u"- '%s'" % sensor[5] if sensor[5] else ""),
                    last,
                    marker,
                ]
            }
        elif sensor[5]:
            sd = {
                sname: [
                    sensor[0],
                    None,
                    u"- / - - '%s'" % sensor[5],
                    last,
                    marker
                ]
            }
        else:
            sd = {
                sname: [
                    sensor[0] if 'pt1000' not in sensor[1] else '',
                    None,
                    "- / -",
                    last,
                    marker
                ]
            }

        if sort == 'rooms':
            if sensor[4]:
                rstr = u"%s / %s" % (etagendict[raumdict[sensor[4]][1]], raumdict[sensor[4]][0])
                sensoren[eundr.index(rstr)][rstr].append(sd)
            elif not isinstance(gwdict[sensor[6]], str) and gwdict[sensor[6]].is_heizraumgw():
                sensoren[1]['Heizraumsensoren'].append(sd)
            else:
                sensoren[0]["unzugeordnet"].append(sd)

        elif sort == 'lines' or sort == 'last':
            if sensor[3]:
                if sensor[6] is None or sensor[6] not in gwdict:
                    linienname = "ohne Gateway"
                else:
                    if sensor[6] is not None and gwdict[sensor[6]].is_heizraumgw():
                        linienname = liniendict_hrgw[sensor[3]] if sensor[3] else "-"
                    else:
                        linienname = liniendict[sensor[3]] if sensor[3] and 0 < sensor[3] < 6 else ""
                    linienname = "%s - %s" % (gwdict[sensor[6]], linienname)
            else:
                if sensor[6] is None or sensor[6] not in gwdict:
                    linienname = "ohne Gateway"
                else:
                    linienname = "%s - unzugeordnet" % gwdict[sensor[6]]
            sensoren[l2idx[linienname]][linienname].append(sd)

    for i, rdata in enumerate(sensoren):
        for rstr, sensors in rdata.items():
            if sort == 'last':
                rdata[rstr] = sorted(sensors, key=lambda s: s.values()[0][3], reverse=True)
            else:
                rdata[rstr] = sorted(sensors, key=lambda s: s.values()[0][2], reverse=True)

    return HttpResponse(json.dumps(sensoren, cls=DjangoJSONEncoder), content_type='application/json')


def _get_last_and_marker(ssn, linie, now, gwdelay):

    last = ch.get_last_temp(ssn)
    marker = 'red'

    if last:

        if len(last) >= 4 and last[1] and last[3] and \
                (last[1]-last[3]).seconds < gwdelay*1.5 and (now-last[1]).seconds < gwdelay*1.5:
            marker = 'green'
        elif len(last) >= 2 and last[1] and (now-last[1]).days < 1:
            marker = 'yellow'

        temp = last[0]
        last = last[1].strftime("am %d.%m. um %H:%M")
        if ssn.startswith("26_"):
            last += " mit %s%%" % temp
            if linie:
                last += " auf Linie %s" % linie
        elif "ui" in ssn:
            last += " mit %d" % temp
        else:
            if temp == 85 or temp is None:
                last = u"Fehler! Bitte Leitungen prüfen!"
            else:
                last += " mit %.2f&deg;" % temp
                if linie:
                    last += " auf Linie %s" % linie

    return last, marker


@is_admin
def sensor_editieren(request, hausid, sid=None):

    haus = Haus.objects.get(pk=long(hausid))
    if request.method == "GET":

        if 'sdel' in request.GET and request.user.userprofile.get().role != 'K':
            err = False
            sensor_name = urllib.unquote(request.GET.get('sdel', '')).decode('utf8')
            if sensor_name.startswith('26_'):
                try:
                    sensor = Luftfeuchtigkeitssensor.objects.get(name=sensor_name)
                except Luftfeuchtigkeitssensor.DoesNotExist:
                    err = True
            else:
                try:
                    sensor = Sensor.objects.get(name=sensor_name)
                except Sensor.DoesNotExist:
                    err = True

            if not err and sensor.raum:

                if sensor.mainsensor and sensor == sensor.raum.get_mainsensor():
                    cb2outs = sig_get_outputs_from_output.send(
                        sender='hardware', raum=sensor.raum, regelung=sensor.raum.regelung
                    )
                    outs = [o for out in [_o[1] for _o in cb2outs] for o in out]
                    for out in outs:
                        if len(out[4]):
                            err = True

                else:
                    cb2outs = sig_get_outputs_from_regelung.send(
                        sender='hardware',
                        haus=haus,
                        raum=sensor.raum,
                        regelung=sensor.raum.regelung
                    )
                    outs = [o for out in [_o[1].items() for _o in cb2outs] for o in out]
                    for ctrldev, out in outs:
                        for o in out:
                            if o[1] and o[2] == sensor:
                                err = True

            if err:
                hparams = haus.get_module_parameters()
                atsensor = False
                if hparams.get('ruecklaufregelung', dict()).get('atsensor', -1) == sensor.get_identifier():
                    atsensor = True
                error = "Sensor ist regelungsrelevant. Bitte entfernen Sie zuerst die zugeordneten Ausgänge."
                eundr = []
                for etage in haus.etagen.all():
                    e = {etage: []}
                    for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                        e[etage].append(raum)
                    eundr.append(e)
                return render_response(request, "m_install_sc.html",
                                       {"haus": haus, "s": sensor, "etagen": haus.etagen.all(),
                                        "gateways": haus.gateway_related.all(),
                                        'rlsensor': True if not sensor.mainsensor else False, 'atsensor': atsensor,
                                        'serror': error, "eundr": eundr})
            else:
                sensor.delete()
                return render_redirect(request, '/m_setup/%s/hardware/s/' % hausid)

        else:
            s = Sensor()
            rlsensor = False
            uinum = None
            if sid:
                if sid.startswith('26_'):
                    try:
                        s = Luftfeuchtigkeitssensor.objects.get(name=sid)
                    except Luftfeuchtigkeitssensor.DoesNotExist:
                        pass
                elif sid.startswith("PT1000"):
                    try:
                        s = Sensor.objects.get(name=sid.split('_')[2]+'#'+sid.split('_')[0].lower())
                    except Sensor.DoesNotExist:
                        pass

                elif sid.startswith("Digitaleingang"):
                    # example Digitaleingang_2_an_b8-27-eb-54-d4-9a
                    uinum = sid.split('_')[1]
                    s_name = "{mac}#ui{uinum}0".format(mac=sid.split('_')[-1], uinum=uinum)
                    try:
                        s = Sensor.objects.get(name=s_name.lower())
                    except Sensor.DoesNotExist:
                        pass

                else:
                    try:
                        s = Sensor.objects.get(name=sid)
                        if s.raum.regelung.regelung == "ruecklaufregelung" and \
                                s.get_identifier() in s.raum.regelung.get_parameters()['rlsensorssn']:
                            rlsensor = True
                    except (AttributeError, Sensor.DoesNotExist, Exception):
                        pass
            hparams = haus.get_module_parameters()
            atsensor = False
            if hparams.get('ruecklaufregelung', dict()).get('atsensor', -1) == s.get_identifier():
                atsensor = True

            # sensortypen kann man hier eigentlich auch aus den regelungen holen
            eundr = []
            for etage in haus.etagen.all():
                e = {etage: []}
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    e[etage].append(raum)
                eundr.append(e)
            return render_response(request, "m_install_sc.html",
                                   {"haus": haus, "s": s, "etagen": haus.etagen.all(),
                                    "gateways": haus.gateway_related.all(), 'rlsensor': rlsensor, 'atsensor': atsensor,
                                    'eundr': eundr, 'uinum': uinum})

    if request.method == "POST" and request.user.userprofile.get().role != 'K':

        try:
            offset = float(request.POST['sensor_offset'].replace(',', '.'))
        except (ValueError, MultiValueDictKeyError):
            offset = 0.0

        if sid:
            if 'PT1000' in sid:
                try:
                    s = Sensor.objects.get(name=sid.split('_')[2]+'#'+sid.split('_')[0].lower())
                except Sensor.DoesNotExist:
                    return render_redirect(request, '/m_setup/%s/hardware/s/' % hausid)
                s.offset = offset
                s.description = request.POST.get('description', '')
                s.save()
                return render_redirect(request, '/m_setup/%s/hardware/s/' % hausid)
            if 'Digitaleingang' in sid:
                try:
                    s = Sensor.objects.get(name=request.POST.get('ssn', ''))
                    s.description = request.POST.get('description', '')
                    s.save()
                    return render_redirect(request, '/m_setup/%s/hardware/s/' % hausid)
                except Sensor.DoesNotExist:
                    return render_redirect(request, '/m_setup/%s/hardware/s/' % hausid)

            try:
                alter_sensor = Sensor.objects.get(name=sid)
                neuer_sensor = Sensor.objects.get(name=sid)
            except Sensor.DoesNotExist:
                try:
                    neuer_sensor = Luftfeuchtigkeitssensor.objects.get(name=sid)
                    alter_sensor = Luftfeuchtigkeitssensor.objects.get(name=sid)
                except Luftfeuchtigkeitssensor.DoesNotExist:
                    alter_sensor = None
        else:
            alter_sensor = None
            if request.POST.get('ssn', '').startswith("26_"):
                neuer_sensor = Luftfeuchtigkeitssensor()
            else:
                neuer_sensor = Sensor()

        neuer_sensor.name = request.POST.get('ssn', '')
        neuer_sensor.description = request.POST.get('description', '')
        neuer_sensor.raum = Raum.objects.get(pk=long(request.POST['raumid'])) if request.POST['raumid'] != '0' else None
        neuer_sensor.gateway = Gateway.objects.get(pk=long(request.POST['gateway'])) if request.POST['gateway'] != '0' else None
        neuer_sensor.offset = offset
        neuer_sensor.haus = haus

        if isinstance(neuer_sensor, Luftfeuchtigkeitssensor):
            if alter_sensor and alter_sensor.get_type() != neuer_sensor.get_type():
                error = u"Unzulässige Änderung des Sensortyps zwischen Luftfeuchtigkeit und Temperatur."
                return render_response(request, "m_install_sc.html",
                                       {"haus": haus, "s": neuer_sensor, "etagen": haus.etagen.all(),
                                        "gateways": haus.gateway_related.all(), "serror": error, "rlsensor": False,
                                        "atsensor": False})
            error = ''
            try:
                neuer_sensor.save()
            except ValueError as e:
                error = u"%s<br/>" % str(e)

            if len(error):
                return render_response(request, "m_install_sc.html",
                                       {"haus": haus, "s": neuer_sensor, "etagen": haus.etagen.all(),
                                        "gateways": haus.gateway_related.all(), 'serror': error,
                                        'rlsensor': False, 'atsensor': False})
            else:
                return render_redirect(request, '/m_setup/%s/hardware/s/' % hausid)

        error = ''

        if neuer_sensor.raum and neuer_sensor.raum.get_mainsensor() and neuer_sensor.raum.get_mainsensor() != neuer_sensor and request.POST.get('sensortyp') == 'rt':
            ms = neuer_sensor.raum.get_mainsensor()
            text = u'Raum&auml;nderung nicht m&ouml;glich, weil %s bereits %s als Raumtemperatursensor' \
                   u' zugewiesen bekommen hat. Klicken Sie hier, um diesen Sensor zu bearbeiten.' \
                   % (u"%s/%s" % (neuer_sensor.raum.etage, neuer_sensor.raum), u"%s %s" % (ms.name, ms.description))
            link = ms.get_link()
            error = link % text
            return render_response(request, "m_install_sc.html",
                                   {"haus": haus, "s": neuer_sensor, "etagen": haus.etagen.all(),
                                    "gateways": haus.gateway_related.all(), "serror": error, 'rlsensor': False,
                                    'atsensor': False})

        err = False
        # + pruefen ob alter_sensor ausgaenge hat
        # + wenn ja: pruefen ob neuer_sensor die gleichen ausgaenge hat

        as_has_outs = False
        if alter_sensor and alter_sensor.raum \
                and ((alter_sensor == alter_sensor.raum.get_mainsensor() and alter_sensor.raum.get_regelung().get_ausgang() is not None)
                     or (alter_sensor.get_identifier() in alter_sensor.raum.get_regelung().get_parameters().get('rlsensorssn', dict())
                         and len(alter_sensor.raum.get_regelung().get_parameters()['rlsensorssn'][alter_sensor.get_identifier()]))):
            as_has_outs = True

        # zur sicherheit doppelt?
        if alter_sensor and alter_sensor.raum:
            if alter_sensor.mainsensor and alter_sensor == alter_sensor.raum.get_mainsensor():
                cb2outs = sig_get_outputs_from_output.send(
                    sender='hardware', raum=alter_sensor.raum, regelung=alter_sensor.raum.regelung
                )
                outs = [o for out in [_o[1] for _o in cb2outs] for o in out]
                for out in outs:
                    if len(out[4]):
                        as_has_outs = True

            else:
                cb2outs = sig_get_outputs_from_regelung.send(
                    sender='hardware',
                    haus=haus,
                    raum=alter_sensor.raum,
                    regelung=alter_sensor.raum.regelung
                )
                outs = [o for out in [_o[1].items() for _o in cb2outs] for o in out]
                for ctrldev, out in outs:
                    for o in out:
                        if o[1] and o[2] == alter_sensor:
                            as_has_outs = True

        if as_has_outs and (alter_sensor.raum != neuer_sensor.raum or (alter_sensor.gateway_id != neuer_sensor.gateway_id and alter_sensor.gateway) or alter_sensor.name != neuer_sensor.name):
            err = True

        if as_has_outs and \
                ((alter_sensor.mainsensor and request.POST.get('sensortyp') != 'rt')
                 or (alter_sensor.get_identifier() in alter_sensor.raum.get_regelung().get_parameters().get('rlsensorssn', dict()) and request.POST.get('sensortyp') != 'rl')):
            err = True

        if as_has_outs and \
                request.POST.get('sensortyp') == 'rl' and (neuer_sensor.raum is None or neuer_sensor.raum.regelung.regelung != 'ruecklaufregelung'):
            err = True

        if err:
            hparams = haus.get_module_parameters()
            atsensor = False
            if hparams.get('ruecklaufregelung', dict()).get('atsensor', -1) == alter_sensor.get_identifier():
                atsensor = True
            error = u"Die gewünschte Änderung löscht oder verändert Ausgänge. Bitte entfernen Sie zuerst die zugeordneten Ausgänge."
            return render_response(request, "m_install_sc.html",
                                   {"haus": haus, "s": alter_sensor, "etagen": haus.etagen.all(),
                                    "gateways": haus.gateway_related.all(),
                                    'rlsensor': True if not alter_sensor.mainsensor else False, 'atsensor': atsensor,
                                    'serror': error})

        # if alter_sensor and neuer_sensor and alter_sensor.gateway_id != neuer_sensor.gateway_id:
        #     neuer_sensor.gateway_id = None

        try:
            neuer_sensor.save()
        except ValueError as e:
            error += "%s<br/>" % str(e)
        else:

            if alter_sensor and (alter_sensor.name == neuer_sensor.name and alter_sensor.raum == neuer_sensor.raum and
                    alter_sensor.gateway == neuer_sensor.gateway and alter_sensor.haus == neuer_sensor.haus and
                    (alter_sensor.mainsensor is True if request.POST.get('sensortyp') == 'rt' else False or
                        (alter_sensor.raum and alter_sensor.get_identifier() in alter_sensor.raum.get_regelung().get_parameters().get('rlsensorssn', dict()) and
                     request.POST.get('sensortyp') == 'rl') or
                        (alter_sensor.get_identifier() == haus.get_module_parameters().get('ruecklaufregelung', dict()).get('atsensor', -1) and request.POST.get('sensortyp') == 'at'))):
                # es hat sich nur der offset oder die description geaendert
                pass

            else:
                if alter_sensor and alter_sensor.raum_id:  # and alter_sensor.raum_id != neuer_sensor.raum_id:
                    cb2outs = sig_get_outputs_from_output.send(
                        sender='hardware',
                        raum=alter_sensor.raum,
                        regelung=alter_sensor.raum.regelung
                    )
                    outs = [o for out in [_o[1] for _o in cb2outs] for o in out]
                    err_del = sig_create_regelung.send(
                        sender='hardware',
                        regelung=alter_sensor.raum.regelung,
                        operation='deletesensor',
                        sensor=neuer_sensor,
                        sensortyp=request.POST.get('sensortyp'),
                        outs=outs,
                        changed_sensor=alter_sensor
                    )
                    error += "<br/>".join((e[1] for e in err_del if e[1] is not None))

                if neuer_sensor.raum_id or request.POST.get('sensortyp') == 'at':
                    err_ed = sig_create_regelung.send(
                        sender='hardware',
                        regelung=neuer_sensor.raum.regelung if neuer_sensor.raum_id else None,
                        operation='addsensor',
                        sensor=neuer_sensor,
                        sensortyp=request.POST.get('sensortyp'),
                        outs=None,
                        changed_sensor=alter_sensor
                    )
                    error += "<br/>".join((e[1] for e in err_ed if e[1] is not None))

        if len(error):
            return render_response(request, "m_install_sc.html",
                                   {"haus": haus, "s": neuer_sensor, "etagen": haus.etagen.all(),
                                    "gateways": haus.gateway_related.all(), 'serror': error,
                                    'rlsensor': True if request.POST.get('sensortyp') == 'rl' else False,
                                    'atsensor': True if request.POST.get('sensortyp') == 'at' else False})
        else:
            return render_redirect(request, '/m_setup/%s/hardware/s/' % hausid)


@is_admin
def sensoren_editieren(request, hausid):

    # mit unterschiedlich vielen Gateways testen

    haus = Haus.objects.get(pk=long(hausid))
    gateways = haus.gateway_related.all()

    if request.method == 'GET':
        return render_response(request, 'm_install_smult.html', {"haus": haus, "gateways": gateways, "step": 0})

    elif request.method == 'POST':

        step = request.POST.get('step', -1)
        if step == '0':

            err = u''
            ssns = request.POST.get('ssns')
            valid_ssns = []
            for ssn in ssns.split('\n'):
                ssn = ssn.strip()

                if not len(ssn):
                    continue

                s = re.match(r'([0-9A-Fa-f]{2}[_]){7}([0-9A-Fa-f]{2})', ssn)
                if len(ssn) != 23 or not s:
                    err += u'"%s" ist eine ungültige Seriennummer.<br/>' % ssn

                else:
                    valid_ssns.append(ssn)

            if len(err):
                return render_response(request, 'm_install_smult.html', {"haus": haus, "gateways": gateways, "step": 0,
                                                                         "serror": err, "ssns": ssns})

            gateway = request.POST.get('gateway', '0')
            if gateway == '0' or gateway == '-1':
                gw = None
            else:
                try:
                    gw = Gateway.objects.get(pk=long(gateway))
                except Gateway.DoesNotExist:
                    gw = None

            if 'done' in request.POST:

                if len(valid_ssns):
                    for vssn in valid_ssns:
                        if vssn.startswith("26_"):
                            s = Luftfeuchtigkeitssensor(name=vssn, gateway=gw, haus=haus)
                        else:
                            s = Sensor(name=vssn, gateway=gw, haus=haus)
                        try:
                            s.save()
                        except ValueError:
                            pass  # sensor bereits angelegt

                return render_redirect(request, "/m_setup/%s/hardware/s/" % haus.id)

            elif 'cont' in request.POST:
                eundr = [(0, u"Raum auswählen")]
                for etage in haus.etagen.all():
                    for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id).values_list("id", "name"):
                        eundr.append((raum[0], u"%s / %s" % (etage.name, raum[1])))
                return render_response(request, 'm_install_smult.html',
                                       {"haus": haus, "step": 1, "ssns": valid_ssns, "eundr": eundr,
                                        "gateways": gateways if gateway == '0' else None, "gw": gw})

        elif step == '1':

            ssns = request.POST.getlist('ssn')
            error = ''
            for ssn in ssns:
                gateway = request.POST.get("gateway_%s" % ssn, "")
                try:
                    gw = Gateway.objects.get(pk=long(gateway))
                except (Gateway.DoesNotExist, ValueError):
                    gw = None
                raum = request.POST.get("raum_%s" % ssn, "")
                try:
                    r = Raum.objects.get(pk=long(raum))
                except (Raum.DoesNotExist, ValueError):
                    r = None
                offset = request.POST.get("sensor_offset_%s" % ssn, "0.0")
                try:
                    offset = float(offset)
                except ValueError:
                    offset = 0.0

                sensortyp = request.POST.get("sensortyp_%s" % ssn, 0)
                if sensortyp == "lf":
                    s = Luftfeuchtigkeitssensor(name=ssn, gateway=gw, raum=r, haus=haus)
                else:
                    s = Sensor(name=ssn, gateway=gw, raum=r, mainsensor=True if sensortyp == 'rt' else False, haus=haus, offset=offset)

                try:
                    s.save()
                except ValueError:
                    continue

                if s.raum_id or sensortyp == 'at':
                    err_ed = sig_create_regelung.send(
                        sender='hardware',
                        regelung=s.raum.regelung if s.raum_id else None,
                        operation='addsensor',
                        sensor=s,
                        sensortyp=sensortyp,
                        outs=None,
                        changed_sensor=None
                    )
                    error += "<br/>".join((e[1] for e in err_ed if e[1] is not None))

            if len(error):
                return render_response(request, "m_install_smult.html", {"haus": haus, "error": error, "step": 1})

            return render_redirect(request, '/m_setup/%s/hardware/s/' % haus.id)


@is_admin
def ausgaenge_einrichtung(request, hausid):
    haus = Haus.objects.get(pk=long(hausid))
    if request.method == "GET":
        return render_response(request, "m_install_a.html", {"haus": haus})

    if request.method == "POST" and request.user.userprofile.get().role != 'K':
        logging.warning(request.POST)

        if len(request.POST) <= 1:
            logging.warning("nothing to be done, redirecting")
            return render_redirect(request, "/m_setup/%s/hardware/a/" % haus.id)

        all_outputs = sig_get_all_outputs.send(sender='hardware', haus=haus)
        all_possible_outputs = sig_get_possible_outputs_from_regelung.send(sender='hardware', haus=haus)
        apo = {}
        for cbfunc, dev2outs in all_possible_outputs:
            for ctrldev, outs in dev2outs.items():
                apo.setdefault(ctrldev, set())
                for out in outs:
                    apo[ctrldev].add(out[0])
        logging.warning("apo -> %s" % apo)
        z = {}

        for cbfunc, dev2outs in all_outputs:
            for ctrldev, outs in dev2outs.items():
                z.setdefault(ctrldev, dict())
                for out in outs[1].keys():
                    sensorid = request.POST.get('%s_%s_%s' % (ctrldev.id, ctrldev.get_type(), out))
                    if sensorid in apo[ctrldev]:
                        if sensorid.startswith("mischer") or sensorid.startswith("diffreg") or sensorid.startswith("fps") or sensorid.startswith("gwmreg") or sensorid.startswith("nullreg"):
                            z[ctrldev].setdefault(sensorid, list()).append(str(out))
                        else:
                            sensor = AbstractSensor.get_sensor(sensorid)
                            if sensor:
                                z[ctrldev].setdefault(sensor, list()).append(str(out))

                for sensor in ctrldev.get_sensoren():
                    z[ctrldev].setdefault(sensor, list())

        # {
        #   <RFController: 0x12345678>:
        #       {
        #           <RFSensor: 15683 WZ MS>: [],
        #           <RFAktor: 2 WZ WT1 RT>: ['4']
        #       },
        #   <Gateway: test>:
        #       {
        #           <Sensor: rlr rlsensor1>: [],
        #           <Sensor: rlr rlsensor2>: [],
        #           <Sensor: rlr rts>: [],
        #           <RFAktor: 2 WZ WT1 RT>: ['1'],
        #           <Sensor: zpr rts>: ['2']
        #       }
        # }

        existing_outs = sig_get_all_outputs_from_output.send(sender='hardware', haus=haus)
        logging.warning("eo1 -> %s" % existing_outs)
        eo = dict()
        for cbfunc, outs in existing_outs:
            for out in outs:
                eo.setdefault(out[0], list())
                eo[out[0]].append(out)

        logging.warning("eo2: %s" % str(eo))

        for ctrl, outs in eo.items():
            for out in outs:
                if not out[2]._get_reg().managesoutputs():
                    out[3].delete()
                    sig_create_output.send(sender='delete', haus=haus, ctrldevice=ctrl, regelung=out[2], sensor=None, outs=out[4])
                    # if out[2].regelung in {'vorlauftemperaturregelung', 'differenzregelung'}:
                    #     out[2].delete()

        s2a = dict()
        for ctrl, s2o in z.items():
            for sensor, outs in s2o.items():
                try:
                    regelung = sensor.raum.get_regelung()
                except AttributeError:
                    if isinstance(sensor, AbstractSensor):
                        continue
                    elif sensor.startswith("mischer"):
                        reg = Regelung.objects.filter(regelung="vorlauftemperaturregelung", parameter="{'mid': '%s'}" % sensor.split('_')[1])
                        if len(reg) > 1:
                            for r in reg[1:]:
                                r.delete()
                            regelung = reg[0]
                        elif not len(reg):
                            regelung = Regelung(regelung="vorlauftemperaturregelung", parameter="{'mid': '%s'}" % sensor.split('_')[1])
                            regelung.save()
                        else:
                            regelung = reg[0]
                    elif sensor.startswith("diffreg"):
                        reg = Regelung.objects.filter(regelung="differenzregelung", parameter="{'dreg': '%s'}" % sensor.split('_')[1])
                        if len(reg) > 1:
                            for r in reg[1:]:
                                r.delete()
                            regelung = reg[0]
                        elif not len(reg):
                            regelung = Regelung(regelung="differenzregelung", parameter="{'dreg': '%s'}" % sensor.split('_')[1])
                            regelung.save()
                        else:
                            regelung = reg[0]
                    elif sensor.startswith("fps"):
                        regelung = Regelung.objects.get(pk=long(sensor[4:]))
                    elif sensor.startswith("gwmreg"):
                        regelung = Regelung.objects.get(pk=int(sensor.split('_')[1]))
                    elif sensor.startswith("nullreg"):
                        regelung = Regelung.objects.get(pk=int(sensor.split('_')[1]))
                if len(outs):
                    if (isinstance(sensor, AbstractSensor) and sensor.raum in s2a) or (isinstance(sensor, unicode) and sensor in s2a):
                        # UPDATE
                        sig_create_output.send(sender='update', haus=haus, ctrldevice=ctrl, regelung=regelung, sensor=sensor, outs=outs)
                    else:
                        # CREATE
                        sig_create_output.send(sender='create', haus=haus, ctrldevice=ctrl, regelung=regelung, sensor=sensor, outs=outs)
                        if isinstance(sensor, AbstractSensor):
                            s2a[sensor.raum] = True
                        else:
                            s2a[sensor] = True

            ch.set("%s_outs" % ctrl.name, None)
            for i in range(1, 16):
                ch.set("%s_%s" % (ctrl.name, i), None)

        for ctrldev in apo.keys():
            ch.set_gateway_update(ctrldev.name)

        return render_redirect(request, "/m_setup/%s/hardware/a/" % haus.id)


@is_admin
def ausgaenge_anzeigen(request, hausid):
    import json
    from django.core.serializers.json import DjangoJSONEncoder
    haus = Haus.objects.get(pk=long(hausid))
    hparams = haus.get_module_parameters()
    ausgaenge = dict()
    belegte_ausgaenge = dict()

    raumdict = {}
    for etage in haus.etagen.all():
        for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id).values_list("id", 'etage__name', "name"):
            raumdict[raum[0]] = u"Raumregelung %s: %s/%s" % (raum[0], raum[1], raum[2])

    outputs = sig_get_all_outputs_from_output.send('hardware', haus=haus)
    for _, outs in outputs:
        for out in outs:
            regouts = sig_get_outputs_from_regelung.send('hardware', haus=out[0].haus, raum=out[1], regelung=out[2])
            for _, regout in regouts:
                for dev, o in regout.items():
                    if out[0] != dev:
                        continue
                    belegte_ausgaenge.setdefault(out[0], list())
                    belegte_ausgaenge[out[0]] += [_o for _o in o if _o[1]]
                    belegte_ausgaenge[out[0]] = sorted(belegte_ausgaenge[out[0]])

    all_outs = sig_get_all_outputs.send('hardware', haus=haus)
    for cbfunc, dev2outs in all_outs:
        for device, (devstr, outs) in dev2outs.items():
            if device in belegte_ausgaenge:
                for ausgangname, ausgang, sensor, mutable in belegte_ausgaenge[device]:
                    if mutable:
                        if isinstance(sensor, AbstractSensor):
                            outs[ausgangname].append(sensor.get_identifier())
                            outs[ausgangname].append(u"%s %s" % (raumdict[sensor.raum_id], unicode(sensor)))
                        elif isinstance(sensor, tuple) and sensor[0].startswith('diffreg'):
                            outs[ausgangname].append(sensor[0])
                            outs[ausgangname].append(u"Differenzregelung %s: '%s'" % (sensor[0].split('_')[1], hparams.get('differenzregelung', dict()).get(sensor[0].split('_')[1], dict()).get('name', '')))
                        elif isinstance(sensor, tuple) and sensor[0].startswith('fps'):
                            outs[ausgangname].append(sensor[0])
                            outs[ausgangname].append(u"FPS: %s" % sensor[1])
                        elif isinstance(sensor, tuple) and sensor[0].startswith('gwmreg'):
                            outs[ausgangname].append(sensor[0])
                            outs[ausgangname].append(u'%s' % (sensor[1])) #Gateway Monitor
                        elif isinstance(sensor, tuple) and sensor[0].startswith('nullreg'):
                            outs[ausgangname].append(sensor[0])
                            outs[ausgangname].append(u'%s' % (sensor[1])) #Nullregelung
                        elif sensor.startswith('mischer'):
                            outs[ausgangname].append(sensor)
                            outs[ausgangname].append(u'Vorlauftemperaturregelung %s%s' % ((u"%s: '%s'" % (sensor.split('_')[1], hparams.get('vorlauftemperaturregelung', dict()).get(sensor.split('_')[1], dict()).get('name', ""))), {'don': ': auf', 'doff': ': zu', 'ana': ''}[sensor.split('_')[2]]))
                    else:
                        outs[ausgangname].append('inactive')
                        outs[ausgangname].append(u'%s' % unicode(sensor) if sensor else 'Sonderausgang')

            if outs:
                ausgaenge[devstr] = [([k] + v) for k, v in sorted(outs.items())]
    return HttpResponse(json.dumps(ausgaenge, cls=DjangoJSONEncoder), content_type='application/json')


@is_admin
def ausgaenge_anzeigen_sensoren(request, hausid):

    def atoi(text):
        return int(text) if text.isdigit() else text

    def natural_keys(text):
        return [atoi(c) for c in re.split(r'(\d+)', text)]

    import json
    from django.core.serializers.json import DjangoJSONEncoder
    haus = Haus.objects.get(pk=long(hausid))
    ret = dict()

    raumdict = {}
    for etage in haus.etagen.all():
        for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id).values_list("id", "etage__name", "name"):
            raumdict[raum[0]] = u"Raumregelung %s: %s/%s" % (raum[0], raum[1], raum[2])

    possible_outputs = sig_get_possible_outputs_from_regelung.send(sender='hardware', haus=haus, raumdict=raumdict)
    for cbfunc, dev2outs in possible_outputs:
        for device, outs in dev2outs.items():
            idx = u"%s##%s##%s" % (device.id, device, device.get_type())
            ret.setdefault(idx, list())
            ret[idx] += outs
            ret[idx] = sorted(ret[idx], key=lambda s: natural_keys(s[1]))

    return HttpResponse(json.dumps(ret, cls=DjangoJSONEncoder), content_type='application/json')


@is_admin
def edit_etage_move(request, hausid, etagenid, dir):
    etage = Etage.objects.get(pk=long(etagenid))
    etagen = etage.haus.etagen.all()
    last = etage.haus.etagen.order_by('-position')[0]

    ret = move_helper(etage, etagen, last, dir)
    if ret:
        ret.save()
    etage.save()

    return render_redirect(request, "/m_setup/%s/hardware/er/" % hausid)


@is_admin
def edit_raum_move(request, hausid, raumid, dir):
    raum = Raum.objects.get(pk=long(raumid))
    etage = raum.etage
    raeume = Raum.objects.filter(etage_id=etage.id)
    last = raeume.order_by('-position')[0]

    ret = move_helper(raum, raeume, last, dir)
    if ret:
        ret.save()
    raum.save()

    return render_redirect(request, "/m_setup/%s/hardware/er/" % hausid)


def move_helper(changed_entity, all_entities, lastinlist, dir):
    if dir == 'up' and changed_entity.position > 1:
        changed_entity.position -= 1
    elif dir == 'down' and changed_entity.position < lastinlist.position:
        changed_entity.position += 1
    else:
        return

    for e in all_entities:
        if e.position == changed_entity.position:
            if dir == 'up':
                e.position += 1
            if dir == 'down':
                e.position -= 1
            return e
