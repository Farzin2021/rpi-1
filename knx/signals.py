from heizmanager.models import sig_get_outputs_from_output, sig_get_all_outputs_from_output, sig_get_possible_outputs_from_output, sig_get_all_outputs, sig_get_ctrl_devices, sig_create_output, sig_create_output_regelung
import django.dispatch as dispatch
from models import KNXGateway, KNXAktor, KNXAusgang, KNXSensor
from heizmanager.models import Raum
from django.db.models.signals import pre_save
import zmq
import logging
import heizmanager.cache_helper as ch
from heizmanager.getandset import calc_opening
import json


@dispatch.receiver(pre_save, sender=KNXGateway)
def _knx_create_signal(sender, instance, **kwargs):
    if not instance.pk:
        from heizmanager.models import sig_solltemp_change
        sig_solltemp_change.connect(knx_solltemp_change)


@dispatch.receiver(sig_get_outputs_from_output)
def knx_get_outputs_from_output(sender, **kwargs):
    raum = kwargs['raum']
    regelung = kwargs['regelung']
    try:
        ausgang = KNXAusgang.objects.get(regelung_id=regelung.pk)
    except KNXAusgang.DoesNotExist:
        return []

    aktoren = []
    for aktorid in ausgang.aktor:
        try:
            aktoren.append(KNXAktor.objects.get(pk=aktorid).group_address)
        except KNXAktor.DoesNotExist:
            ausgang.aktor.remove(aktorid)
            ausgang.save()
    aktoren = sorted(aktoren)

    return [(ausgang.gateway, raum, regelung, ausgang, aktoren)]


@dispatch.receiver(sig_get_all_outputs_from_output)
def knx_get_all_outputs_from_output(sender, **kwargs):
    ausgaenge = KNXAusgang.objects.all().select_related('gateway', 'regelung')

    haus = kwargs['haus']

    def get_raum(reg_id):
        try:
            return Raum.objects.get_for_user(haus.eigentuemer, regelung_id=reg_id)
        except Raum.DoesNotExist:
            return None

    ret = []
    for ausgang in ausgaenge:
        aktoren = []
        for aktorid in ausgang.aktor:
            try:
                aktoren.append(KNXAktor.objects.get(pk=long(aktorid)).group_address)
            except KNXAktor.DoesNotExist:
                ausgang.aktor.remove(aktorid)
                ausgang.save()
        aktoren = sorted(aktoren)

        ret.append((ausgang.gateway, get_raum(ausgang.regelung_id), ausgang.regelung, ausgang, aktoren))

    return ret


@dispatch.receiver(sig_get_possible_outputs_from_output)
def knx_get_possible_outputs_from_output(sender, **kwargs):

    ret = {}
    gws = KNXGateway.objects.all()
    if not len(gws):
        return {}

    gwdict = {gws[0].id: gws[0]}
    for raum in Raum.objects.select_related('regelung').filter(regelung__regelung=sender):
        ret.setdefault(raum, list())
        for sensor in raum.get_sensoren():
            if isinstance(sensor, KNXSensor):
                ret[raum].append((sensor, gwdict[sensor.gateway_id]))

    return ret


@dispatch.receiver(sig_get_all_outputs)
def knx_get_all_outputs(sender, **kwargs):
    gws = KNXGateway.objects.all()
    if not len(gws):
        return {}

    gw = gws[0]
    aktoren = KNXAktor.objects.filter(gateway=gw)

    return {
        gw: (
            str(gw.id) + '##KNX##KNXGateway',
            dict((aktor.group_address, [aktor.description or aktor.group_address]) for aktor in aktoren)
        )
    }


@dispatch.receiver(sig_get_ctrl_devices)
def knx_get_all_gws(sender, **kwargs):
    haus = kwargs['haus']
    gws = KNXGateway.objects.filter(haus=haus)
    if not len(gws):
        return []
    else:
        return [gws[0]]


@dispatch.receiver(sig_create_output)
def knx_create_output(sender, **kwargs):
    haus = kwargs['haus']
    ctrldev = kwargs['ctrldevice']
    regelung = kwargs['regelung']
    sensor = kwargs['sensor']
    outs = kwargs['outs']
    if type(ctrldev).__name__ == "KNXGateway":
        try:
            ausgang = regelung.knxausgang
        except KNXAusgang.DoesNotExist:
            ausgang = KNXAusgang(regelung=regelung, gateway=ctrldev)
        if sender == 'create':
            ausgang.aktor = []
        for o in outs:
            try:
                aktor = KNXAktor.objects.get(gateway=ctrldev, group_address=o)
                ausgang.aktor += [aktor.id]
            except KNXAktor.DoesNotExist:
                pass

        if not sender == 'delete':
            ausgang.save()

        sig_create_output_regelung.send(sender=sender, ctrldevice=ctrldev, regelung=regelung, sensor=sensor, outs=outs)


def knx_solltemp_change(sender, **kwargs):
    haus = kwargs['haus']
    raum = kwargs['raum']

    gws = KNXGateway.objects.all()
    if len(gws):
        gw = gws[0]
    else:
        from heizmanager.models import sig_solltemp_change
        sig_solltemp_change.disconnect(receiver=knx_solltemp_change)
        return

    context = zmq.Context()
    outsocket = context.socket(zmq.DEALER)
    outsocket.connect("tcp://127.0.0.1:%s" % 5557)

    try:
        do_update = False
        knxaktoren = KNXAktor.objects.all()
        knxausgaenge = KNXAusgang.objects.filter(regelung_id=raum.regelung_id)
        for ausgang in knxausgaenge:
            for aktorid in ausgang.aktor:
                aktor = knxaktoren.get(pk=aktorid)
                if ausgang.gateway_id != aktor.gateway_id or ausgang.regelung_id != raum.regelung_id or aktor.raum != raum:
                    continue

                do_update = True  # handler muss update anfragen, weil raum noch nicht gespeichert

        if do_update:
            outsocket.send_multipart(['serverknx', '', json.dumps({'devices': [], 'do_update': True})])

    except Exception as e:
        logging.exception("exc: %s" % str(e))
    finally:
        outsocket.close()
        context.term()
