# -*- coding: utf-8 -*-

from django.http import HttpResponse
from heizmanager.render import render_response, render_redirect
import json
import heizmanager.network_helper as nh
from heizmanager.models import RPi, Haus, Raum
from heizmanager.models import sig_get_outputs_from_output, sig_new_temp_value, sig_new_output_value, sig_create_regelung
from django.views.decorators.csrf import csrf_exempt
import logging
from fabric.api import local
from models import KNXGateway, KNXAktor, KNXSensor, KNXAusgang
import heizmanager.cache_helper as ch
from datetime import datetime
from heizmanager.getandset import calc_opening
from heizmanager.mobile.m_temp import get_module_offsets


logger = logging.getLogger("logmonitor")


def get_global_settings_link(request, haus):
    hparams = haus.get_module_parameters()
    pv = local("python3 -V", capture=True)
    if pv >= "Python 3.7" and hparams.get('knx_active', False):
        return "<a href='/m_setup/%s/knx/'>KNX</a>" % haus.id
    else:
        return None


def get_global_settings_page(request, haus, action=None, entityid=None):

    def _get_eundr():
        eundr = []
        for etage in haus.etagen.all():
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id).values_list("id", "name"):
                eundr.append((raum[0], u"%s / %s" % (etage.name, raum[1])))
        return eundr

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=int(haus))

    hparams = haus.get_module_parameters()

    if request.method == "GET":
        pv = local("python3 -V", capture=True)

        if pv >= "Python 3.7" and hparams.get('knx_active', False):

            if action == 'add':
                return render_response(request, 'm_install_knxc.html', {'haus': haus, 'eundr': _get_eundr()})

            elif action == 'sedit':
                ga = entityid.replace('_', '/')
                try:
                    sensor = KNXSensor.objects.get(group_address=ga)
                except KNXSensor.objects.MultipleObjectsReturned:
                    sensor = KNXSensor.objects.filter(group_address=ga)[0]
                atsensor = False
                hparams = haus.get_module_parameters()
                if sensor.get_identifier() == hparams.get('ruecklaufregelung', {}).get('atsensor'):
                    atsensor = True
                params = sensor.get_parameters()
                sensor.offset = params.get('offset', 0.0)

                return render_response(request, 'm_install_knxc.html', {'haus': haus, 'eundr': _get_eundr(), 'datapoint': sensor, 'atsensor': atsensor})

            elif action == 'aedit':
                ga = entityid.replace('_', '/')
                try:
                    aktor = KNXAktor.objects.get(group_address=ga)
                except KNXAktor.MultipleObjectsReturned:
                    aktor = KNXAktor.objects.filter(group_address=ga)[0]
                return render_response(request, 'm_install_knxc.html', {'haus': haus, 'eundr': _get_eundr(), 'datapoint': aktor, 'atsensor': False})

            elif action == "sdelete":
                ga = entityid.replace('_', '/')
                try:
                    sensor = KNXSensor.objects.get(group_address=ga)
                except KNXSensor.MultipleObjectsReturned:
                    sensor = KNXSensor.objects.filter(group_address=ga)[0]

                if sensor.raum and sensor.mainsensor:
                    s_has_outs = False
                    cb2outs = sig_get_outputs_from_output.send(
                        sender='hardware', raum=sensor.raum, regelung=sensor.raum.regelung
                    )
                    outs = [o for out in [_o[1] for _o in cb2outs] for o in out]
                    for out in outs:
                        if len(out[4]):
                            s_has_outs = True

                    if s_has_outs:
                        return render_response(request, "m_install_knxc.html", {'haus': haus, 'datapoint': sensor, 'err': u'Vor Raumänderung bitte zuerst zugeordnete Ausgänge entfernen', 'eundr': _get_eundr()})

                    sensor.raum.set_mainsensor(None)

                sensor.delete()
                return render_redirect(request, "/m_setup/%s/knx/" % haus.id)

            elif action == "adelete":
                ga = entityid.replace('_', '/')
                try:
                    aktor = KNXAktor.objects.get(group_address=ga)
                except KNXAktor.MultipleObjectsReturned:
                    aktor = KNXAktor.objects.filter(group_address=ga)[0]

                ausgaenge = KNXAusgang.objects.filter(gateway=aktor.gateway)
                for ausgang in ausgaenge:
                    if aktor.id in ausgang.aktor:
                        return render_response(request, "m_install_knxc.html", {'haus': haus, 'datapoint': aktor, 'err': u'Vor Raumänderung bitte zuerst zugeordnete Ausgänge entfernen', 'eundr': _get_eundr()})
                aktor.delete()
                return render_redirect(request, "/m_setup/%s/knx/" % haus.id)

            elif 'gwaddr' in request.GET:
                gws = KNXGateway.objects.all()
                if len(gws):
                    gw = gws[0]
                else:
                    gw = KNXGateway(haus=haus)

                gaddr = request.GET.get('gwaddr', '')
                ga = gaddr.split('.')
                try:
                    if not (0 <= int(ga[0]) <= 255 and 0 <= int(ga[1]) <= 255 and 0 <= int(ga[2]) <= 255 and (gaddr.count('.') == 2 or (gaddr.count('.') == 3 and 0 <= int(ga[3]) <= 255))):
                        raise Exception
                except Exception:
                    return HttpResponse(u'Unzulässige Gatewayadresse', status=400)

                gw.address = request.GET['gwaddr'].strip()
                gw.save()

                try:
                    local("sudo supervisorctl restart knxhandler")
                except:
                    pass

                return HttpResponse()

            elif 'gwmarker' in request.GET:
                gws = KNXGateway.objects.all()
                if len(gws):
                    gw = gws[0]
                    return HttpResponse(gw.get_marker())
                else:
                    return HttpResponse('red')

            else:
                gws = KNXGateway.objects.all()
                if len(gws):
                    gw = gws[0]
                else:
                    gw = None

                aktoren = sorted(KNXAktor.objects.all(), key=lambda x: x.intga)
                sensoren = sorted(KNXSensor.objects.all(), key=lambda x: x.intga)

                return render_response(request, "m_install_knx.html", {'haus': haus, 'gateway': gw, 'aktoren': aktoren, 'sensoren': sensoren})

        else:
            return render_redirect(request, "/m_setup/%s/hardware/" % haus.id)

    elif request.method == "POST":

        def checkga(ga):
            try:
                ga = ga.strip().split('/')
                if len(ga) != 3 or not (0 <= int(ga[0]) <= 255 and 0 <= int(ga[1]) <= 255 and 0 <= int(ga[2]) <= 255):
                    raise Exception
                return True
            except:
                return False

        if action == "add":

            gws = KNXGateway.objects.all()
            gw = gws[0]

            ga = request.POST.get('group_address')
            description = request.POST.get('description')
            dpt = request.POST.get('dpt')
            raumid = request.POST.get('raum')
            try:
                raum = Raum.objects.get(pk=int(raumid))
            except:
                raum = None
            typ = request.POST.get('typ')
            recga = request.POST.get('recgroupaddr', '')

            if len(recga):
                _recga = []
                for addr in recga.split(','):
                    addr = addr.strip()
                    if checkga(addr):
                        _recga.append(addr)
                recga = _recga
            else:
                recga = []

            if not checkga(ga):
                return render_response(request, "m_install_knxc.html", {'haus': haus, 'datapoint': {'group_address': request.POST.get('group_address'), 'description': description, 'datapoint_type': dpt, 'raum': raum}, 'err': u'Unzulässige Gruppenadresse', 'eundr': _get_eundr()})

            group_address = request.POST.get('group_address').strip()
            if len(KNXSensor.objects.filter(group_address=group_address)) or len(KNXAktor.objects.filter(group_address=group_address)):
                return render_response(request, "m_install_knxc.html", {'haus': haus, 'datapoint': {'group_address': request.POST.get('group_address'), 'description': description, 'datapoint_type': dpt, 'raum': raum}, 'err': u'Gruppenadresse bereits in Verwendung', 'eundr': _get_eundr()})

            if dpt == "9.001":

                if typ == "sensor":
                    # mainsensor = True if request.POST.get('mainsensor') == 'on' and raum is not None else False
                    sensortyp = request.POST.get('sensortyp')
                    sensor = KNXSensor(group_address=request.POST.get('group_address'), datapoint_type=dpt, description=description, mainsensor=sensortyp == "rt", raum=raum, gateway=gw, haus=haus)

                    if sensortyp == "rt" and raum.get_mainsensor():
                        return render_response(request, "m_install_knxc.html", {'haus': haus, 'datapoint': sensor, 'err': u'Raum hat bereits einen Raumtemperatursensor', 'eundr': _get_eundr()})

                    if sensortyp == "rl":
                        if raum and raum.regelung.regelung != 'ruecklaufregelung':
                            from rf.models import RFAusgang
                            if raum.regelung.get_ausgang() or len(RFAusgang.objects.filter(regelung_id=raum.regelung_id)) or len(KNXAusgang.objects.filter(regelung_id=raum.regelung_id)):
                                return render_response(request, "m_install_knxc.html", {'haus': haus, 'datapoint': sensor, 'err': u'Bitte zuerst dem Raum zugeordnete Ausgänge entfernen', 'eundr': _get_eundr()})
                            else:
                                sensor.save()
                                err_ed = sig_create_regelung.send(
                                    sender='knx',
                                    regelung=sensor.raum.regelung,
                                    operation='addsensor',
                                    sensor=sensor,
                                    sensortyp="rl",
                                    outs=None,
                                    changed_sensor=None
                                )
                                error = "<br/>".join((e[1] for e in err_ed if e[1] is not None))
                                if len(error):
                                    return render_response(request, "m_install_knxc.html", {'haus': haus, 'datapoint': sensor, 'err': error, 'eundr': _get_eundr()})
                    
                    if sensortyp == "at":
                        sensor.save()
                        err_ed = sig_create_regelung.send(
                            sender='knx',
                            regelung=sensor.raum.regelung,
                            operation='addsensor',
                            sensor=sensor,
                            sensortyp="at",
                            outs=None,
                            changed_sensor=None
                        )
                        error = "<br/>".join((e[1] for e in err_ed if e[1] is not None))
                        if len(error):
                            return render_response(request, "m_install_knxc.html", {'haus': haus, 'datapoint': sensor, 'err': error, 'eundr': _get_eundr()})
                    
                    sensor.save()  # kann redundant sein, ist aber wurscht
                    
                    params = sensor.get_parameters()
                    params['sensortyp'] = sensortyp
                    try:
                        offset = float(request.POST.get('offset', 0.0).replace(',', '.'))
                        params['offset'] = offset
                    except:
                        pass
                    sensor.set_parameters(params)

                    if sensortyp == "rt" and raum:
                        raum.set_mainsensor(sensor)

                elif typ == "aktor":
                    aktor = KNXAktor(group_address=request.POST.get('group_address'), datapoint_type=dpt, description=description, raum=raum, gateway=gw, rec_group_addresses=recga)
                    aktor.save()

            elif dpt in {'1.001', '5.001'}:

                if typ == "aktor":
                    aktor = KNXAktor(group_address=request.POST.get('group_address'), datapoint_type=dpt, description=description, raum=raum, gateway=gw, rec_group_addresses=recga)
                    aktor.save()

                elif typ == "sensor":
                    sensor = KNXSensor(group_address=request.POST.get('group_address'), datapoint_type=dpt, description=description, mainsensor=False, raum=raum, gateway=gw)
                    sensor.save()

            try:
                local("sudo supervisorctl restart knxhandler")
            except:
                pass
            
            return render_redirect(request, "/m_setup/%s/knx/" % haus.id)

        elif action == "sedit":

            ga = entityid.replace("_", "/")
            sensor = KNXSensor.objects.get(group_address=ga)

            description = request.POST.get('description')
            raumid = request.POST.get('raum')
            try:
                raum = Raum.objects.get(pk=int(raumid))
            except:
                raum = None
            sensortyp = request.POST.get('sensortyp')

            sensor.description = description

            if (raum != sensor.raum and sensor.raum is not None and sensor.mainsensor) or \
                    (raum == sensor.raum and raum is not None and sensor.mainsensor and sensortyp != "rt") or \
                    (raum != sensor.raum and sensor.raum and sensor.get_typ() == "rl"):
                s_has_outs = False
                cb2outs = sig_get_outputs_from_output.send(
                    sender='hardware', raum=sensor.raum, regelung=sensor.raum.regelung
                )
                outs = [o for out in [_o[1] for _o in cb2outs] for o in out]
                for out in outs:
                    if len(out[4]):
                        s_has_outs = True

                if s_has_outs:
                    return render_response(request, "m_install_knxc.html", {'haus': haus, 'datapoint': sensor, 'err': u'Vor Raumänderung bitte zuerst zugeordnete Ausgänge entfernen', 'eundr': _get_eundr()})

                sensor.raum.set_mainsensor(None)

            sensor.raum = raum
            sensor.mainsensor = sensortyp == "rt"
            sensor.save()
            if sensor.mainsensor and sensor.raum:
                sensor.raum.set_mainsensor(sensor)

            params = sensor.get_parameters()
            params['sensortyp'] = sensortyp
            try:
                offset = float(request.POST.get('offset', 0.0).replace(',', '.'))
                params['offset'] = offset
            except:
                pass
            sensor.set_parameters(params)

            if sensortyp == "at":
                hparams = haus.get_module_parameters()
                hparams.setdefault('ruecklaufregelung', {})
                hparams['ruecklaufregelung']['atsensor'] = sensor.get_identifier()
                haus.set_module_parameters(hparams)

            try:
                local("sudo supervisorctl restart knxhandler")
            except:
                pass
            
            return render_redirect(request, "/m_setup/%s/knx/" % haus.id)

        elif action == 'aedit':

            ga = entityid.replace("_", "/")
            aktor = KNXAktor.objects.get(group_address=ga)

            description = request.POST.get('description')
            raumid = request.POST.get('raum')
            try:
                raum = Raum.objects.get(pk=int(raumid))
            except:
                raum = None
            recga = request.POST.get('recgroupaddr', '')

            if len(recga):
                _recga = []
                for addr in recga.split(','):
                    addr = addr.strip()
                    if checkga(addr):
                        _recga.append(addr)
                recga = _recga
            else:
                recga = []

            aktor.description = description
            aktor.rec_group_addresses = recga

            if raum != aktor.raum:
                ausgaenge = KNXAusgang.objects.filter(gateway=aktor.gateway)
                for ausgang in ausgaenge:
                    if aktor.id in ausgang.aktor:
                        return render_response(request, "m_install_knxc.html", {'haus': haus, 'datapoint': aktor, 'err': u'Vor Raumänderung bitte zuerst zugeordnete Ausgänge entfernen', 'eundr': _get_eundr()})

            aktor.raum = raum
            aktor.save()

            try:
                local("sudo supervisorctl restart knxhandler")
            except:
                pass

            return render_redirect(request, "/m_setup/%s/knx/" % haus.id)


@csrf_exempt
def set_knx(request, rpimac, controllerid):
    
    if request.method == "POST":

        rpi = RPi.objects.get(name=rpimac)
        data = request.POST.get("data", "").replace(' ', '+') # todo ab hier alles in die knx.models analog zu rf.models
        try:
            data = json.loads(
                rpi.crypt_keys.decrypt(data).rsplit('}', 1)[0] + '}'
            )
        except (ValueError, TypeError, AttributeError):
            try:
                data = json.loads(request.POST['data'])
            except Exception as e:
                logging.exception("exception in set_knx: %s" % str(e))
                return HttpResponse()
        except RPi.DoesNotExist:
            return HttpResponse()

        ch.set_gw_ping(controllerid)
        logging.warning("set_knx received %s" % data)
        if not len(data):  # update
            return HttpResponse()

        try:
            gw = KNXGateway.objects.get(address=controllerid)
            group_address = data['group_address']
            sensoren = KNXSensor.objects.filter(gateway=gw, group_address=group_address)
            if len(sensoren): # todo und alles was hier folgt sollte in die knx.models
                sensor = sensoren[0]
                value = float(data['value'])
                ch.set_new_vals("%s_%s_val" % (gw.address.replace('.', '_'), sensor.get_url_ga()), value, gw.haus_id)
                sig_new_temp_value.send(sender="tset", name=sensor.get_identifier(), value=value, modules=gw.haus.get_modules(), timestamp=datetime.now())
                logger.warning("0|%s|%s,%s" % (controllerid.lower(), sensor.group_address, value))
            else:
                aktor = KNXAktor.objects.filter(gateway=gw, rec_group_addresses__contains=group_address)
                if len(aktor):
                    aktor = aktor[0]
                    value = int(data['value'])
                    ch.set_new_vals("%s_%s_val" % (gw.address.replace('.', '_'), aktor.get_url_ga()), value, gw.haus_id)
                    logger.warning("0|%s|%s,%s" % (controllerid.lower(), aktor.group_address, value))
        except Exception:
            logging.exception("exc")
    
    return HttpResponse()


def get_knx(request, controllerid):

    if request.method == "GET":
         
        mac = nh.get_mac()
        rpi = RPi.objects.get(name=mac)
        gws = KNXGateway.objects.filter(haus=rpi.haus)
        ret = {'devices': []}
        for gw in gws[:1]:
            ret['config'] = {'controller_address': gw.address}
            aktoren = KNXAktor.objects.filter(gateway=gw)
            ausgaenge = KNXAusgang.objects.filter(gateway=gw)
            for ausgang in ausgaenge:
                reg = ausgang.get_regelung()

                for aktorid in ausgang.aktor:
                    aktor = aktoren.get(pk=aktorid)

                    if aktor.datapoint_type == "1.001":
                        try:
                            val = reg.do_ausgang(aktor.group_address).content
                        except Exception:
                            logging.exception("exc")
                            val = "<0>"
                        
                        if aktor.raum and aktor.raum.regelung == reg:
                            mode = aktor.raum.get_betriebsart()
                            val = calc_opening(val, mode, "4.23", 1, aktor.raum)

                    elif aktor.datapoint_type == "5.001":
                        if aktor.raum and aktor.raum.regelung == reg:  # aktor kann ja auch in einem raum sein, aber zb von einer fps gesteuert werden
                            val = ch.get("%s_%s" % (gw.address.replace('.', '_'), aktor.get_url_ga()))
                            if val is None:
                                val = "<>"
                            mode = aktor.raum.get_betriebsart()
                            val = calc_opening(val, mode, "5.03", 1, aktor.raum)
                        else:
                            val = reg.do_ausgang(aktor.group_address).content

                    else:
                        continue                        

                    ziel = aktor.raum.solltemp + get_module_offsets(aktor.raum, only_clear_offsets=True)
                    sig_new_output_value.send(sender='get_knx', name='%s_%s' % (gw.address, aktor.group_address), value=val, ziel=ziel, parameters={'empty': True}, timestamp=datetime.now(), modules=gw.haus.get_modules())

                    ret['devices'].append({'group_address': aktor.group_address, 'dpt': aktor.datapoint_type, 'value': val})
            
            sensoren = KNXSensor.objects.filter(gateway=gw)
            for sensor in sensoren:
                ret['devices'].append({'group_address': sensor.group_address, 'dpt': sensor.datapoint_type})

            for aktor in aktoren:
                for recga in aktor.rec_group_addresses:
                    ret['devices'].append({'group_address': recga, 'dpt': aktor.datapoint_type})
                
                if aktor.datapoint_type == "9.001" and aktor.raum_id:
                    ret['devices'].append({'group_address': aktor.group_address, 'dpt': aktor.datapoint_type, 'value': aktor.raum.solltemp})
       
        logging.warning("get_knx returns %s" % ret)
        
        if controllerid == 'config':
            return HttpResponse(json.dumps(ret))
        else:
            return HttpResponse(rpi.crypt_keys.encrypt(json.dumps(ret)))

