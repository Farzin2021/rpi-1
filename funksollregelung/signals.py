from heizmanager.models import sig_get_outputs_from_regelung, sig_get_possible_outputs_from_regelung, sig_create_regelung
from django.dispatch import receiver
import funksollregelung.views as fsr


@receiver(sig_get_outputs_from_regelung)
def fsr_get_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raum = kwargs['raum']
    regelung = kwargs['regelung']
    if regelung.regelung == 'funksollregelung':
        return fsr.get_outputs(haus, raum, regelung)
    else:
        return {}


@receiver(sig_get_possible_outputs_from_regelung)
def fsr_get_possible_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raumdict = kwargs.get('raumdict', {})
    return fsr.get_possible_outputs(haus, raumdict)


@receiver(sig_create_regelung)
def rlr_create_regelung(sender, **kwargs):
    regelung = kwargs['regelung']
    if regelung is None or regelung.regelung != 'funksollregelung':
        return

    operation = kwargs['operation']

    if operation == 'deletesensor':
        sensor = kwargs['sensor']
        outs = kwargs['outs']
        if sensor.mainsensor:
            for out in outs:
                out[3].delete()