from Crypto.Cipher import AES
from Crypto import Random
from aeskey import key


def aes_encrypt(data):
    import base64

    Random.atfork()
    iv = Random.new().read(AES.block_size)
    aes = AES.new(base64.b64decode(key), AES.MODE_CBC, iv)
    out = iv + aes.encrypt(data + Random.new().read(-len(data) % 16).encode('hex')[::2])

    return base64.b64encode(out)


def aes_decrypt(data):
    import base64
    data = base64.b64decode(data)
    iv = data[:16]

    aes = AES.new(base64.b64decode(key), AES.MODE_CBC, iv)
    data = aes.decrypt(data[16:])

    return data