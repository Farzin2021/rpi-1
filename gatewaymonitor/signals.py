from heizmanager.models import sig_get_outputs_from_regelung, sig_get_possible_outputs_from_regelung, sig_create_regelung, sig_create_output_regelung, Raum
from django.dispatch import receiver
import gatewaymonitor.views as gwm
import heizmanager.cache_helper as ch
import logging


@receiver(sig_get_outputs_from_regelung)
def gwm_get_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raum = kwargs['raum']
    regelung = kwargs['regelung']
    return gwm.get_outputs(haus, raum, regelung)


@receiver(sig_get_possible_outputs_from_regelung)
def gwm_get_possible_outputs(sender, **kwargs):
    haus = kwargs['haus']
    return gwm.get_possible_outputs(haus)


@receiver(sig_create_regelung)
def gwm_create_regelung(sender, **kwargs):
    # called when a sensor is added or deleted and requires changes to a regelung
    # currently not necessary here
    pass


@receiver(sig_create_output_regelung)
def gwm_create_output(sender, **kwargs):
    logging.warning("gwm mr_create_output: %s %s" % (sender, str(kwargs)))
    regelung = kwargs['regelung']
    if regelung.regelung == 'gatewaymonitor':
        ctrldevice = kwargs['ctrldevice']
        #if sender == 'delete':
        #ch.delete("gwm_%s" % ctrldevice.name)

