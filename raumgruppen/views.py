# -*- coding: utf-8 -*-
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Raum, GatewayAusgang, Sensor
from django.http import HttpResponse
import zeitschalter.views as zeitschalter
import heizmanager.cache_helper as ch
from models import Raumgruppe
from wetter import views as wetter
import logging


logger = logging.getLogger("logmonitor")


def get_name():
    return u'Raumgruppen'


def get_cache_ttl():
    return 86400


def is_togglable():
    return True


def get_offset():
    return 0.0


def is_hidden_offset():
    return True


def get_global_settings_link(request, haus):
    return '<a href="/m_setup/%s/raumgruppen/">Raumgruppen</a>' % haus.id


def get_global_description_link():
    desc = u"Beliebige Gruppierung von Räumen."
    desc_link = "http://support.controme.com/auswahl-und-aktivierung-von-modulen/modul-raumgruppen/"
    return desc, desc_link


def get_global_settings_page(request, haus):
    if request.method == "GET":

        if 'rgdel' in request.GET and request.user.userprofile.get().role != 'K':
            if request.user.userprofile.get().role != 'K':
                try:
                    rg = Raumgruppe.objects.get(pk=long(request.GET['rgdel']))
                    rg.delete()
                except Raumgruppe.DoesNotExist:  # alter wert in request.GET
                    pass
            return render_redirect(request, "/m_setup/%s/raumgruppen/" % haus.id)

        elif 'rgid' in request.GET and 'rid' in request.GET:
            if request.user.userprofile.get().role != 'K':
                rg = Raumgruppe.objects.get(pk=long(request.GET['rgid']))
                try:
                    rg.raeume.remove(request.GET['rid'])
                except ValueError:
                    pass
                rg.save()
            return render_redirect(request, "/m_setup/%s/raumgruppen/?rgid=%s" % (haus.id, request.GET['rgid']))

        elif 'rgid' in request.GET:  # detailseite fuer RG mit ID=rgid
            raumgruppe = Raumgruppe.objects.get(pk=long(request.GET['rgid']))
            rgraeume = [Raum.objects.get_for_user(request.user, pk=id) for id in raumgruppe.raeume]
            raeume = raumgruppe.get_raeume_sorted()
            eundr = _get_eundr(request, haus)
            return render_response(request, "m_settings_raumgruppe_edit.html",
                                   {"haus": haus, "rg": raumgruppe, "rgraeume": rgraeume, "sraeume": raeume,
                                    "eundr": eundr})

        elif 'getrg' in request.GET:
            import json
            from django.core.serializers.json import DjangoJSONEncoder
            raumgruppen = _get_raumgruppen(request.user, haus)
            ret = {}
            for raumgruppe in raumgruppen:
                ret[str(raumgruppe[0])+'##'+raumgruppe[1]] = raumgruppe[2]
            if not len(ret):
                ret = None
            return HttpResponse(json.dumps(ret, cls=DjangoJSONEncoder), content_type='application/json')

        else:  # hauptseite
            raumgruppen = _get_raumgruppen(request.user, haus)
            eundr = _get_eundr(request, haus)
            return render_response(request, "m_settings_raumgruppen.html",
                                   {"haus": haus, "raumgruppen": raumgruppen, "eundr": eundr})

    if request.method == "POST" and request.user.userprofile.get().role != 'K':
        if 'rgcreate' in request.POST:
            if 'name' not in request.POST or not len(request.POST['name']):
                error = 'Bitte Namen angeben.'
                raumgruppen = _get_raumgruppen(request.user, haus)
                eundr = _get_eundr(request, haus)
                return render_response(request, "m_settings_raumgruppen.html",
                                       {"haus": haus, "raumgruppen": raumgruppen, "error": error, "eundr": eundr})

            name = request.POST['name']
            raeume = list()
            for key in request.POST.items():
                if key[0].startswith('rsel'):
                    raumid = key[0].split('rsel',1)[1]
                    raeume.append(raumid)

            if not len(raeume):
                error = 'Bitte R&auml;eme ausw&auml;hlen.'
                raumgruppen = _get_raumgruppen(request.user, haus)
                eundr = _get_eundr(request, haus)
                return render_response(request, "m_settings_raumgruppen.html",
                                       {"haus": haus, "raumgruppen": raumgruppen, "error": error, "eundr": eundr})

            try:
                raumgruppe = Raumgruppe.objects.get(name=name, haus=haus)
                raumgruppe.raeume = raeume
            except (Raumgruppe.DoesNotExist, AttributeError):
                raumgruppe = Raumgruppe(name=name, raeume=raeume, haus=haus)
            raumgruppe.save()

            return render_redirect(request, '/m_setup/%s/raumgruppen/' % haus.id)

        elif 'rgedit' in request.POST:
            if request.user.userprofile.get().role != 'K':
                raumgruppe = Raumgruppe.objects.get(pk=long(request.POST['rgid']))
                if len(request.POST['rgname']):
                    raumgruppe.name = request.POST['rgname']
                raeume = list()
                for key in request.POST.items():
                    if key[0].startswith('rsel'):
                        raumid = key[0].split('rsel',1)[1]
                        raeume.append(raumid)
                for r in raeume:
                    raumgruppe.raeume.append(r)
                raumgruppe.save()
                # rgraeume = [Raum.objects.get_for_user(request.user, pk=id) for id in raumgruppe.raeume]
                return render_redirect(request, '/m_setup/%s/raumgruppen/' % haus.id)


def _get_eundr(request, haus):
    eundr = []
    for etage in haus.etagen.all():
        e = {etage: []}
        for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
            e[etage].append(raum)
        eundr.append(e)
    return eundr


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_raumgruppen.html", {'haus': haus})


def get_local_settings_link_haus(request, haus):
    if 'raumgruppen' not in haus.get_modules():
        return []
    links = ["<li data-role='list-divider' data-theme='a'>Raumgruppen</li>"]
    for raumgruppe in Raumgruppe.objects.filter(haus=haus):
        links.append("<li><a href='/m_raumgruppen/%s/'>%s</a></li>" % (str(raumgruppe.id), raumgruppe.name))
    if len(links) == 1: links = []
    return links
    

def get_local_settings_page_haus(request, haus, action, raumgruppeid):
    '''
    das sind die Raumgruppenseiten in der Etagen/Raeume Liste
    '''
    if request.method == "GET":
        modules = haus.get_modules()
        raumgruppe = Raumgruppe.objects.get(pk=long(raumgruppeid))
        zeitd, zeith = zeitschalter._get_zeit_select()
        waktiv = False
        if 'wetter' in modules:
            waktiv = True
        zsaktiv = False
        if 'zeitschalter' in modules:
            zsaktiv = True
        raeume = []
        for rid in raumgruppe.raeume:
            r = Raum.objects.get_for_user(request.user, pk=long(rid))
            if r:
                raeume.append(r.name)
        return render_response(request, "m_raumgruppe.html",
                               {'rg': raumgruppe, 'raeume': ', '.join(raeume), "zeitd": zeitd, "zeith": zeith,
                                'waktiv': waktiv, 'zsaktiv': zsaktiv, 'haus': raumgruppe.haus})

    if request.method == "POST":
        soll = warm = kalt = solloffset = zsoffset = None
        if 'temp' in request.POST and request.POST['temp'] == '1':
            soll = float(request.POST['soll'].replace(',','.'))
        
        if 'solloffset' in request.POST and request.POST['solloffset'] == '1':
            if len(request.POST['soffsetval']):
                solloffset = float(request.POST['soffsetval'].replace(',','.'))

        activateall = False
        if 'wetter' in request.POST and request.POST['wetter'] == '1':
            warm = 0
            if len(request.POST['warm']): warm = int(request.POST['warm'], 10)
            kalt = 0
            if len(request.POST['kalt']): kalt = int(request.POST['kalt'], 10)
            if 'activateall' in request.POST and request.POST['activateall'] == 'on':
                activateall = True
        
        wetter_off = False
        if 'wetteroff' in request.POST and request.POST['wetteroff'] == '1':
            wetter_off = True
        
        if 'zs' in request.POST and request.POST['zs'] == '1':
            d = int(request.POST['zeitd'])
            h = float(request.POST['zeith'])
            if (d != 0 or h != 0.0) and len(request.POST['zsoffset']):
                zsoffset = float(request.POST['zsoffset'].replace(',','.'))
            else:
                zsoffset = None
        
        zs_off = False
        if 'zsoff' in request.POST and request.POST['zsoff'] == '1':
            zs_off = True
                
        for raumid in raumgruppe.raeume:
            raum = Raum.objects.get_for_user(request.user, pk=raumid)
            if not raum:
                continue
            _set_raum_soll(raum, soll, warm, kalt, solloffset)
            if activateall:
                wetter.activate(raum)
            if wetter_off:
                wetter.deactivate(raum)
            if zs_off:
                zeitschalter._remove_zeitschalter(raum)
                # genau zeit kennen wir nicht, also 5min
                ch.set_module_offset_raum('zeitschalter', raum.etage.haus.id, raum.id, 0.0, ttl=300)
                ch.delete("%s_roffsets_dict" % raum.id)
            if zsoffset:
                zeitschalter._set_zeitschalter(raum, zsoffset, d, h)
                # genaue zeit kennen wir nicht, also 5min
                ch.set_module_offset_raum('zeitschalter', raum.etage.haus.id, raum.id, zsoffset, ttl=300)
                ch.delete("%s_roffsets_dict" % raum.id)

            # irgendwas geaendert, get cache loeschen
            if soll or warm or kalt or solloffset or zsoffset or zs_off or wetter_off:
                try:
                    from heizmanager import getandset
                    getandset._invalidate_get_cache(raum.get_regelung().get_ausgang().gateway.name,
                                                    raum.get_regelung().get_ausgang().ausgang)
                    request.path = '/get/%s/%s/' % (raum.get_regelung().get_ausgang().gateway.name,
                                                    raum.get_regelung().get_ausgang().ausgang.split(',')[0])
                    request.META['HTTP_REFERER'] = '/m_raum/'  # wegen get/get_all Sperre
                    response = getandset.get(request,
                                             raum.get_regelung().get_ausgang().gateway.name,
                                             raum.get_regelung().get_ausgang().ausgang.split(',')[0])\
                        .content.replace('!', '')
                except (GatewayAusgang.DoesNotExist, AttributeError, TypeError):
                    pass

        return render_redirect(request, "/")


def _get_raumgruppen(user, haus):
    raumgruppen = Raumgruppe.objects.filter(haus=haus)
    if len(raumgruppen):
        rg = []
        for raumgruppe in raumgruppen:
            raeume = []
            for raumid in raumgruppe.raeume:
                try:
                    raum = Raum.objects.get_for_user(user, pk=raumid)
                    raeume.append(raum.etage.name + '/' + raum.name)
                except Raum.DoesNotExist:
                    raumgruppe.raeume.remove(raumid)
                    raumgruppe.save()
            rg.append((raumgruppe.id, raumgruppe.name, sorted(raeume)))
        raumgruppen = sorted(rg)
    return raumgruppen


def _set_raum_soll(raum, soll=None, warm=None, kalt=None, solloffset=None):
    if soll:
        if raum.get_regelung().regelung == "ruecklaufregelung":
            ms = raum.get_mainsensor()
            params = raum.get_regelung().get_parameters()
            if not ms or (ms and params.get('ignorems', False)):
                params['offset'] = soll
                raum.get_regelung().set_parameters(params)
            else:
                raum.solltemp = soll
                logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur via Raumgruppe auf %.2f gesetzt" % raum.solltemp))
        else:
            raum.solltemp = soll
            logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur via Raumgruppe auf %.2f gesetzt" % raum.solltemp))
    if warm is not None and kalt is not None:
        import ast
        params = ast.literal_eval(raum.module_parameters)
        params['wetter']['wetter_params'] = str(warm)+','+str(kalt)
        raum.module_parameters = str(params)
    if solloffset:
        if raum.get_regelung().regelung == "ruecklaufregelung":
            ms = raum.get_mainsensor()
            params = raum.get_regelung().get_parameters()
            if not ms or (ms and params.get('ignorems', False)):
                params['offset'] += solloffset
                raum.get_regelung().set_parameters(params)
            else:
                raum.solltemp += solloffset
        else:
            raum.solltemp += solloffset
            logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur via Raumgruppe auf %.2f gesetzt" % raum.solltemp))

    try:
        ausgang = raum.regelung.get_ausgang()
        if not ausgang is None:
            ch.set_gateway_update(ausgang.gateway.name)
    except Sensor.DoesNotExist:
        logging.warning('temperatur.soll: sensor.doesnotexist')
    except GatewayAusgang.DoesNotExist:
        logging.warning('temperatur.soll: gatewayausgang.doesnotexist')
    finally:
        if not raum.isttemp and soll:  # wenn beim ersten Mal setzen 'ist == None'
            raum.isttemp = soll
        raum.save()
        return
