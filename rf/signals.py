import zmq
import django.dispatch as dispatch
from models import RFController, RFAktor, RFAusgang
from django.db.models.signals import pre_save
import json
import logging
from heizmanager.mobile.m_temp import get_module_offsets
from heizmanager.network_helper import get_mac

@dispatch.receiver(pre_save, sender=RFController)
def _rfc_create_signal(sender, instance, **kwargs):
    if not instance.pk:
        from heizmanager.models import sig_solltemp_change
        sig_solltemp_change.connect(rf_solltemp_change)


def rf_solltemp_change(sender, **kwargs):
    haus = kwargs['haus']
    raum = kwargs['raum']

    rfc = RFController.objects.filter(haus=haus)
    if not len(rfc):
        from heizmanager.models import sig_solltemp_change
        sig_solltemp_change.disconnect(receiver=rf_solltemp_change)
        return

    context = zmq.Context()
    outsocket = context.socket(zmq.DEALER)
    outsocket.connect("tcp://127.0.0.1:%s" % 5557)

    try:
        rfaktoren = RFAktor.objects.filter(type='wt', protocol='zwave', raum_id=raum.id)
        for rfa in rfaktoren:
            outsocket.send_multipart(['serverzwave', '', json.dumps({rfa.name: {0x43: ("Heating 1", round(raum.solltemp))}})])

        rfaktoren = RFAktor.objects.filter(type__startswith="hkt", protocol="btle", raum_id=raum.id)
        offset = get_module_offsets(raum, only_clear_offsets=False)
        solltemp = raum.solltemp  # die eingangene aenderung kann durch den raum.regelung.get_params() aufruf weiter unten offenbar im cache ueberschrieben werden
        ziel = solltemp + offset
        zieloffset = 0.0
        ms = raum.get_mainsensor()
        if ms:
            last = ms.get_wert()
            if last:
                regparams = raum.regelung.get_parameters()
                rsfaktor = regparams.get('rsfaktor', 0)
                try:
                    if ziel < last[0]-0.1:
                        zieloffset = round((ziel-last[0])*rsfaktor, 1)  # zo negativ
                    elif ziel > last[0]+0.1:
                        zieloffset = round((ziel-last[0])*rsfaktor, 1)  # zo positiv
                    else:
                        zieloffset = 0.0
                except TypeError:
                    zieloffset = 0.0
            else:
                pass

        offset = (-offset)+(-zieloffset)
        mac = get_mac()
        for rfa in rfaktoren:
            if rfa.controller.name != mac:
                continue
            outsocket.send_multipart(['serverbtle', '', json.dumps({"data": {rfa.name: {'soll': solltemp, 'offset': offset}}})])

        rfaktoren = RFAktor.objects.filter(type__in=["wp", "relais"], protocol="enocean")
        rfausgaenge = RFAusgang.objects.filter(regelung_id=raum.regelung_id)
        for rfa in rfaktoren:
            for ausgang in rfausgaenge:
                if ausgang.controller != rfa.controller:
                    continue
       
                if rfa.id in ausgang.aktor and ausgang.regelung_id == raum.regelung_id:
                    try:
                        val = int(ausgang.regelung.do_ausgang(0).content.translate(None, "<>!"))
                    except Exception as e:
                        logging.exception("error converting out for reg.id==%s" % ausgang.regelung_id)
                        val = 0
                    if rfa.type == 'relais':
                        outsocket.send_multipart(['serverenocean', '', json.dumps({"data": {rfa.name: {'val': val, 'rorg': 0xd2, 'rorg_func': 0x01, 'rorg_type': 0x01}}})])
                        val = min(val, 1)
                        params = rfa.get_parameters()
                        if params.get('last_val') != val and val is not None:
                            params['last_val'] = val
                            params['last_ack'] = False
                            params['last_ack_val'] = None
                            rfa.set_parameters(params)

                    elif rfa.type == 'wp':
                        outsocket.send_multipart(['serverenocean', '', json.dumps({"data": {rfa.name: {'val': val, 'rorg': 0xd2, 'rorg_func': 0x01, 'rorg_type': 0x0a}}})])

        rfaktoren = RFAktor.objects.filter(type="hkta52001", protocol="enocean", raum_id=raum.id)
        for rfa in rfaktoren:
            ms = raum.get_mainsensor()
            msval = 0x00
            if ms:
                msval = ms.get_wert()
                if msval:
                    msval = msval[0]

                msval = ms.get_wert()
                marker = ms.get_marker()
                if msval and marker == "green":
                    msval = msval[0]
                    import heizmanager.cache_helper as ch
                    last_vals = ch.get_last_vals("%s_%s_val" % (rfa.controller.lower(), rfa.name))
                    if msval - ziel > 2 and isinstance(last_vals, tuple) and int(last_vals[0].get('CV', 0)) > 0:
                        ziel = -99
                else:
                    msval = 0x00  # wegen der green bedingung
            outsocket.send_multipart(['serverenocean', '', json.dumps({"data": {rfa.name: {'SP': solltemp + get_module_offsets(raum), 'TMP': msval, 'rorg': 0xa5, 'rorg_func': 0x20, 'rorg_type': 0x01}}})])

    except Exception as e:
        logging.exception("exc: %s" % str(e))
    finally:
        outsocket.close()
        context.term()
