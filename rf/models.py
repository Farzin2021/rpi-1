# -*- coding: utf-8 -*-

from django.db import models
from heizmanager.models import AbstractDevice, AbstractSensor, AbstractOutput, RPi, Raum, Sensor
import ast
from heizmanager import cache_helper as ch
from heizmanager import network_helper as nh
from heizmanager import pytz
from heizmanager.fields import ListField
from django.db.models.signals import post_init, post_delete, post_save
import django.dispatch as dispatch
from heizmanager.models import sig_get_outputs_from_output, sig_get_all_outputs, sig_get_possible_outputs_from_output, sig_get_ctrl_devices, sig_get_all_outputs_from_output, sig_create_output, sig_create_output_regelung, sig_new_output_value, sig_new_temp_value
import logging
from itertools import chain
from caching.base import CachingManager
import traceback
import requests
from rpi.aeshelper import aes_encrypt
import json
import math
from datetime import datetime, timedelta
from quickui.views import QuickuiMode
from heizmanager.abstracts.base_mode import BaseMode
import copy
from heizmanager.mobile.m_temp import get_module_offsets
from fabric.api import local


logger = logging.getLogger("logmonitor")


class RFDevice(AbstractDevice):
    PROTOCOL_CHOICES = (
        ('zwave', 'ZWave'),
        ('btle', 'Bluetooth'),
        ('enocean', 'Enocean'),
    )
    TYPE_CHOICES = (  # hersteller, klasse, typ, version?
        ('hkt', u'Heizkörper Thermostat Comet'),
        ('hktControme', u'Heizkörper Thermostat Controme'),
        ('hktGenius', u'Heizkörper Thermostat Genius'),
        ('hkteTRV', u'Heizkörperthermostat eTRV'),
        ('ctrl', 'Controller'),
        ('undef', 'undefined'),
        ('sensor', 'Sensor'),
        ('multisensor', 'Mehrfachsensor'),
        ('relais', 'Relais'),
        ('wp', 'Funksteckdose'),
        ('repeater', 'Range Extender'),
        ('wt', 'Wandthermostat'),
        ('cntct', 'Kontakt'),
        ('sensora50401', 'Temperatur-/Luftfeuchtesensor (A5-04-01)'),
        ('sensora50205', 'Temperatursensor (A5-02-05)'),
        ('sensora50213', u'Außentemperatursensor (A5-02-13)'),
        ('wpd2010b', 'Funksteckdose (D2-01-0B)'),
        ('hkta52001', u'Heizkörperthermostat (A5-20-01)'),
        ('pira50703', 'Bewegungsmelder (A5-07-03)'),
    )

    protocol = models.CharField(max_length=16, choices=PROTOCOL_CHOICES)
    type = models.CharField(max_length=16, choices=TYPE_CHOICES)
    parameters = models.TextField()
    hidden = models.BooleanField(default=False)
    
    objects = CachingManager()

    @staticmethod
    def get_command_class(typestring):
        t2cc = {
            "hkt": 0x43,  # CC_THERMOSTAT_SETPOINT
            "sml": 0x31   # CC_SENSOR_MULTILEVEL
        }
        if typestring in t2cc:
            return t2cc[typestring]
        else:
            return 0x00  # CC NOP

    @staticmethod
    def get_type_from_generic_type(typestring):
        gt2t = {
            0x08: "Thermostate",
            0x20: "Sensoren"
        }
        try:
            return gt2t[typestring]
        except KeyError:
            return None

    @staticmethod
    def get_generic_type_from_type(typestring):
        t2gt = {
            "hkt": 0x08,
            "multisensor": 0x20
        }
        try:
            return t2gt[typestring]
        except KeyError:
            return None

    def get_type(self):
        return type(self).__name__

    def get_type_string(self):
        for c in self.TYPE_CHOICES:
            if self.type == c[0]:
                return c[1]

    def get_parameters(self):
        try:
            return ast.literal_eval(self.parameters)
        except SyntaxError:
            return {}

    def set_parameters(self, params):
        self.parameters = str(params)
        self.save()

    def get_link(self):
        return "<a style='color:#cb0963; white-space:normal;' target='_blank' href='/m_setup/" + str(self.haus_id) + \
               "/rf/'>%s</a>"

    def get_protocol_info(self):
        # the following breaks /ac/ and /acs/ for rfcontrollers. .get_protocol_info is only used in one of /ac/ and /acs/ and thus matching /ac/ and /acs/ in m_install_a doesnt work anymore.
        # info_dict = {
        #     'zwave': u"(weißer Funk-Stick)",
        #     'enocean': u"(transparenter Funk-Stick)"
        # }
        #
        # return ("%s %s" % (self.name, info_dict.get(str(self.protocol), ''))).rstrip(" ")
        return self.name

    class Meta:
        app_label = 'heizmanager'
        abstract = True


class RFController(RFDevice):
    rpi = models.ForeignKey(RPi, null=True)  # fuer Verschluesselung
    
    objects = CachingManager()

    def __init__(self, *args, **kwargs):
        self._sensoren = list()
        self._aktoren = list()
        super(RFController, self).__init__(*args, **kwargs)

    def __unicode__(self):
        return self.name

    def get_marker(self):
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        last = ch.get_gw_ping(self.name)

        if not last:
            params = self.get_parameters()
            last_data = params.get('last_data')
            if last_data:
                from . import views as getandset
                if self.protocol == 'zwave':
                    getandset.set_zwave(None, self.rpi.name, self.name, last_data)
                elif self.protocol == 'btle':
                    getandset.set_btle(None, self.rpi.name, self.name, last_data)
                last = ch.get_gw_ping(self.name)

        shortest_wakeup = self.get_shortest_wakeup()
        marker = 'red'
        if last and len(last) and last[0] and (now.replace(tzinfo=berlin)-last[0].replace(tzinfo=berlin)).total_seconds() < shortest_wakeup*4:
            marker = 'green'
        elif last and len(last) and last[0] \
                and shortest_wakeup*4 < (now.replace(tzinfo=berlin)-last[0].replace(tzinfo=berlin)).total_seconds() < shortest_wakeup*8:
            marker = 'yellow'
        return marker

    @staticmethod
    def get_marker(name, shortest_wakeup):
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        last = ch.get_gw_ping(name)

        marker = 'red'
        if shortest_wakeup is None:
            shortest_wakeup = 900
        if last and len(last) and last[0] and (now.replace(tzinfo=berlin)-last[0].replace(tzinfo=berlin)).total_seconds() < shortest_wakeup*4:
            marker = 'green'
        elif last and len(last) and last[0] \
                and shortest_wakeup*4 < (now.replace(tzinfo=berlin)-last[0].replace(tzinfo=berlin)).total_seconds() < shortest_wakeup*8:
            marker = 'yellow'
        return marker

    def get_shortest_wakeup(self):
        params = self.get_parameters()
        if self.protocol == 'zwave':
            return min(params.get("wakeup_intervals", dict()).get(0x84, dict()).values())
        elif self.protocol == 'btle':
            return max(min(params.get("wakeup_intervals", {'intervall': 60}).values()), 60)
        elif self.protocol == 'enocean':
            return max(min(params.get("wakeup_intervals", {'intervall': 900}).values()), 1800)

    def get_autark(self):
        params = self.get_parameters()
        if 'autark' in params:
            return params['autark']
        else:
            return False

    def get_sensoren(self):
        return self._sensoren

    def get_aktoren(self):
        return self._aktoren

    def get_identifier(self):
        return "%s*%s" % (type(self).__name__, self.id)

    @staticmethod
    def create_controller(rpi, controllerid, protocol):
        controller = RFController(haus=rpi.haus, name=controllerid.lower(), description=controllerid.lower(), protocol=protocol, type='ctrl', rpi=rpi)
        params = dict()
        if protocol == "btle":
            params['autark'] = True
            params['wakeup_intervals'] = {'intervall': 60}
        elif protocol == "zwave":
            params['wakeup_intervals'] = {0x84: {0x08: 300, 0x20: 300, 0x07: 300}}
            params['autark'] = False
        elif protocol == "enocean":
            params['autark'] = True
            params['wakeup_intervals'] = {'intervall': 1800}
        controller.set_parameters(params)
        controller.save()

        return controller

    @staticmethod
    def get_controller(rpimac, controllerid, protocol):

        try:
            controller = RFController.objects.get(name=controllerid.lower())
        except RFController.DoesNotExist:
            if '_' in controllerid or controllerid.startswith('0x000'):
                return None
            try:
                rpi = RPi.objects.get(name=rpimac.lower())
            except (RPi.DoesNotExist, AttributeError):
                return None
            controller = RFController.create_controller(rpi, controllerid, protocol)
        else:
            try:
                rpi = RPi.objects.get(name=rpimac.lower())
                if rpi != controller.rpi:
                    controller.rpi = rpi
                    controller.save()
            except (RPi.DoesNotExist, AttributeError):
                pass
            if controller.haus_id is None:
                try:
                    rpi = RPi.objects.get(name=rpimac.lower())
                    controller.haus_id = rpi.haus_id
                    controller.save()
                except (RPi.DoesNotExist, AttributeError):
                    pass

        return controller

    def decrypt_data(self, data):
        _data = data.replace(' ', '+')
        try:
            data = json.loads(
                self.rpi.crypt_keys.decrypt(_data).rsplit('}', 1)[0] + '}'
            )

        except (ValueError, TypeError, AttributeError):

            try:
                data = json.loads(data)
            except Exception as e:
                return None

        except RPi.DoesNotExist:
            return None

        return data

    def get_controller_settings(self):

        settings = {}

        if self.protocol == "zwave":
            settings['settings'] = dict()
            settings['settings']['wakeup_intervals'] = self.get_parameters().get('wakeup_intervals', {132: dict()})
            settings['settings']['wakeup_intervals'][132][33] = settings['settings']['wakeup_intervals'][132].get(32)

        elif self.protocol == "btle":
            params = self.get_parameters()
            settings['intervall'] = params.get('wakeup_intervals', dict()).get('intervall', 10)
            if not len(RFAktor.objects.filter(controller=self)):
                settings['intervall'] = 300
            settings['discovery'] = params.get('discovery', False)

        return settings

    def update_last_seen(self, data, last_data=False):

        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        cparams = self.get_parameters()

        if self.protocol == "zwave":
            least_seen = None
            last_seen = cparams.get('last_data', dict())
            if not isinstance(last_seen, dict):  # legacy
                last_seen = dict()
            for dev, vals in data.items():
                v = last_seen.get(dev, dict())
                if 'neighbors' in v and not len(v['neighbors']):
                    del v['neighbors']
                v.update(vals)
                if ('battery' in vals or len(vals.get('values', dict())) or vals.get('value') or 'Switch' in vals)\
                        or dev == '1':
                    prevls = v.get('ls')
                    v['prevls'] = prevls
                    v['ls'] = now.strftime("%Y-%m-%d %H:%M:%S")
                    if v['ls'] > least_seen:
                        least_seen = v['ls']
                last_seen[dev] = v
            cparams['last_data'] = last_seen

            if last_data and least_seen:
                tstamp = datetime.strptime(least_seen, "%Y-%m-%d %H:%M:%S")
                tstamp.replace(tzinfo=berlin)
                ch.set(key='getping_%s' % self.name, value=(tstamp, None))

        elif self.protocol == "btle":
            last_seen = cparams.get('last_data', dict())
            for dev, vals in data.items():
                if dev in {'last_seen'}:
                    continue
                v = last_seen.get(dev, dict())
                if vals['last_seen'] != v.get('last_seen', ''):
                    prevls = v.get('last_seen', '')
                    v.update(vals)
                    v['prevls'] = prevls
                last_seen[dev] = v
            prevls = cparams.get('last_seen')
            cparams['prevls'] = prevls
            cparams['last_seen'] = data['last_seen']  # now.strftime("%Y-%m-%d %H:%M")
            # ctrl_prevls = cparams['prevls']
            cparams['last_data'] = last_seen

        self.set_parameters(cparams)

    class Meta:
        app_label = 'heizmanager'


class RFAktor(RFDevice, AbstractSensor):

    controller = models.ForeignKey(RFController)
    value = models.FloatField(null=True)  # oder default value?
    values = models.TextField()
    battery = models.IntegerField(default=-1)
    locked = models.BooleanField(default=False)

    objects = CachingManager()

    def __unicode__(self):
        return "%s %s" % (self.name, self.description)

    def get_wert(self, val='Temperature'):
        try:
            last = ch.get_last_vals(self.controller.name+'_'+self.name+'_val')

            if val == 'Temperature':
                v = last[0][val]
                offset = self.get_temperature_offset()
                last = (v+offset, last[1], last[2], last[3])
            else:
                last = (last[0][val], last[1], last[2], last[3])  # damits einheitlich ist zu (v)sensor.get_wert()
        except (TypeError, KeyError, RFController.DoesNotExist):
            return None
        return last

    def is_possible_output(self):
        if self.type in ['relais', '2chrelais', 'wp', 'wpd2010b']:
            return True
        return False

    def get_marker(self, use_last_data=True):
        shortest_wakeup = self.controller.get_shortest_wakeup()
        if self.type in ["2chrelais", "wp", "wpd2010b"]:
            # marker = self.controller.get_marker()
            marker = RFController.get_marker(self.controller.name, shortest_wakeup)
            return marker

        elif self.type == "relais" and self.protocol == "enocean":
            params = self.get_parameters()
            if params.get('last_ack') and params.get('last_val') == params.get('last_ack_val'):
                return 'green'
            else:
                return 'red'

        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)

        if self.type == "undef":
            wakeup = shortest_wakeup
        else:
            params = self.controller.get_parameters()
            wakeup = params.get("wakeup_intervals", dict()).get(0x84, dict()).get(RFDevice.get_generic_type_from_type(self.type), 300 if self.protocol != 'enocean' else 1800)

        # das hier faengt nur, wenn eine default 300 aus dem else gekommen ist
        if wakeup < shortest_wakeup:
            wakeup = shortest_wakeup

        last = ch.get_gw_ping(self.controller.name+'_'+self.name)

        if not last and use_last_data:
            cparams = self.controller.get_parameters()
            last_data = cparams.get('last_data')
            if last_data:
                from . import views as getandset
                if self.protocol == 'zwave':
                    getandset.set_zwave(None, self.controller.rpi.name, self.controller.name, last_data)
                elif self.protocol == 'btle':
                    getandset.set_btle(None, self.controller.rpi.name, self.controller.name, last_data)
                last = ch.get_gw_ping(self.controller.name+'_'+self.name)

        marker = 'red'

        if last and len(last) == 2 and last[0] and last[1]:
            ndiff = (now.replace(tzinfo=berlin)-last[0].replace(tzinfo=berlin)).total_seconds()
        elif last and len(last) and last[0]:
            ldiff = wakeup * 6  # damit es das erste if schlaegt
            ndiff = (now.replace(tzinfo=berlin)-last[0].replace(tzinfo=berlin)).total_seconds()
        else:
            ldiff = 86401
            ndiff = 86401

        if ndiff < wakeup*4:
            marker = 'green'
        elif wakeup*4 < ndiff < wakeup*8:
            marker = 'yellow'

        return marker

    def get_temperature_offset(self):
        # hier kann man noch pruefen, ob dieser sensor ueberhaupt temperatur misst, und wenn nein, None zurueckgeben
        return self.get_parameters().get('toffset', 0.0)

    def get_name(self):
        return "%s - %s" % (self.controller.name, self.name)

    def update_device(self, data, last_data, **kwargs):

        devparams = self.get_parameters()
        update_raumsoll = self.raum is not None
        controllerid = self.controller.name.lower()
        last_vals = ch.get_last_vals("%s_%s_val" % (controllerid, self.name))
        if last_vals is not None:
            last_vals = last_vals[0]
        vals = {}
        soll = None
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)

        if self.protocol == "zwave":

            if not self.locked and not self.type.startswith("hkt"):
                if 'value' in data and 1 < data['value'] < 40:
                    if self.type == 'wt' and data['value'] > 4:
                        vals = {'Heating 1': data['value']}
                    elif data['value'] > 4:
                        vals = {'Temperature': data['value']}
                if 'updval' in data:
                    vals['updval'] = data['updval']
                if 'values' in data:
                    if self.type == 'wt' and float(data['values'].get('Temperature', 4)) == 4:
                        del data['values']['Temperature']
                    vals.update(data['values'])
                if 'Switch' in data:
                    vals['Switch'] = data['Switch']

            elif self.locked and self.type == 'wt':
                if 'values' in data:
                    vals.update(data['values'])

            try:
                vals['battery'] = data['battery']
            except KeyError:
                pass

            if self.raum_id and 'Heating 1' in vals and round(self.raum.solltemp) != round(vals.get('Heating 1')) and \
                    ((last_vals and 'Heating 1' in last_vals and round(float(vals.get('Heating 1'))) != round(float(last_vals.get('Heating 1')))) or (round(float(vals.get('updval', -99))) == round(self.raum.solltemp))):
                update_raumsoll = True
                soll = float(vals.get('Heating 1'))
            else:
                update_raumsoll = False

            if last_vals:
                last_vals.update(vals)
                vals = last_vals

            if 'Temperature' in data.get('values', dict()) and not last_data and self.controller.haus and float(data['values']['Temperature']) > 4:
                modules = self.controller.haus.get_modules()
                offset = self.get_temperature_offset()
                sig_new_temp_value.send(sender="tset", name=self.get_identifier(), value=data['values']['Temperature']+offset, modules=modules, timestamp=datetime.now())
                logger.warning("0|%s|%s,%s" % (controllerid, "%s_%s" % (controllerid, self.name), data['values']['Temperature']+offset))

            if len(data.get('values', [])) or data.get('value', None) or 'battery' in data or 'Switch' in data:
                if last_data:
                    ls = data.get('ls')
                    prevls = data.get('prevls')
                    if ls:
                        tstamp = datetime.strptime(ls, "%Y-%m-%d %H:%M:%S")
                        tstamp.replace(tzinfo=berlin)
                        if prevls:
                            prevls = datetime.strptime(prevls, "%Y-%m-%d %H:%M:%S")
                            prevls.replace(tzinfo=berlin)
                        ch.set(key='getping_%s_%s' % (controllerid, self.name), value=(tstamp, prevls))

                else:
                    if not (self.type.startswith("hkt") and 'value' not in data):
                        ch.set_gw_ping("%s_%s" % (controllerid, self.name))

        elif self.protocol == "btle":
            if not last_data and 'last_seen' in data:
                ls = data.get('last_seen')
                try:
                    tz = pytz.timezone("Europe/Berlin")
                    ls = datetime.strptime(ls, "%Y-%m-%d %H:%M")
                    if (datetime.now(tz)-tz.localize(ls)).total_seconds() > self.controller.get_shortest_wakeup():
                        return
                except:
                    pass

            is_new_led = data.get('isNewLed')
            try:
                ist = float(data.get('ist'))
                offset = self.get_temperature_offset()
                ist += offset
            except (ValueError, TypeError):
                ist = None
            try:
                soll = float(data.get('soll'))
            except:
                soll = None
            offset = data.get('offset')
            battery = data.get('battery')
            dt = data.get('datetime', [0, 0, 0, 0, 0])

            vals = {'ist': ist, 'Temperature': ist, 'soll': soll, 'offset': offset, 'battery': battery, 'datetime': dt}

            # if not self.raum_id:
            #     self.get_parameters().get('configtemp', 21.0)

            if last_vals is not None and last_vals.get('ist') is not None and (ist is None or (math.fabs(last_vals['ist'] - ist) > 7 and not (last_vals.get('ist') < 5 or last_vals.get('ist') > 40))):
                vals['ist'] = last_vals['ist']
                vals['Temperature'] = last_vals['ist']
                ist = None

            if ist is not None and last_data is False and self.controller.haus:
                modules = self.controller.haus.get_modules()
                sig_new_temp_value.send(sender="tset", name=self.get_identifier(), value=ist, modules=modules, timestamp=datetime.now())
                logger.warning("0|%s|%s,%s" % (controllerid, self.name, ist))

            if self.raum_id:
                hktlast = ch.get('hktlast_%s' % self.raum_id)
            else:
                hktlast = None
            if (hktlast is not None and hktlast[0] == self.raum.solltemp) and ((self.type == "hktControme" and self.raum_id) or (kwargs['prevlseq'] and (is_new_led or (16.0 <= soll <= 28.5)))):
                update_raumsoll = True
            else:
                update_raumsoll = False

            cparams = self.controller.get_parameters()
            ls = cparams['last_data'].get(self.name, dict()).get('last_seen', '')
            if data.get('datetime', [0, 0, 0, 0, 0]) != [0, 0, 0, 0, 0] or self.type in ['hktControme', 'hkteTRV']:
                ch.set_gw_ping("%s_%s" % (controllerid, self.name))
            elif len(ls) and ls < (now - timedelta(seconds=60)).strftime("%Y-%m-%d %H:%M"):
                pass
            else:
                if last_data:
                    ls = data.get('last_seen')
                    _prevls = data.get('prevls')
                    dt = data.get('datetime', [0, 0, 0, 0, 0])
                    if ls and dt != [0, 0, 0, 0, 0]:
                        if ls.count(':') == 1:
                            ls += ':00'
                        tstamp = datetime.strptime(ls, "%Y-%m-%d %H:%M:%S")
                        tstamp.replace(tzinfo=berlin)
                        if _prevls:
                            if _prevls.count(':') == 1:
                                _prevls += ':00'
                            _prevls = datetime.strptime(_prevls, "%Y-%m-%d %H:%M:%S")
                            _prevls.replace(tzinfo=berlin)
                        ch.set(key='getping_%s_%s' % (controllerid, self.name), value=(tstamp, _prevls))

        ch.set_new_vals("%s_%s_val" % (controllerid, self.name), vals, self.controller.haus_id)

        if (self.type.startswith("hkt") or self.type == "wt") and soll is not None and (soll != 21.0 or not devparams.get('twentyonelocked', False)) and not last_data and not self.locked and update_raumsoll and self.raum.solltemp != soll:

            if not devparams.get('switchmode', False):

                mode = BaseMode.get_active_mode_raum(self.raum)
                if mode is not None:
                    mode['activated_mode'].set_mode_temperature_raum(self.raum, soll)

                else:
                    self.raum.solltemp = soll
                    self.raum.save()

                logger.warning(u"%s|%s" % (self.raum.id, u"Solltemperatur via ext. Thermostat auf %.2f gesetzt" % soll))  # hier, weil rf_solltemp_change:22 einen fehler wirft

            else:

                default_duration = self.controller.haus.get_module_parameters().get('quickui', {}).get('default_duration', 180)
                qum = QuickuiMode(soll, default_duration)
                qum.set_mode_raum(self.raum, log=False)

                logger.warning(u"%s|%s" % (self.raum.id, u"Solltemperatur via ext. Thermostat für %s Minuten auf %.2f gesetzt" % (default_duration, soll)))

    def get_device_settings(self):

        ret = {}

        def _update_hktlast():
            # brauchen wir nur für btle hkt
            cparams = self.controller.get_parameters()
            last_val = ch.get("hktlast_%s" % self.raum_id)
            if not last_val:
                hktlast = cparams.get('hktlast', dict())
                if self.raum_id in hktlast:
                    ch.set("hktlast_%s" % self.raum_id, hktlast[self.raum_id])
                    last_val = hktlast[self.raum_id]
            if last_val:
                if last_val[0] == round(self.raum.solltemp, 1) and last_val[1] == round(ret['offset']+ret.get('zieloffset', 0.0), 1):
                    lv = last_val
                else:
                    lv = (round(self.raum.solltemp, 1), round(ret['offset']+ret.get('zieloffset', 0.0), 1), last_val[0], last_val[1])
                ch.set("hktlast_%s" % self.raum_id, lv)
                cparams.setdefault('hktlast', dict())
                cparams['hktlast'][self.raum_id] = lv
                self.controller.set_parameters(cparams)
            else:
                ch.set("hktlast_%s" % self.raum_id, (round(self.raum.solltemp, 1), round(ret['offset']+ret.get('zieloffset', 0.0), 1), None, None))

        if self.raum_id:
            reg = self.raum.get_regelung()
            if reg and reg.regelung == "funksollregelung":
                val = reg.do_rfausgang(self.controller.id)
                ret.update(val.get(self.name, {}))
                try:
                    params = reg.get_parameters()
                    params.update({'regelung': 'funksollregelung'})
                    sig_new_output_value.send(sender='get_all', name='%s_%s' % (self.controller.name, self.name), value=1, ziel=val[self.name]['soll']+val[self.name]['offset'], parameters=params, timestamp=datetime.now(), modules=self.haus.get_modules())
                except Exception:
                    pass
                else:
                    _update_hktlast()

            elif reg and reg.regelung in {'zweipunktregelung', 'ruecklaufregelung'}:  # todo normale hkt auch in raeumen mit zpr/rlr erlauben
                from heizmanager.mobile.m_temp import get_module_offsets
                ret['soll'] = self.raum.solltemp
                ret['offset'] = get_module_offsets(self.raum)
                ret['zieloffset'] = 0.0
                _update_hktlast()

        if self.protocol == "btle":

            if self.type == "hkteTRV":
                ret['secret'] = self.get_parameters().get('secret', '')
                ret['typ'] = 'eTRV'
                ret['offset'] = 0.0
                ret.setdefault('soll', self.get_parameters().get('configtemp', 21.0))
            elif self.type == "hkt":
                ret['typ'] = 'Comet Blue'
                ret['offset'] = (-ret.get('offset', 0)) + (-ret.get('zieloffset', 0.0))
                ret.setdefault('soll', self.get_parameters().get('configtemp', 21.0))
            elif self.type == "hktGenius":
                ret['typ'] = 'Genius'
                ret['offset'] = ret.get('offset', 0.0) + ret.get('zieloffset', 0.0)
                ret.setdefault('soll', self.get_parameters().get('configtemp', 21.0))
            elif self.type == "hktControme":
                ret['typ'] = 'Controme RBDG10'
                # ret.setdefault('offset', 0.0)
                ret['offset'] = 0.0

        elif self.protocol == "zwave" and self.raum_id:

            ziel = ret['soll'] + ret['offset'] + ret.get('zieloffset', 0.0)
            ret = {}  # nochmal zuruecksetzen wegen zwavehandler behandlung
            aparams = self.get_parameters()
            if aparams.get('manufacturer_id') == "0148" and aparams.get('product_type') == "0001" and aparams.get('product_id') == "0001":  # stella z
                ret[0x40] = ("", "Comfort")
                ret[0x43] = ("Comfort setpoint", min(round(ziel, 1), 50.0))

            elif 'product_id' in aparams and self.type != 'wt':  # im moment: dlc
                ret[0x43] = ("", float("%.1f" % min(ziel, 28.0)))

            elif self.type == 'wt' and self.raum:
                ret[0x43] = ("Heating 1", round(self.raum.solltemp or 21.0))

        return ret

    def get_device_settings_for_output(self, val, regelung):

        ret = {}

        if self.protocol == "zwave":

            ret[self.name] = {}

            if self.type in ['wp', 'relais', 'wpd2010b']:
                ret[self.name][0x25] = ("", 1) if int(val) > 0 else ("", 0)

            elif self.type == '2chrelais':
                aparams = self.get_parameters()
                try:
                    ms = regelung.raum.get_mainsensor()
                except:
                    ms = None
                if ms is not None:
                    retvals = []
                    if ms.id == aparams.get('2ch', dict()).get(1):
                        retvals.append((2, 1) if val > 0 else (2, 0))
                    if ms.id == aparams.get('2ch', dict()).get(2):
                        retvals.append((3, 1) if val > 0 else (3, 0))

                    ret[self.name].setdefault(0x25, list())
                    ret[self.name][0x25] += retvals

            else:
                ret[self.name][0x25] = ("", val)

        return ret

    @property
    def ctrl_device(self):
        return self.controller

    class Meta:
        app_label = 'heizmanager'
        ordering = ['name']


class RFSensor(RFDevice, AbstractSensor):
    controller = models.ForeignKey(RFController)
    values = models.TextField()
    battery = models.IntegerField(default=-1)
    
    objects = CachingManager()

    def __unicode__(self):
        return "%s %s" % (self.name, self.description)

    def get_wert(self, val='Temperature'):
        trans = {'Luminance': 'Helligkeit', 'Temperature': 'Temperatur', 'Relative Humidity': 'Relative Luftfeuchte'}
        try:
            last = ch.get_last_vals(self.controller.name+'_'+self.name+'_val')

            if val == 'Temperature':
                v = last[0][val]
                offset = self.get_temperature_offset()
                last = (v+offset, last[1], last[2], last[3])
            else:
                last = (last[0][val], last[1], last[2], last[3])  # damits einheitlich ist zu (v)sensor.get_wert()
        except (TypeError, KeyError, RFController.DoesNotExist):
            return None
        return last

    def get_temperature_offset(self):
        # hier kann man noch pruefen, ob dieser sensor ueberhaupt temperatur misst, und wenn nein, None zurueckgeben
        return self.get_parameters().get('toffset', 0.0)

    def _get_seriennummer(self):
        # return self.name
        return self.id
    seriennummer = property(_get_seriennummer)

    def get_marker(self, use_last_data=True):
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)

        shortest_wakeup = self.controller.get_shortest_wakeup()
        if self.type == "undef":
            wakeup = shortest_wakeup
        else:
            params = self.controller.get_parameters()
            wakeup = params.get("wakeup_intervals", dict()).get(0x84, dict()).get(RFDevice.get_generic_type_from_type(self.type), 300 if self.protocol != 'enocean' else 1800)

        # das hier faengt nur, wenn eine default 300 aus dem else gekommen ist
        if wakeup < shortest_wakeup:
            wakeup = shortest_wakeup

        last = ch.get_gw_ping(self.controller.name+'_'+self.name)

        if not last and use_last_data:
            cparams = self.controller.get_parameters()
            last_data = cparams.get('last_data')
            if last_data:
                from . import views as getandset
                if self.protocol == 'zwave':
                    getandset.set_zwave(None, self.controller.rpi.name, self.controller.name, last_data)
                elif self.protocol == 'btle':
                    getandset.set_btle(None, self.controller.rpi.name, self.controller.name, last_data)
                last = ch.get_gw_ping(self.controller.name+'_'+self.name)

        marker = 'red'

        if last and len(last) == 2 and last[0] and last[1]:
            ndiff = (now.replace(tzinfo=berlin)-last[0].replace(tzinfo=berlin)).total_seconds()
        elif last and len(last) and last[0]:
            ldiff = wakeup * 6  # damit es das erste if schlaegt
            ndiff = (now.replace(tzinfo=berlin)-last[0].replace(tzinfo=berlin)).total_seconds()
        else:
            ldiff = 86401
            ndiff = 86401

        if ndiff < wakeup*4:
            marker = 'green'
        elif wakeup*4 < ndiff < wakeup*8:
            marker = 'yellow'

        return marker

    def get_name(self):
        return "%s - %s" % (self.controller.name, self.name)

    def update_device(self, data, last_data=None):

        if self.protocol == "zwave":

            vals = data.get('values', dict())
            try:
                vals['battery'] = data['battery']
            except KeyError:
                pass

            if 'Temperature' in data.get('values', dict()) and not last_data and self.controller.haus:
                modules = self.controller.haus.get_modules()
                offset = self.get_temperature_offset()
                sig_new_temp_value.send(sender="tset", name=self.get_identifier(), value=data['values']['Temperature']+offset, modules=modules, timestamp=datetime.now())
                logger.warning("0|%s|%s,%s" % (self.controller.name.lower(), "%s_%s" % (self.controller.name.lower(), self.name), data['values']['Temperature']+offset))

            if len(vals):
                last = ch.get_last_vals("%s_%s_val" % (self.controller.name, self.name))
                if last and last[0]:
                    if 'Alarm' in last[0] and 'Alarm' not in vals:
                        vals['Alarm'] = False
                    last_temp = last[0].get('Temperature')
                    last_vals = copy.deepcopy(last[0])
                    last_vals.update(vals)
                else:
                    last_vals = vals
                    last_temp = None

                if 'Temperature' in last_vals and data.get('product_type') == '0002' and data.get('manufacturer_id') == '0086' and data.get('product_id') == '0064':
                    if -12.0 < last_vals['Temperature'] < 14.0:
                        # auf jeden Fall Celsius
                        pass
                    elif last_vals['Temperature'] > 50.0:
                        # auf jeden Fall Fahrenheit
                        last_vals['Temperature'] = (last_vals['Temperature']-32.0)*5.0/9.0
                    elif last_temp and math.fabs(last_vals['Temperature']-last_temp) >= 24.0:
                            if last_vals['Temperature'] < last_temp:
                                # last_vals ist Celsius
                                pass
                            else:
                                # last_vals ist Fahrenheit
                                last_vals['Temperature'] = (last_vals['Temperature']-32.0)*5.0/9.0

                if vals.get('Alarm', False) and self.raum_id:
                    if 'praesenzregelung' in self.raum.get_modules() and last_data is None:
                        ch.delete("%s_roffsets_dict" % self.raum_id)

                    try:
                        local("/usr/bin/python /home/pi/rpi/telegrambot/bot.py %s %s" % (self.controller.haus_id, self.raum_id))
                    except Exception:
                        logging.exception("exception sending msg via telegram")

                ch.set_new_vals("%s_%s_val" % (self.controller.name, self.name), last_vals, self.controller.haus_id)

            if len(data.get('values', [])) or data.get('value', None) or 'battery' in data or 'Switch' in data:
                if last_data:
                    ls = data.get('ls')
                    prevls = data.get('prevls')
                    if ls:
                        tstamp = datetime.strptime(ls, "%Y-%m-%d %H:%M:%S")
                        berlin = pytz.timezone('Europe/Berlin')
                        tstamp.replace(tzinfo=berlin)
                        if prevls:
                            prevls = datetime.strptime(prevls, "%Y-%m-%d %H:%M:%S")
                            prevls.replace(tzinfo=berlin)
                        ch.set(key='getping_%s_%s' % (self.controller.name, self.name), value=(tstamp, prevls))

                else:
                    if not (self.type.startswith("hkt") and 'value' not in data):
                        ch.set_gw_ping("%s_%s" % (self.controller.name, self.name))

    def get_device_settings(self):
        pass

    @property
    def ctrl_device(self):
        return self.controller

    class Meta:
        app_label = 'heizmanager'


class RFAusgang(AbstractOutput):
    controller = models.ForeignKey(RFController, related_name='rfausgang')
    aktor = ListField(models.ForeignKey(RFAktor), default=[])

    objects = CachingManager()

    def save(self, *args, **kwargs):
        tb = traceback.extract_stack()
        tb_files = [t[0] for t in tb]
        tb_methods = [t[2] for t in tb]

        if 'ausgaenge_einrichtung' in tb_methods or 'raum_editieren' in tb_methods or 'gateway_editieren' in tb_methods\
                or './pumpenlogik/views.py' in tb_files\
                or './solarpuffer/views.py' in tb_files\
                or './vsensoren/models.py' in tb_files:
            super(RFAusgang, self).save(*args, **kwargs)

        else:
            macaddr = nh.get_mac()
            ret = {
                'tb': tb
            }
            try:
                requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % aes_encrypt(json.dumps(ret)), timeout=10)
            except:
                pass

    def delete(self, *args, **kwargs):
        tb = traceback.extract_stack()
        tb_files = [t[0] for t in tb]
        tb_methods = [t[2] for t in tb]

        if 'ausgaenge_einrichtung' in tb_methods or 'raum_editieren' in tb_methods or 'etage_editieren' in tb_methods \
                or 'gateway_editieren' in tb_methods\
                or './pumpenlogik/views.py' in tb_files\
                or './solarpuffer/views.py' in tb_files\
                or './vsensoren/models.py' in tb_files\
                or './vsensoren/views.py' in tb_files:
            super(RFAusgang, self).delete(*args, **kwargs)

        else:
            macaddr = nh.get_mac()
            ret = {
                'tb': tb
            }
            try:
                requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % aes_encrypt(json.dumps(ret)), timeout=10)
            except:
                pass

    def get_output_settings(self):

        try:
            val = int(self.regelung.do_ausgang(0).content.translate(None, "<>!"))
        except:
            return {}

        ret = {}
        reg = self.regelung
        regparams = reg.get_parameters()

        for aktor_id in self.aktor:

            try:
                aktor = RFAktor.objects.get(pk=int(aktor_id))
            except RFAktor.DoesNotExist:
                continue

            aktorval = aktor.get_device_settings_for_output(val, reg)
            ret.update(aktorval)

            ch.set("zwave_rfausgang_%s%s" % (self.controller.name, aktor.name), (val, datetime.utcnow()))

            regparams.update({'regelung': reg.regelung})
            if reg.regelung in {'zweipunktregelung', 'ruecklaufregelung'}:
                ziel = reg.raum.solltemp + get_module_offsets(reg.raum, only_clear_offsets=True)
            else:
                ziel = 0.0
            sig_new_output_value.send(sender='get_all', name='%s_%s' % (self.controller.name, aktor.name), value=val, ziel=ziel, parameters=regparams, timestamp=datetime.now(), modules=self.controller.haus.get_modules())

        return ret

    def get_regelung(self):
        if self.regelung_id:
            regelung = self.regelung
            return regelung

        return None

    class Meta:
        app_label = 'heizmanager'


@dispatch.receiver(post_init, sender=Raum)
def _set_raum_sensoren(sender, instance, **kwargs):
    rfsensoren = RFSensor.objects.filter(raum=instance)
    instance._sensoren += list(rfsensoren)
    rfaktoren = RFAktor.objects.filter(raum=instance, type='wt')
    instance._sensoren += list(rfaktoren)
    rfaktoren = RFAktor.objects.filter(raum=instance, type__startswith='hkt', protocol='btle')
    instance._sensoren += list(rfaktoren)


@dispatch.receiver(post_init, sender=RFController)
def _set_rfcontroller_sensoren(sender, instance, **kwargs):
    sensoren = chain(list(RFSensor.objects.filter(controller=instance)), list(RFAktor.objects.filter(controller=instance, type__in=['wt', 'hktControme'])))
    for s in sensoren:
        instance._sensoren.append(s)


@dispatch.receiver(post_init, sender=RFController)
def _set_rfcontroller_aktoren(sender, instance, **kwargs):
    aktoren = chain(
        list(RFAktor.objects.filter(controller=instance, type__startswith='hkt')),
        list(RFAktor.objects.filter(controller=instance, type='relais')),
        list(RFAktor.objects.filter(controller=instance, type='wp')),
        list(RFAktor.objects.filter(controller=instance, type='wpd2010b'))
    )
    for a in aktoren:
        instance._aktoren.append(a)


@dispatch.receiver(post_delete, sender=RFSensor)
@dispatch.receiver(post_delete, sender=RFAktor)
@dispatch.receiver(post_save, sender=RFSensor)
@dispatch.receiver(post_save, sender=RFAktor)
def _invalidate_sensor_cache(sender, instance, **kwargs):
    ch.delete(instance.get_identifier())


@dispatch.receiver(sig_get_outputs_from_output)
def rf_get_outputs_from_output(sender, **kwargs):
    raum = kwargs['raum']
    regelung = kwargs['regelung']
    try:
        ausgang = RFAusgang.objects.get(regelung=regelung)
    except RFAusgang.DoesNotExist:
        return []

    aktoren = []
    for aktor in ausgang.aktor:
        try:
            aktoren.append(RFAktor.objects.get(pk=int(aktor)).name)
        except RFAktor.DoesNotExist:
            ausgang.aktor.remove(aktor)
            ausgang.save()
    aktoren = sorted(aktoren)  #, key=lambda a: a.name)

    return [(ausgang.controller, raum, regelung, ausgang, aktoren)]
    # return [(a.controller, raum, regelung, a, [ak.name for ak in a.aktor]) for a in ausgaenge]


@dispatch.receiver(sig_get_all_outputs_from_output)
def rf_get_all_outputs_from_output(sender, **kwargs):
    ausgaenge = RFAusgang.objects.all().select_related('controller', 'regelung')

    haus = kwargs['haus']

    def get_raum(reg_id):
        try:
            return Raum.objects.get_for_user(haus.eigentuemer, regelung_id=reg_id)
        except Raum.DoesNotExist:
            return None

    ret = []
    for ausgang in ausgaenge:
        aktoren = []
        for aktor in ausgang.aktor:
            try:
                aktoren.append(RFAktor.objects.get(pk=int(aktor)).name)
            except RFAktor.DoesNotExist:
                ausgang.aktor.remove(aktor)
                ausgang.save()
        aktoren = sorted(aktoren)

        ret.append((ausgang.controller, get_raum(ausgang.regelung_id), ausgang.regelung, ausgang, aktoren))

    return ret


@dispatch.receiver(sig_get_possible_outputs_from_output)
def rf_get_possible_outputs_from_output(sender, **kwargs):
    haus = kwargs['haus']
    ret = {}
    controllers = RFController.objects.all()
    rfcdict = dict((rfc.id, rfc) for rfc in controllers)
    for raum in Raum.objects.select_related('regelung').filter(regelung__regelung=sender):
        ret.setdefault(raum, list())
        for sensor in chain(list(RFSensor.objects.filter(raum_id=raum.id)), list(RFAktor.objects.filter(raum_id=raum.id, type__in=['wt', 'hktControme']))):
            ret[raum].append((sensor, rfcdict[sensor.controller_id]))

    return ret


@dispatch.receiver(sig_get_all_outputs)
def rf_get_all_outputs(sender, **kwargs):
    ret_rfcs = dict()
    rfcs = RFController.objects.all()

    for rfc in rfcs:
        rfas = RFAktor.objects.filter(controller=rfc)
        if not len(rfas) or not rfc.rpi:
            continue
        ret_rfcs[rfc] = (
            str(rfc.id) + '##' + rfc.get_protocol_info() + '##' + rfc.get_type(),
            dict((rfa.name, [rfa.description]) for rfa in rfas if rfa.type in ['wp', 'relais', 'wpd2010b'])
        )
    return ret_rfcs


@dispatch.receiver(sig_get_ctrl_devices)
def rf_get_all_rfcs(sender, **kwargs):
    haus = kwargs['haus']
    return haus.rfcontroller_related.all()


@dispatch.receiver(sig_create_output)
def rf_create_output(sender, **kwargs):
    haus = kwargs['haus']
    ctrldev = kwargs['ctrldevice']
    regelung = kwargs['regelung']
    sensor = kwargs['sensor']
    outs = kwargs['outs']
    if type(ctrldev).__name__ in globals():
        try:
            ausgang = regelung.rfausgang
        except RFAusgang.DoesNotExist:
            ausgang = RFAusgang(
                regelung=regelung, controller=ctrldev
            )
        if sender == 'create':
            ausgang.aktor = []
        for o in outs:
            try:
                aktor = RFAktor.objects.get(controller=ctrldev, name=o)
                ausgang.aktor += [aktor.id]
            except RFAktor.DoesNotExist:
                pass

        if not sender == 'delete':
            ausgang.save()

        sig_create_output_regelung.send(sender=sender, ctrldevice=ctrldev, regelung=regelung, sensor=sensor, outs=outs)
