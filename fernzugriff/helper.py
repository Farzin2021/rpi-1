import requests
from fabric.api import local


def check_setup():
    try:
        r = local("dpkg -s openvpn", capture=True)
    except:
        local("sudo apt-get update")
        local("sudo apt-get install -y openvpn")
    try:
        local("ls /etc/openvpn/down.sh")
    except:
        local("sudo cp /home/pi/rpi/config/openvpn_down.sh /etc/openvpn/down.sh")
        local("sudo chmod +x /etc/openvpn/down.sh")
        local("sudo cp /home/pi/rpi/config/openvpn_up.sh /etc/openvpn/up.sh")
        local("sudo chmod +x /etc/openvpn/up.sh")
        local("sudo cp /home/pi/rpi/config/openvpn.conf /etc/openvpn/openvpn.conf")


def get_keys():

    with open('/sys/class/net/eth0/address') as f:
        mac = f.read()
        mac = mac.strip().replace(':', '-').lower()

    try:
        local("sudo ls /etc/openvpn/client.crt /etc/openvpn/client.key /etc/openvpn/ca.crt /etc/openvpn/ta.key")
        # und wenn das funktioniert hat, zweiter check:
        ip = local("ip -4 addr show dev tun0 | grep inet | awk '{print $2;}'", capture=True)
        if "does not exist" not in ip:
            ip = ip[:-3].split('.')
            if int(ip[1]) != int(mac[:2], 16) or int(ip[2]) != int(mac[2:4], 16) or int(ip[3]) != int(mac[4:], 16):
                local("sudo rm /etc/openvpn/ca.crt /etc/openvpn/client.crt /etc/openvpn/client.key /etc/openvpn/ta.key")
                raise Exception("wrong keys")
    except:
        mac = mac.lower().replace('-', '')[-6:]
        vac = local("cat /home/pi/rpi/rpi/verification_code.py", capture=True).strip().rsplit(' ', 1)[1].replace("'", "")
        ret = requests.get("https://fwd.controme.com/ctrl/keys/ms%s/?vac=%s" % (mac, vac))
        with open("/home/pi/rpi/fernzugriff/keydl.tar", "wb") as tar:
            tar.write(ret.content)
        local("sudo tar -xf /home/pi/rpi/fernzugriff/keydl.tar")
        local("sudo mv /home/pi/rpi/ca.crt /etc/openvpn/ca.crt")
        local("sudo mv /home/pi/rpi/ms%s.crt /etc/openvpn/client.crt" % mac)
        local("sudo mv /home/pi/rpi/ms%s.key /etc/openvpn/client.key" % mac)
        local("sudo mv /home/pi/rpi/ta.key /etc/openvpn/ta.key")
        local("sudo rm /home/pi/rpi/fernzugriff/keydl.tar")


def add_systemd_conditional():
    local("sudo systemctl enable openvpn@openvpn.service")
    f = open("/etc/systemd/system/multi-user.target.wants/openvpn@openvpn.service", "r")
    contents = f.readlines()
    f.close()

    j = 0
    for i, line in enumerate(contents):
        if "[Unit]" in line:
            j = i+1
        if 'ConditionPathExists' in line:
            j = None
    if j is not None:
        contents.insert(j, "ConditionPathExists=/home/pi/rpi/fernzugriff/run.pid\n")

    f = open("/etc/systemd/system/multi-user.target.wants/openvpn@openvpn.service", "w")
    contents = "".join(contents)
    f.write(contents)
    f.close()


if __name__ == "__main__":
    check_setup()
    get_keys()
    add_systemd_conditional()