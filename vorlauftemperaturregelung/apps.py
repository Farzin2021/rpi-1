from django.apps import AppConfig


class VorlauftemperaturregelungConfig(AppConfig):
    name = 'vorlauftemperaturregelung'
    verbose_name = 'Vorlauftemperaturregelung'

    def ready(self):
        from . import signals
