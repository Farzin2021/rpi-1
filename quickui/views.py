
# -*- coding: utf-8 -*-

import pytz
from datetime import datetime, timedelta
from benutzerverwaltung.decorators import multizone_quick_access
from django.views.decorators.csrf import csrf_exempt
from heizmanager.mobile.m_temp import _get_haus_for_user, get_module_offsets, get_offsets_icon
from heizmanager.models import Raum
import heizmanager.cache_helper as ch
import logging
import json
from heizmanager.render import render_response, render_redirect
from django.http import HttpResponse
from django.conf import settings
from heizmanager.abstracts.base_mode import BaseMode
import helpers
from heizmanager.abstracts.base_time_task import TimeTask

logger = logging.getLogger("logmonitor")


class QuickuiMode(BaseMode):
    MOD = 'quickui'

    def __init__(self, temperature, duration):
        self._temperature = float(str(temperature).replace(',', '.'))
        self._duration = int(duration)
        self.now = datetime.now(pytz.timezone('Europe/Berlin'))

    @property
    def temperature(self):
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        self._temperature = value

    @property
    def duration(self):
        return self._duration

    @duration.setter
    def duration(self, value):
        self._duration = value

    def set_mode_raum(self, raum, *args, **kwargs):

        # in case raum went stale between uwsgi and celery
        raum = Raum.objects.filter(pk=raum.id)[0]

        ex_time = self.now + timedelta(minutes=self.duration)
        ex_time = helpers.get_nearest_time(ex_time)
        time_to_ret = ex_time.strftime("%H:%M")
        ex_time = helpers.get_str_formatted_time(ex_time)

        ret_dict = dict()
        ret_dict['time'] = time_to_ret
        ret_dict['full_expire_time'] = ex_time
        ch.set("room_time_%s" % raum.id, (self.duration, ex_time, self.temperature), time=(self.duration * 60) + 1)

        # set hand-mode to db
        r_params = raum.get_module_parameters()

        r_params['temp_solltemp'] = raum.solltemp

        q_r_params = r_params.get('quickui', dict())
        q_r_params['ziel_val'] = self.temperature
        q_r_params['room_time'] = (self.duration, ex_time, self.temperature)
        r_params['quickui'] = q_r_params

        raum.set_module_parameters(r_params)

        raum.solltemp = self.temperature
        raum.save()

        if kwargs.get('log', False):
            logger.warning(u"%s|%s" % (raum.id, u"Zieltemperatur via QuickUI auf %.2f gesetzt." % raum.solltemp))

        return ret_dict

    def set_mode_temperature_raum(self, raum, temperature):
        qparams = self.hand_mode_params(raum)
        if qparams:
            ex_time = helpers.ret_formatted_time_from_str(qparams[1]).replace(tzinfo=pytz.timezone("Europe/Berlin"))
            now = datetime.now(pytz.timezone('Europe/Berlin'))
            t = (ex_time-now).total_seconds()
            ch.set("room_time_%s" % raum.id, (qparams[0], qparams[1], temperature), time=t+1)
            r_params = raum.get_spec_module_params("quickui")
            r_params['room_time'] = (qparams[0], qparams[1], temperature)
            raum.set_spec_module_params("quickui", r_params)
            raum.solltemp = temperature
            raum.save()
            return True
        return False

    @staticmethod
    def is_expired(str_time):
        now = datetime.now(pytz.timezone('Europe/Berlin'))
        return now.replace(tzinfo=None) > helpers.ret_formatted_time_from_str(str_time)

    @staticmethod
    def hand_mode_params(raum):
        return ch.get('room_time_%s' % raum.id) or\
               raum.get_spec_module_params(QuickuiMode.MOD).get('room_time')

    @classmethod
    def get_mode_raum(cls, raum, *args, **kwargs):
        quick_mode = QuickuiMode.hand_mode_params(raum)
        ret = dict()
        # check there is hand-mode entry and is not expired
        if quick_mode and not QuickuiMode.is_expired(quick_mode[1]):
            ret['soll'] = quick_mode[2]
            ret['activated_mode_text'] = (datetime.strptime(quick_mode[1], '%Y-%m-%d %H:%M')).strftime("%H:%M")
            ret['activated_mode_id'] = -1
            ret['activated_mode'] = QuickuiMode(quick_mode[2], quick_mode[0])
            ret.update(QuickuiMode.get_capabilities(raum.etage.haus))
            return ret
        elif quick_mode and QuickuiMode.is_expired(quick_mode[1]):
            QuickuiMode.expire_hand_mode(raum)
        return None

    @staticmethod
    def expire_hand_mode(raum):
        # in case things went stale
        if isinstance(raum, int):
            raum = Raum.objects.filter(id=raum)[0]
        else:
            raum = Raum.objects.filter(pk=raum.id)[0]

        r_params = raum.get_module_parameters()
        q_r_params = r_params.get(QuickuiMode.MOD, dict())
        ch.set("room_time_%s" % raum.id, None)
        q_r_params['room_time'] = dict()
        r_params[QuickuiMode.MOD] = q_r_params
        raum.set_module_parameters(r_params)

        activated_mode = BaseMode.get_active_mode_raum(raum)
        if activated_mode and 'soll' in activated_mode:
            raum.solltemp = activated_mode['soll']
            raum.save()

        elif 'temp_solltemp' in r_params:
            raum.solltemp = float(r_params.get('temp_solltemp'))
            raum.save()

        logger.warning(u"%s|%s" % (raum.id, u"Manuelle Zieltemperatur von QuickUI beendet. Solltemperatur zurückgesetzt auf %.2f" % raum.solltemp))
        return True

    @staticmethod
    def process_hand_mode_expiry(raum):
        quick_mode = QuickuiMode.hand_mode_params(raum)
        if quick_mode and QuickuiMode.is_expired(quick_mode[1]):
            return QuickuiMode.expire_hand_mode(raum)
        return False

    def set_mode_regelung(self, haus, objid, modulename, *args, **kwargs):
        pass

    def get_mode_regelung(self, haus, objid, modulename, *args, **kwargs):
        pass

    def is_module_activated(self):
        return True

    @staticmethod
    def get_capabilities(haus):
        return {'use_offsets': False, 'icon': 'icon-hand',
                'verbose_name': 'quickui', 'mod': 'quickui', 'priority': 2}


class QuickuiTask(TimeTask):
    MOD = 'quickui'

    method_factory = {
        'expire-handmode': QuickuiMode.expire_hand_mode
    }

    @staticmethod
    def get_module_tasks(haus):
        tasks_list = list()
        for etage in haus.etagen.all():
            for raum in etage.raeume.all().only("module_parameters"):
                qui_mode = QuickuiMode.hand_mode_params(raum)
                if not qui_mode or qui_mode[1] is None:
                    continue

                tasks_list.append(QuickuiTask(QuickuiMode.MOD, 'expire-handmode',
                                  QuickuiTask.MOD, qui_mode[1], raum=raum.id))
        return tasks_list


@multizone_quick_access
@csrf_exempt
def quick_change(request):
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)
    haus = _get_haus_for_user(request.user)
    # temperaturmod
    hausmods = haus.get_modules()
    hparams = haus.get_module_parameters()
    tempmode = False
    tempmode_params = hparams.get('temperaturmodi', {})
    if 'temperaturszenen' in hausmods:
        tempmode = True

    is_any_room = False
    if request.method == 'GET':
        ch.set_usage_val("raumliste_quick", 1)
        eundr = []
        if haus:
            for etage in haus.etagen.all():
                e = {etage: []}
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    is_any_room = True
                    ms = raum.get_mainsensor()
                    temp = None
                    if ms:
                        last = ms.get_wert()
                        if last is not None:
                            temp = last[0]

                    if temp is None:
                        temp = '-'
                    else:
                        temp = '%.2f' % temp


                    activated_mode = QuickuiMode.get_mode_raum(raum)
                    soll = 0
                    if activated_mode:
                        if 'soll' in activated_mode:
                            soll = activated_mode['soll']

                    if not soll:
                        soll = raum.solltemp or 21.0
                        
                    _offsets = get_module_offsets(raum, as_dict=True, only_clear_offsets=True)
                    offsets = sum(_offsets.values())

                    if temp != '-':
                        ziel = float(soll) + float(offsets)
                        ziel = '%.2f' % ziel
                    else:
                        ziel = 0

                    show_time = ""

                    q_r_params = QuickuiMode.hand_mode_params(raum)
                    if q_r_params:
                        ziel = q_r_params[2]
                        ziel = '%.2f' % float(ziel)
                        show_time = q_r_params[1]

                    if show_time:
                        show_time = (datetime.strptime(show_time, '%Y-%m-%d %H:%M')).strftime("%H:%M")

                    e[etage].append((raum, temp, ziel, show_time))  # todo: das hat unten nur die .values_list verwendet
                eundr.append(e)

                if len(eundr) == 0:
                    eundr = None

        default_time = hparams.get('quickui', {}).get('default_duration', 180)
        time_all = (now + timedelta(minutes=default_time)).strftime("%H:%M")
        return render_response(request, "index_quick_change.html",
                          {"haus": haus, 'eundr': eundr, 'time_all': time_all,
                           'is_any_room': is_any_room, 'tempmode': tempmode, 'tempmodes': tempmode_params.iteritems(),
                           'default_time': default_time
                           })

    if request.method == 'POST':

        room_id = int(request.POST.get('room_id', 0))
        if room_id:
            raum = Raum.objects.get(id=room_id)
            r_params = raum.get_module_parameters()
            q_r_params = r_params.get('quickui', {})
        else:
            logic_var = request.POST.get('set_all_time', False) and request.POST.get('timezone', False)
            if logic_var:
                return HttpResponse("error")
            
        if 'set_all_time' in request.POST:
            ch.delete_module_tasks(haus.id, 'quickui')
            ch.set_usage_val("raumliste_quick_sat", 1)

            slider_val = int(request.POST.get('slider_time_val', 0))
            expiration_time = (now + timedelta(minutes=slider_val))
            expiration_time = helpers.get_nearest_time(expiration_time)
            display_time = expiration_time.strftime("%H:%M")

            if 'delete_all' in request.POST:
                id_list = []
                ziel_list = []
                if haus:
                    for etage in haus.etagen.all():
                        for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id).only("module_parameters"):
                            QuickuiMode.expire_hand_mode(raum)
                            raum.refresh_from_db()
                            r_params = raum.get_module_parameters()
                            id_list.append(raum.id)
                            offset = get_module_offsets(raum, only_clear_offsets=True)
                            soll = r_params.get('temp_solltemp', 21.0)
                            recover_ziel = soll + offset
                            ziel_list.append(recover_ziel)

                return HttpResponse(json.dumps({"id_list": id_list, "ziel_list": ziel_list, "all_time": display_time, 'tempmode': tempmode}), content_type="application/json")

            else:
                return HttpResponse(json.dumps({"all_time": display_time, 'tempmode': tempmode}), content_type="application/json")

        elif 'set_room_time' in request.POST:
            ch.delete_module_tasks(haus.id, 'quickui')
            ch.set_usage_val("raumliste_quick_srt", 1)

            slider_val = int(request.POST.get('slider_time_val'))
            expiration_time = (now + timedelta(minutes=slider_val))
            expiration_time = helpers.get_nearest_time(expiration_time)
            display_time = expiration_time.strftime("%H:%M")
            ziel = q_r_params.get('ziel_val', 20)

            if slider_val == 0:  # hand mode manually expired
                QuickuiMode(ziel, slider_val).expire_hand_mode(raum)

            else:
                QuickuiMode(ziel, slider_val).set_mode_raum(raum)

            active_mode_name = "-"

            mode = QuickuiMode.get_mode_raum(raum)
            mode_icon = "icon-clock"
            module_icon = ""
            if mode is None:
                offset, module_icon = get_offsets_icon(raum)
            else:
                mode_icon = mode['icon']
                if mode.get('use_offsets', False):
                    offset, module_icon = get_offsets_icon(raum)
                else:
                    offset = 0.0

            raum.refresh_from_db()
            soll = raum.solltemp
            ziel = float(soll) + offset
            recover_ziel = float(soll) + offset
            if tempmode:
                temp_mode_params = hparams.get("temperaturmodi", {})
                rparams = raum.get_module_parameters()
                room_mode_params = rparams.get("temperaturmodi", {})
                activated_mode = room_mode_params.get('activated_mode', 1)
                if activated_mode:
                    active_mode_name = temp_mode_params.get(activated_mode, {}).get("name", '-')
                    if len(active_mode_name) > 10:
                        active_mode_name = active_mode_name[0:7] + "..."
                    soll = room_mode_params.get(activated_mode, {}).get('solltemp', 21.0)
                    recover_ziel = float(soll) + offset
                    ziel = float(soll) + offset

            return HttpResponse(json.dumps({"room_id": room_id, "room_time": display_time,
                                            'tempmode': tempmode, "mode_name": active_mode_name, "soll": soll, "ziel": ziel,
                                            'offset': offset, 'mode_icon': mode_icon, 'module_icon': module_icon, 'recover_ziel': recover_ziel}), content_type="application/json")

        elif 'set_ziel_slider' in request.POST:
            ch.delete_module_tasks(haus.id, 'quickui')
            duration = request.POST.get('all_time')
            ziel = request.POST.get('slidernumber')
            ret_dict = dict()
            if ziel and duration:
                ziel = helpers.fix_temperature_value(str(ziel))
                quickui_mode = QuickuiMode(ziel, duration)
                ret_dict = quickui_mode.set_mode_raum(raum)
                status = 200
            else:
                ret_dict['success'] = False
                ret_dict['error'] = 'input problem'
                status = 401
            return HttpResponse(json.dumps(ret_dict), content_type="application/json", status=status)

        elif 'set_ziel_slider_hr' in request.POST:
            ch.delete_module_tasks(haus.id, 'quickui')

            q_params = QuickuiMode.hand_mode_params(raum)
            if q_params:
                time_val = q_params[0]
                ziel = float(request.POST.get('slidernumber').replace(',', '.'))
                QuickuiMode(ziel, time_val).set_mode_raum(raum)

            return HttpResponse(json.dumps({"done": True}), content_type="application/json")

        elif 'timezone' in request.POST:
            time_zone = str(pytz.timezone(settings.TIME_ZONE))
            return HttpResponse(json.dumps({"timezone": time_zone}), content_type="application/json")

        elif 'get_room_slider' in request.POST:
            time_zone = str(pytz.timezone(settings.TIME_ZONE))
            show_time = ""
            try:
                slider_val, ex_time, ziel = ch.get("room_time_%s" % room_id)
            except:
                slider_val = 0
                ex_time = "1999-01-01 12:00"
            if ex_time:
                show_time = (datetime.strptime(ex_time, '%Y-%m-%d %H:%M')).strftime("%H:%M")
            return HttpResponse(json.dumps({"timezone": time_zone, "slider_val": slider_val, "room_time": show_time}), content_type="application/json")


def set_jsonapi(data, haus, usr, entityid):
    hparams = haus.get_module_parameters()
    q_params = hparams.get('quickui', {})
    q_params['default_duration'] = int(data.get('duration', 180))
    hparams['quickui'] = q_params
    haus.set_module_parameters(hparams)
    return {}

